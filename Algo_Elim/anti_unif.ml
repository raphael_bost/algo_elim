open List
open Pretty_printer
open Language
open Tools
open Arithmetic
open Dag
open Common_template
open Permutation

                                           
(* ?? génére tous les m-uplets de n-permutations ?? *)
let rec gives_permutations_n_m n m =
  let lperm_n = gives_permutations_n n in
    gives_permutations_n_m_aux lperm_n m
and gives_permutations_n_m_aux lperm m =
  match m with
    | 1 -> map (fun a -> [a]) lperm
    | k when k > 0 -> 
	let lperm_m1 = gives_permutations_n_m_aux lperm (m-1) in
	  fold_right (fun perm l -> (map (fun perm_m1 -> (perm)::(perm_m1)) lperm_m1)@l) lperm [];;


(* get longuest dag with option dag *) 

(* returns the triple (length of the dag, dag, position in the list) *)

let rec bigger_dag ldag =
  let (a,b,c,_) = 
  fold_left 
    (fun (n,elt,poselt,pos) dg -> 
       let ld = length dg in 
	 if ld > n 
	 then (ld,dg,pos,(pos + 1)) 
	 else (n,elt,poselt,(pos + 1))
    )
    (0,[],0,0) ldag
  in (a,b,c);;


(* complete a base with options *)

let rec complete_base_opt dg n =
  match n,dg with
    | 0,[] -> []
    | n,a::q -> (Some a)::(complete_base_opt q (n-1))
    | n,[] -> None ::(complete_base_opt [] (n-1))


(* extract dag bases and complete with options *)

let complete_bases_opt ldg n =
    map (fun d -> complete_base_opt d n) ldg;;


(* change dag according to a permutation permutation gives new position [2;3;1] applied to [a;b;c] is [c;a;b] *)           
(* ?? pas plutot en [b;c;a] ??*)

let rec change_dagelt_permutation dagel perm =
  match dagel with
    | ExprD _ | EmptyD -> dagel
    | PairD(d1,d2) -> PairD(change_dagelt_permutation d1 perm, change_dagelt_permutation d2 perm)
    | DivD(d1,d2) -> DivD(change_dagelt_permutation d1 perm, change_dagelt_permutation d2 perm)
    | VecD(l) -> VecD(map (fun (ai,li) -> (ai,map (fun sqi -> change_dagelt_permutation sqi perm) li)) l)
    | PtD i -> PtD (nth perm (i-1));;

(* change opt base according to a permutation *)

let rec modify_with_perm b perm =
  let n = length perm in 
    modify_with_perm_aux b perm n 
and modify_with_perm_aux b perm n =
  match n with
    | 0 -> []
    | _ -> 
	(modify_with_perm_aux b perm (n-1))@
	  [(match (nth b (pos n perm)) with
	     | None -> None
	     | Some d -> Some (change_dagelt_permutation d perm))];;


(* change dag according to a permutation *)

let rec modify_dag_with_perm d perm =
  let n = length perm in 
    modify_dag_with_perm_aux d perm n 
and modify_dag_with_perm_aux d perm n =
  match n with
    | 0 -> [(change_dagelt_permutation (hd d) perm)]
    | _ -> 
	(modify_dag_with_perm_aux d perm (n-1))@
	  [(change_dagelt_permutation (nth (tl d) (pos n perm)) perm)];;  (* ?? pkoi tl d ?? a cause des indices décalés ? *)



(* Check dependencies to the left *)
(* returns true if the dag contains dependencies to the left *)
let rec left_dependencies ldag =
  left_dependencies_aux ldag 1
and left_dependencies_aux ldag n =
  match ldag with
    | [] -> true
    | (Some a)::q -> left_dependencies_elt a n && left_dependencies_aux q (n+1)
    | None ::q -> left_dependencies_aux q (n+1)
and left_dependencies_elt d n =
  match d with
    | ExprD _ -> true
    | PairD(d1,d2) | DivD(d1,d2) -> left_dependencies_elt d1 n && left_dependencies_elt d2 n
    | VecD(l) -> 
	fold_right (fun (ai,li) b -> b && (fold_right (fun sqi b1 -> b1 && left_dependencies_elt sqi n) li true)) l true
    | PtD i -> i < n;;


(* dag element equality modulo commutativity *)

let rec equal_dag_elt t1 t2 =
  match t1,t2 with
    | t,tt when t=tt -> true
    | ExprD e1, ExprD e2 when e1=e2 -> true
    | DivD(e1,e2),DivD(e3,e4) -> equal_dag_elt e1 e2 && equal_dag_elt e2 e4
    | VecD(l1),VecD(l2) -> 
	length l1 = length l2 &&
	for_all 
	(fun (ai,li) -> 
	   exists
	     (fun (bi,qi) -> 
	     bi = ai && length li = length qi &&
		 for_all 
		 (fun sqli -> exists (fun sqqi -> equal_dag_elt sqli sqqi) qi) 
		 li
	     )
	     l2
	)
	l1
    | _ -> false


(* gives the list of all the dependencies of one element *)

let rec all_dependencies_dag dgl =
  all_dependencies_dag_aux dgl []
and all_dependencies_dag_aux dgl l =
  match dgl with
    | (ExprD _)::q -> l
    | (DivD(d1,d2))::q | (PairD(d1,d2))::q ->
	all_dependencies_dag_aux (d1::q) (all_dependencies_dag_aux (d2::q) l)
    | (VecD(ls))::q ->
	fold_right 
	  (fun (ai,li) ld -> 
	     fold_right 
	       (fun sqi ldep -> all_dependencies_dag_aux (sqi::q) ldep) 
	       li
	       ld
	  )
	  ls 
	  l
    | (PtD i)::q->
	cons_no_repeat i (all_dependencies_dag_aux ((nth dgl i)::q) l);;


(* gives the list of direct dependencies *)

let rec direct_dependencies dgl =
  direct_dependencies_aux dgl []
and direct_dependencies_aux dgl l =
  match dgl with
	| (EmptyD) -> l 
    | (ExprD _) -> l
    | (DivD(d1,d2)) | (PairD(d1,d2)) ->
	direct_dependencies_aux d1 (direct_dependencies_aux d2 l)
    | (VecD(ls)) ->
	fold_right 
	  (fun (ai,li) ld -> 
	     fold_right 
	       (fun sqi ldep -> direct_dependencies_aux sqi ldep) 
	       li
	       ld
	  )
	  ls 
	  l
    | (PtD i) -> cons_no_repeat i l;;


(* replace the None elements in a list of base with left dependency and a reference dag (one of the longuest) according to the list of positive elements *)

let rec replace_none_list_bases lbas lpos def_elt (lgdag,refdag,posref) locvar =
  let basenbr = length lbas in
    replace_none_list_bases_aux lbas lpos 0 (create_empty_tab basenbr) [] def_elt (lgdag,refdag,posref) locvar
and replace_none_list_bases_aux lbas lpos n lmodif elt_const def_elt (lgdag,refdag,posref) locvar =
  if n = (lgdag-1)
  then (lmodif, elt_const)
  else 
    let ref_elt = (nth refdag (n+1)) in
      if
	(no_loc_var_dag (ref_elt::(tl refdag)) locvar &&
	  (contain_comm (direct_dependencies ref_elt) elt_const) &&
	  ((for_all
	      (fun dg -> 
		 match (hd dg) with
		   | None -> false
		   | Some d -> equal_dag_elt d ref_elt 
	      ) lbas) ||
	     (for_all
		(fun dg -> 
		   match (hd dg) with
		     | None -> true
		     | Some d -> equal_dag_elt d ref_elt 
		) lbas && 
		(mem (dag_to_tree (ref_elt::(tl refdag))) lpos))
	  ))
      then
	replace_none_list_bases_aux 
	  (map tl lbas) 
	  lpos
	  (n+1)
	  (map (fun lmodif -> lmodif@[ref_elt]) lmodif)
	  ((n+1)::elt_const)
	  def_elt
	  (lgdag,refdag,posref)
	  locvar
      else
	replace_none_list_bases_aux 
	  (map tl lbas) 
	  lpos
	  (n+1)
	  (map2 (fun lbas lmodif -> 
		   lmodif@[
		     match hd lbas with 
		       | None -> def_elt
		       | Some b -> b]) lbas lmodif)
	  elt_const
	  def_elt
	  (lgdag,refdag,posref)
	  locvar;;


(* Gives the mesure relative to a template sum 4^variable defined*)

let rec mesure_temp_4 temp =
  match temp with
    | PairT(t1,t2)::q -> mesure_temp_4 (t1::q) + mesure_temp_4 (t2::q)
    | _ -> 
	power 4 (length (all_dependencies_temp temp));;

let mesure = ref 
  (fun (a,b) -> (mesure_temp_4 a, length b));;

let best_mesure = ref invert_lexical_order;;

(* Given a list of dags of same length, gives the common template of all of them *)

let rec common_template_dag_list ldag =
  common_template_dag_list_aux (map (map dag_elt_to_temp) ldag )
and common_template_dag_list_aux ldag =
  match hd ldag with
    | [a] -> [greatest_temp_elt_list (map hd ldag)]
    | a::q -> (greatest_temp_elt_list (map hd ldag))::(common_template_dag_list_aux (map tl ldag));;



(* given a permutation and an old mesure return if better the better dag list, the list of const elements and
its mesure *)

let best_permutation ldag lperm (elt,mes_elt) lpos (lgthdag,refdag,posdag) def_elt locvar =
  (* construct list of bases with option*)
  let lbasopt = complete_bases_opt (map tl ldag) (lgthdag - 1) in
    (* apply the permuatation to it *)
  let lbasperm = map2 (modify_with_perm) lbasopt lperm in
    if for_all left_dependencies lbasperm 
    then
      (* apply permutation to ref dag *)  (* necessary because we have applied the permutation to the ref base *)
      let nrefdag = modify_dag_with_perm refdag (nth lperm posdag) in
	(* Replace None in bases *)
      let (newbases,elt_const) = replace_none_list_bases lbasperm lpos def_elt (lgthdag,nrefdag,posdag) locvar in
	(* Construct the new dags (add the head element with permutation)*)
      let new_ldag = 
	map2 
	  (fun hdag nbas -> hdag::nbas) 
	  (map2 (fun dag perm -> change_dagelt_permutation (hd dag) perm) ldag lperm)
	  newbases
      in
	(* construct the common template *)
      let tempcomm = (common_template_dag_list new_ldag) in
	(* mesure the permutation value *)
      let new_mes = (!mesure (tempcomm,elt_const)) in
	if !best_mesure new_mes mes_elt || elt = None 
 	then (Some (new_ldag,elt_const),new_mes) 
	else (elt,mes_elt)	  
    else
      (elt,mes_elt);;


let choose_best_permutation ldag lpos def_elt locvar =
    (* get one of the biggest dag of the list *)
  let (lgth,refdag,posref) = bigger_dag ldag in
    (* extract all the possible permutations *)
  let llperm = gives_permutations_n_m (lgth - 1) (length ldag) in
  let (Some elt) =
    fst    
      (fold_right 
	 (fun perm (elt,mes_elt) -> 
	    best_permutation ldag perm (elt,mes_elt) lpos (lgth,refdag,posref) def_elt locvar)
	 llperm
	 (None,(0,0)))
  in elt;;
       



let antiunif ldag_elt x =
  let temp =  (greatest_temp_elt_list (map dag_elt_to_temp ldag_elt)) in
  let lse = map (fun x -> get_associated_tuple_elt_with_temp x temp) ldag_elt in
  let nx = construct_tuple_var_from_temp_elt temp x in
    (temp_elt_to_fundag_elt temp,lse,nx )
  

let default_sq = ref (ExprD zero);;

type fundag_with_const =
  | ConstT of dag_elt list
  | Templa of (program -> (dag_elt list)) * (program list) * var


(* Construct the dag or dag function correponding to the basis *)
  
let rec construct_template_aux ldag xsq nsq elt_const ndag = 
  match (hd ldag) with
    | [] -> (ConstT [],nsq)
    | a::q when mem ndag elt_const ->
	begin
	  match construct_template_aux (map tl ldag) xsq nsq elt_const (ndag+1) with
	    | ConstT dbase,_ -> (ConstT (a::dbase)),nsq
	    | Templa (tbase,lse,nx),k ->
		Templa ((fun se -> a::(tbase se)),lse,nx),k
	end
    | a::q ->
	begin
	  let (t,lse,nxsq) = antiunif (map hd ldag) (Var (xsq^"_"^(string_of_int nsq))) in
	    match construct_template_aux (map tl ldag) xsq (nsq+1) elt_const (ndag+1) with
	      | ConstT dbase,_ -> 
		  Templa ((fun se -> (t se)::dbase),lse,nxsq),(nsq+1)
	      | Templa (tbase,lseb,nx),k ->
		  Templa ((fun (Pair(se1,se2)) -> (t se1)::(tbase se2)),(map2 (fun x y -> (Pair(x,y))) lse lseb), (Pairvar(nxsq,nx))),k
	end


let rec construct_template_se_var ldag x xsq nsq lpos locvar =
  let (nldag,elt_const) = choose_best_permutation ldag lpos (!default_sq) locvar in
  let (t,lse,nx) = antiunif (map hd nldag) (x) in
    match construct_template_aux (map tl nldag) xsq nsq elt_const 1 with
      | ConstT dbase,_ -> 
	  ((fun se -> (t se)::dbase),lse,nx),(nsq)
      | Templa (tbase,lseb,nxb),k ->
	  ((fun (Pair(se1,se2)) -> (t se1)::(tbase se2)),(map2 (fun x y -> (Pair(x,y))) lse lseb), (Pairvar(nx,nxb))),k
    
