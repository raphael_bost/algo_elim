open Language

(* Some constants *)

let one = (Const (Num 1.))
let zero = (Const (Num 0.))
let tr = Const (Bool true)
let fs = Const (Bool false)

(* Simplified functions that corresponds to the binary and unary operators *)

let plus a b = 
  match a,b with
    | Const (Num 0.),_ -> b
    | _,Const (Num 0.) -> a
    | _,_ -> BOp(Plus,a,b)

let umin a =
  match a with
    | UOp(Umin,e) -> e
    | Const(Num 0.) -> zero
    | _ -> UOp(Umin,a)

let mult a b = 
  match a,b with
    | Const (Num 0.),_ -> zero
    | _,Const (Num 0.) -> zero
    | Const (Num 1.),_ -> b
    | _,Const (Num 1.) -> a
    | UOp(Umin,Const (Num 1.)),_ -> umin b
    | _,UOp(Umin,Const (Num 1.)) -> umin a
    | _,_ -> BOp(Mult,a,b)

let div a b =
  match a,b with
    | Const (Num 0.),_ -> zero
    | _, Const (Num 1.) -> a
    | _,_ -> BOp(Div,a,b)

let ands a b =
  match a,b with
    | Const(Bool false),_ -> fs
    | _,Const(Bool false) -> fs
    | Const(Bool true),_ -> b
    | _,Const(Bool true) -> a
    | _ -> BOp(And,a,b)

let ors a b =
  match a,b with
    | Const(Bool false),_ -> b
    | _,Const(Bool false) -> a
    | Const(Bool true),_ -> tr
    | _,Const(Bool true) -> tr
    | _ -> BOp(Or,a,b)

let umin a =
  match a with
    | UOp(Umin,e) -> e
    | Const(Num 0.) -> zero
    | _ -> UOp(Umin,a)

let sqrt a =
  match a with
    | Const(Num 0.) -> zero
    | Const(Num 1.) -> one
    | _ -> UOp(Sqrt,a)

let neg a =
  match a with
    | Const (Bool true) -> fs
    | Const (Bool false) -> tr
    | _ -> UOp(Neg,a)

let minus a b = plus a (umin b)
