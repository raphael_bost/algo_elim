open Language
open List
open Variables
open Arithmetic
open Tools
open Type_inference
open Elim_div_bool


type sqr_tree =
  | ExprN of program
  | DivN of sqr_tree * sqr_tree
  | VecN of (program * (sqr_tree list)) list
  | PairN of sqr_tree * sqr_tree

exception Unconsistent_letin
exception Misplaced_clean

let rec is_sqrt_div_free e =
  match e with
    | Const _ | Value _ | Fst _ | Snd _ -> true
    | Letin(x,e1,e2) when is_tuple_bool (type_infer_operation e1 []) && is_tuple_bool (type_infer_operation e2 []) -> true
    | Letin _ -> raise Unconsistent_letin
    | UOp(Sqrt,_) | BOp(Div,_,_) -> false
    | UOp(_,e1) -> is_sqrt_div_free e1
    | BOp(_,e1,e2) -> is_sqrt_div_free e1 && is_sqrt_div_free e2
    | Clean e1 -> raise Misplaced_clean
    | Pair(e1,e2) -> is_sqrt_div_free e1 && is_sqrt_div_free e2


(* arithmetic operations on trees *)

let rec constant_multiplication e t =
  match t with
    | ExprN e1 -> ExprN (mult e e1)
    | DivN(en,ed) -> DivN(constant_multiplication e en,ed)
    | VecN(l) -> VecN (map (fun (ai,li) -> (mult e ai,li)) l);;


let rec div_tree t1 t2 =
  match t1,t2 with
    | ExprN _,ExprN _ | VecN _, ExprN _ | VecN _, VecN _ | ExprN _, VecN _ -> DivN(t1,t2)
    | DivN(e1,e2),_ -> div_tree e1 (mult_tree e2 t2)
    | _,DivN(e1,e2) -> div_tree (mult_tree t1 e2) e1
and mult_tree t1 t2 =
  match t1,t2 with
    | ExprN e1,e2 | e2,ExprN e1 ->
	constant_multiplication e1 e2
    | VecN([]),v1 | v1,VecN([]) -> ExprN zero
    | VecN((a1,l1)::q1), VecN((a2,l2)::q2) -> 
	let elet = monom_mult a1 l1 a2 l2 in
	let rec1 = mult_tree (VecN([(a1,l1)])) (VecN(q2)) in
	let rec2 = mult_tree (VecN(q1)) t2 in
	  plus_tree elet (plus_tree rec1 rec2)
    | e1,DivN(e2,e3) | DivN(e1,e3),e2 -> div_tree (mult_tree e1 e2) e3
and plus_tree t1 t2 =
  match t1,t2 with
    | ExprN e1, ExprN e2 -> ExprN (plus e1 e2)
    | ExprN e1, VecN l | VecN l, ExprN e1 ->
	begin
	  match (get_rev_assoc [] l) with
	    | Some e2,nl -> VecN((plus e1 e2,[])::nl)
	    | None,nl -> VecN((e1,[])::nl)
	end
    | VecN(l1),VecN(l2) -> 
	VecN(
	  fold_right 
	    (fun (ai,li) lvec -> 
	       match get_rev_assoc li lvec with
	       | Some e2,nl -> (plus ai e2,li)::nl
	       | None,nl -> (ai,li)::lvec)
	    l1 l2)
    | DivN(e1,e3),e2 | e2,DivN(e1,e3) -> 
	div_tree (plus_tree (mult_tree e2 e3) e1) e3	
and monom_mult a1 l1 a2 l2 =
  let ltree = intersect l1 l2 in
  let lsq = disj_union l1 l2 in
  let ncoef = (fold_right mult_tree ltree (ExprN one)) in
    match lsq with
      | [] -> constant_multiplication (mult a1 a2) ncoef
      | _ -> mult_tree ncoef (VecN([(mult a1 a2,lsq)]));;

     
    

(* transform a numerical expression in a square root tree form *)
let rec expr_num_to_tree e =
  match (to_dnf e) with
(* case no sqrt no div *)
    | e1 when is_sqrt_div_free e1 -> ExprN e1
(* Case with division at top level *)
    | BOp(Div,e1,e2) ->  div_tree (expr_num_to_tree e1) (expr_num_to_tree e2) 
    | BOp(Mult,e1,e2) -> mult_tree (expr_num_to_tree e1) (expr_num_to_tree e2) 
    | BOp(Plus,e1,e2) -> plus_tree (expr_num_to_tree e1) (expr_num_to_tree e2)
    | UOp(Umin,e1) -> constant_multiplication (UOp(Umin,one)) (expr_num_to_tree e1)
    | UOp(Sqrt,e1) -> VecN([one,[expr_num_to_tree e1]]);;


(* transform an expression into its square root tree form *)

let rec expr_to_tree e =
  match e with
    | e1 when is_sqrt_div_free e1 -> ExprN e1
(* since e1 is in P_Nsqrt/ it should contain Const Value Fst and Second and all boolean expressions *)
    | Pair(e1,e2) -> PairN(expr_to_tree e1, expr_to_tree e2)
(* this last case must match the Numerical expressions *)
    | _ -> expr_num_to_tree e ;;



(* test tree equality modulo commutativity *)

let rec equal_tree t1 t2 =
  match t1,t2 with
    | ExprN e1, ExprN e2 when e1=e2 -> true
    | DivN(e1,e2),DivN(e3,e4) -> equal_tree e1 e3 && equal_tree e2 e4
    | VecN(l1),VecN(l2) -> 
	length l1 = length l2 &&
	for_all 
	(fun (ai,li) -> 
	   exists
	     (fun (bi,qi) -> 
	     bi = ai && length li = length qi &&
		 for_all 
		 (fun sqli -> exists (fun sqqi -> equal_tree sqli sqqi) qi) 
		 li
	     )
	     l2
	)
	l1
    | _ -> false


let rec no_loc_var_tree t l =
    match t with
      | ExprN e -> not_free_in_list l e
      | DivN(d1,d2) -> no_loc_var_tree d1 l && (no_loc_var_tree d2 l)
      | VecN(ls) -> 
	  fold_right
	    (fun (ai,li) b1 ->
	       not_free_in_list l ai &&
	       (fold_right
		 (fun sqi b2 -> (no_loc_var_tree sqi l)&& b2)
		 li true) && b1)
	    ls true
      | PairN(d1,d2) -> no_loc_var_tree d1 l && (no_loc_var_tree d2 l);;
