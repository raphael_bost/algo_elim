open Language
open List
open Tools
open Arithmetic
open Dag

type temp_elt =
  | ExpT
  | DivT of temp_elt * temp_elt
  | PairT of temp_elt * temp_elt
  | VecT of (int list) list
  | EmptyT

type temp = temp_elt list

(* transforms a template element in to function the transform assign a program into a dag element *)

let rec temp_elt_to_fundag_elt t =
    match t with
      | ExpT -> (fun x -> ExprD x)
      | DivT(t1,t2) -> (fun (Pair(x,y)) -> DivD(temp_elt_to_fundag_elt t1 x, temp_elt_to_fundag_elt t2 y))
      | PairT(t1,t2) -> (fun (Pair(x,y)) -> PairD(temp_elt_to_fundag_elt t1 x, temp_elt_to_fundag_elt t2 y))
      | VecT([li]) ->  
	  (fun ai -> VecD([ai,(map (fun x -> PtD x) li)]))
      | VecT(li::l) ->
	  let fl = temp_elt_to_fundag_elt (VecT(l)) in
	    (fun (Pair(a1,a2)) -> 
	       let (VecD l2) = fl a2 in
		 VecD((a1,(map (fun x -> PtD x) li))::l2))


(* extract a template from a dag element*)

let rec dag_elt_to_temp d =
  match d with
	| EmptyD -> EmptyT
    | ExprD _ -> ExpT
    | DivD (d1,d2) -> DivT(dag_elt_to_temp d1,dag_elt_to_temp d2)
    | PairD (d1,d2) -> PairT(dag_elt_to_temp d1,dag_elt_to_temp d2)
    | VecD(l) -> VecT(map (fun (ai,li) -> (map (fun (PtD i) -> i) li)) l)


(* gives the greatest template of 2 dags*)

let rec greatest_temp_elt t1 t2 =
  match t1,t2 with
  	| EmptyT, _ -> t2
  	| _, EmptyT -> t1
    | ExpT, VecT(l) | VecT(l), ExpT -> 
	VecT(if (mem [] l) then l else []::l)
    | ExpT, _ -> t2
    | _, ExpT -> t1
    | PairT(t11,t12),PairT(t21,t22) -> PairT(greatest_temp_elt t11 t21, greatest_temp_elt t12 t22)
    | DivT(t11,t12),DivT(t21,t22) -> DivT(greatest_temp_elt t11 t21, greatest_temp_elt t12 t22)
    | DivT(t1,t2),t3 | t3,DivT(t1,t2) -> DivT(greatest_temp_elt t1 t3, greatest_temp_elt t2 ExpT )
    | VecT(l1),VecT(l2) ->
	VecT(
	  fold_right
	    (fun li l -> 
	       if exists (equal_comm li) l then l else li::l)
	    l1
	    l2);;


(* Gives the greatest template from a list of dags *)

let greatest_temp_elt_list l = 
  let (a::q) = l in fold_left greatest_temp_elt a q;;


(* gives the sub expression se associated to a dag element and a bigger template s.t. t(se) = d*)

let rec get_associated_tuple_elt_with_temp d t =
  match d,t with
    | EmptyD, _ -> get_associated_tuple_elt_with_temp (ExprD(Const (Num 0.))) t
	| ExprD a, ExpT -> a
    | ExprD(Const (Num 0.)), VecT([a]) -> zero
    | ExprD(e),VecT([]) -> e
    | ExprD(e), VecT([]::l) ->
	Pair(e,get_associated_tuple_elt_with_temp (ExprD zero) (VecT l))
    | ExprD(e), VecT(a::l) ->
	Pair(zero, get_associated_tuple_elt_with_temp (ExprD e) (VecT l))
    | PairD(t11,t12),PairT(t21,t22) -> Pair(get_associated_tuple_elt_with_temp t11 t21, get_associated_tuple_elt_with_temp t12 t22)
    | ExprD(e), PairT(t1,t2) -> 
	Pair(get_associated_tuple_elt_with_temp (ExprD(Fst e)) t1, get_associated_tuple_elt_with_temp (ExprD(Snd e)) t2)
    | DivD(t11,t12),DivT(t21,t22) -> Pair(get_associated_tuple_elt_with_temp t11 t21, get_associated_tuple_elt_with_temp t12 t22)
    | _,DivT(t1,t2) -> Pair(get_associated_tuple_elt_with_temp d t1, get_associated_tuple_elt_with_temp (ExprD(one)) t2)
    | VecD([]),VecT([li]) -> zero
    | VecD([(ai,lii)]),VecT([li]) when equal_comm (map (fun (PtD i) -> i) lii) li -> ai
    | VecD(l1),VecT(li::l2) ->
	match get_rev_assoc (map (fun i -> PtD i) li) l1 with
	  | Some a,l1q -> Pair(a,get_associated_tuple_elt_with_temp (VecD l1q) (VecT l2))
	  | None,_ -> Pair(zero,get_associated_tuple_elt_with_temp (VecD l1) (VecT l2))


(* gives the tuple of var associated to a numerical template and a variable*)

let rec extract_head_temp_element_uvar t x n =
    match t with
      | ExpT -> Var x
      | DivT(d1,d2) -> Pairvar(extract_head_temp_element_uvar d1 (x^"_n") 1, extract_head_temp_element_uvar d2 (x^"_d") 1)
      | VecT([a]) -> Var (x^"_"^(string_of_int n))
      | VecT(l1::q) ->
	  Pairvar(Var(x^"_"^(string_of_int n)),extract_head_temp_element_uvar (VecT(q)) x (n+1));; 


(* gives the tuple of var associated to a template and one variable*)

let rec extract_head_temp_element_pairvar t x n =
  match t with
    | PairT(t1,t2) -> 
	let x1,n1 = extract_head_temp_element_pairvar t1 x n in
	let x2,n2 = extract_head_temp_element_pairvar t2 x n1 in
	  Pairvar(x1,x2),n2
    | _ -> extract_head_temp_element_uvar t (x^(string_of_int n)) 1,(n+1)


(* gives the tuple of var associated to a template_element and a tuple of variables *)

let rec construct_tuple_var_from_temp_elt t x =
  match x,t with
    | Var z,PairT _ -> fst (extract_head_temp_element_pairvar t z 1)
    | Var z,_ -> (extract_head_temp_element_uvar t z 1)
    | Pairvar(v1,v2),ExpT _ -> Pairvar(v1,v2)
    | Pairvar(v1,v2),PairT(t1,t2) ->
	let va1 = construct_tuple_var_from_temp_elt t1 v1 in
	let va2 = construct_tuple_var_from_temp_elt t2 v2 in
	  Pairvar(va1,va2)


(* gives the list of all the dependencies of a template *)

let rec all_dependencies_temp t =
  all_dependencies_temp_aux t []
and all_dependencies_temp_aux t l =
  match t with
    | (ExpT )::q -> l
    | (DivT(t1,t2))::q | (PairT(t1,t2))::q ->
	all_dependencies_temp_aux (t1::q) (all_dependencies_temp_aux (t2::q) l)
    | (VecT(ls))::q ->
	fold_right 
	  (fun li ld -> 
	     fold_right 
	       (fun i ldep -> cons_no_repeat i (all_dependencies_temp_aux ((nth t i)::q) ldep))  (* ldep plutot que l ?? -> corrige *)
	       li
	       ld
	  )
	  ls 
	  l


let rec super_contain_comm ll1 ll2 =
	match ll1 with
		| l::ql when exists (equal_comm l) ll2 -> super_contain_comm ql ll2
		| [] -> true
		| _ -> false
    
(* returns true if  the tree corresponding to t1 is included in t2 *)
let rec is_included t1 t2 =                                          
	match (t1,t2) with
		| ExpT,ExpT -> true
		| PairT(t11,t12),PairT(t21,t22) -> (is_included t11 t21) && (is_included t12 t22)
		| DivT(t11,t12),DivT(t21,t22) -> (is_included t11 t21) && (is_included t12 t22)
		| VecT(ll1),VecT(ll2) ->  super_contain_comm ll1 ll2
		| _ -> false;;
		
		
		
let test_template (f,lprog,var) =
	let progvar = var_to_program var in
		f progvar;;
		
		  	
