open Language
open List
open Tools
open Arithmetic
open Variables
open Basic_tree

type dag_elt =
  | ExprD of program 
  | VecD of (program * dag_elt list) list
  | DivD of dag_elt * dag_elt
  | PtD of int
  | PairD of dag_elt * dag_elt 
  | EmptyD

type dag = dag_elt list;;
     
exception Empty_dag
(* transform a dag into its corresponding tree *)

let rec dag_to_tree (d : dag) =
  let (h::base) = d in
    match h with
      | ExprD e -> ExprN e
      | VecD l -> VecN(map (fun (ai,li) -> (ai,map (fun sqi -> dag_to_tree (sqi::base)) li)) l)
      | DivD(d1,d2) -> DivN(dag_to_tree (d1::base), dag_to_tree (d2::base))
      | PtD i -> dag_to_tree ((nth d i)::base)  
      | PairD(d1,d2) -> PairN(dag_to_tree (d1::base), dag_to_tree (d2::base))
	  | EmptyD -> raise Empty_dag


(* transforms a tree into a dag, eliminating redundoncy *)

let rec tree_to_dag t : dag =
  let (root,base) = tree_to_dag_aux t [] in root::base
and tree_to_dag_aux t base =
  match t with
    | ExprN e -> (ExprD e),base 
    | DivN (e1,e2) -> 
	let d1,base1 = tree_to_dag_aux e1 base in 
	let d2,base2 = tree_to_dag_aux e2 base1 in
	  DivD(d1,d2),base2
    | VecN((a1,l1)::q) ->
	let lp,base1 = list_tree_to_dag l1 base in
	let VecD(q1),base2 = tree_to_dag_aux (VecN(q)) base1 in
	  VecD((a1,lp)::q1),base2
    | VecN([]) -> VecD([]),base
    | PairN(e1,e2) ->
	let d1,base1 = tree_to_dag_aux e1 base in 
	let d2,base2 = tree_to_dag_aux e2 base1 in
	  PairD(d1,d2),base2
and list_tree_to_dag l base =
  match l with
    | [] -> [],base
    | sq::li -> 
	match get_pos_predicate (fun d -> equal_tree sq (dag_to_tree (d::base))) base with
	  | Some n -> 
	      let li2,base2 = list_tree_to_dag li base in
		((PtD n)::li2),base2
	  | None -> 
	      let sqd,base1 = tree_to_dag_aux sq base in
	      let li2,base2 = list_tree_to_dag li (base1@[sqd]) in
		((PtD (length base1 + 1))::li2),base2;;


(* extract a template, its argument and the corresponding variable  from a dag element representing a numerical expression*)

let rec extract_head_dag_element_template_se_uvar de x n =
    match de with
      | ExprD e -> (fun y -> ExprD y),e,(Var x)
      | DivD(d1,d2) -> 
	  let td1,sed1,x1 = extract_head_dag_element_template_se_uvar d1 (x^"_n") 1 in
	  let td2,sed2,x2 = extract_head_dag_element_template_se_uvar d2 (x^"_d") 1 in
	    (fun (Pair(x,y)) -> DivD(td1 x,td2 y)), Clean (Pair(sed1,sed2)),Pairvar(x1,x2)
      | VecD([(a,l)]) ->
	  (fun e -> VecD([e,l])),a,(Var (x^"_"^(string_of_int n)))
      | VecD((a1,l1)::q) ->
	  let tdq,seq,xq = extract_head_dag_element_template_se_uvar (VecD q) x (n+1) in
	  (fun (Pair(x,y)) ->
	     let VecD(ql) = tdq(y) in
	       VecD((x,l1)::ql)
	  ), Pair(a1,seq),Pairvar(Var(x^"_"^(string_of_int n)),xq)
	  | EmptyD -> raise Empty_dag
	    

(* Extract a template, the arguments and the variable tuple corresponding to a dag element assugned to one variable*)

let rec extract_pair_dag_element_template_se_uvar de x n =
    match de with
      | PairD(d1,d2) ->
	  let (td1,sed1,x1),n1 = extract_pair_dag_element_template_se_uvar d1 x n in
	  let (td2,sed2,x2),n2 = extract_pair_dag_element_template_se_uvar d2 x n1 in
	    ((fun (Pair(x,y)) -> PairD(td1 x,td2 y)), Pair(sed1,sed2),Pairvar(x1,x2)),n2
      | _ -> extract_head_dag_element_template_se_uvar de (x^(string_of_int n)) 1 ,(n+1)


(* Extract a template, the arguments and the variable tuple corresponding to a dag element assigned to a tuple of variable*)

let rec extract_pair_dag_element_template_se_var de x =
  match x,de with
    | Var z,PairD _ -> fst (extract_pair_dag_element_template_se_uvar de z 1)
    | Var z,_ -> (extract_head_dag_element_template_se_uvar de z 1)
    | Pairvar(v1,v2),ExprD(e) -> (fun x -> ExprD x),(e),Pairvar(v1,v2)
    | Pairvar(v1,v2),PairD(d1,d2) ->
	let td1,se1,va1 = extract_pair_dag_element_template_se_var d1 v1 in
	let td2,se2,va2 = extract_pair_dag_element_template_se_var d2 v2 in
	  (fun (Pair(x,y)) -> PairD(td1 x,td2 y)),Pair(se1,se2),Pairvar(va1,va2)
	  

(* check if one of the variable of the list appear as free in the dag *)

let rec no_loc_var_dag d l =
  let (h::q) = d in
    match h with
      | PtD i -> no_loc_var_dag ((nth d i)::q) l
      | ExprD e -> not_free_in_list l e
      | DivD(d1,d2) -> no_loc_var_dag (d1::q) l && (no_loc_var_dag (d2::q) l)
      | VecD(ls) -> 
	  fold_right
	    (fun (ai,li) b1 ->
	       not_free_in_list l ai &&
	       (fold_right
		 (fun sqi b2 -> (no_loc_var_dag (sqi::q) l)&& b2)
		 li true) && b1)
	    ls true
      | PairD(d1,d2) -> no_loc_var_dag (d1::q) l && (no_loc_var_dag (d2::q) l)
	  | EmptyD -> true


type template_compose =
  | Dag of dag_elt list
  | Templ of (program -> (dag_elt list)) * program * var

(* extract dag tamplate, sub_expressions and tuple of variable to be assigned from the dag, the variables, the id of next square root
the square root name the list of local variables *)

let rec extract_dag_template_se_var d x n xsq locvar =
  let (de::base) = d in
  let tde,see,xe = extract_pair_dag_element_template_se_var de x in
    match extract_dag_template_se_var_aux base n xsq base locvar with
      | Dag dbase,_ -> (fun x -> (tde x)::dbase),see,xe,n
      | Templ (tbas,sebas,xebas),k -> 
	  (fun (Pair(x,y)) -> (tde x)::(tbas y)),(Pair(see,sebas)),(Pairvar(xe,xebas)),k
and extract_dag_template_se_var_aux base n xsq fullbase locvar =
  match base with
    | [] -> (Dag [],n)
    | de::bleft when no_loc_var_dag (de::fullbase) locvar -> 
	begin
	  match extract_dag_template_se_var_aux bleft n xsq fullbase locvar with
	    | Dag dbase,_ -> Dag (de::dbase),n
	    | Templ(tbas,sebas,xebas),k -> 
		Templ((fun y -> (de)::(tbas y)),sebas,xebas),k
	end
    | de::bleft ->
	begin
	  let (tde,see,xe) = (extract_head_dag_element_template_se_uvar de (xsq^"_"^(string_of_int n)) 1) in
	    match extract_dag_template_se_var_aux bleft (n+1) xsq fullbase locvar with
	      | Dag dbase,_ -> Templ ((fun x -> (tde x)::dbase),see,xe),(n+1)
	      | Templ(tbas,sebas,xebas),k -> 
		  Templ((fun (Pair(x,y)) -> (tde x)::(tbas y)),(Pair(see,sebas)),(Pairvar(xe,xebas))),k
	end


(* transform a dag into an expression *)

let rec dag_to_expr (d : dag) =
  match d with
    | (ExprD e)::base -> e
    | (PairD(d1,d2))::base -> Pair((dag_to_expr (d1::base)),(dag_to_expr (d2::base)))
    | (DivD(d1,d2))::base -> div (dag_to_expr (d1::base)) (dag_to_expr (d2::base))
    | (PtD i)::base -> dag_to_expr ((nth d i)::base)
    | (VecD(l))::base ->
	fold_right
	  (fun (ai,li) sum ->
	     plus  
	       (mult 
		  ai 
  		  (fold_right
		     (fun sqi psq -> 
			mult (sqrt (dag_to_expr (sqi::base))) psq)
		     li
		     one)
	       )
	       sum
	  )
	  l
	  zero
	| (EmptyD)::base -> raise Empty_dag;;

(* if a pointer is negative, set it to positive *)
let rec make_pointer_positive_in_dag_elt dag_elt =
	match dag_elt with
		| ExprD _  -> dag_elt
		| DivD(d1,d2) -> DivD(make_pointer_positive_in_dag_elt d1,make_pointer_positive_in_dag_elt d2)
		| PairD(d1,d2) -> PairD(make_pointer_positive_in_dag_elt d1,make_pointer_positive_in_dag_elt d2)
		| VecD(ls) -> VecD(map (fun (ai,li) -> (ai,map make_pointer_positive_in_dag_elt li)) ls)
		| PtD(k) -> PtD(abs k) 
;;

let opt_to_dag_elt opt =
	match opt with 
		| Some d -> d
		| None -> EmptyD
;;

let dag_elt_to_opt dag_elt =
	match dag_elt with 
		| EmptyD -> None
		| d -> Some d
;;

