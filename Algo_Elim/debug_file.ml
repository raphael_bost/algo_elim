open Language;
open List
open Memory_size;;
open Tools
open Program_classes
open Type_inference
open Pretty_printer
open Variables;
open Arithmetic;
open Elim_div_bool
open Elim_sqrt_bool
open Template
open Basic_tree
open Dag
open Common_template
open Anti_unif
open Elim_let
open Main_elim


#load "language.cmo";;
#load "tools.cmo"
#load "memory_size.cmo"
#load "variables.cmo";;
#load "interpreter.cmo";;
#load "type_inference.cmo"
#load "pretty_printer.cmo"
#load "program_classes.cmo"
#load "arithmetic.cmo";;
#load "elim_div_bool.cmo"
#load "elim_sqrt_bool.cmo"
#load "basic_tree.cmo"
#load "dag.cmo"
#load "template.cmo"
#load "common_template.cmo"
#load "anti_unif.cmo"

#load "elim_let.cmo"
#load "main_elim.cmo"

#load "lexer.cmo";;
#load "parser.cmo";;


Lexing.from_channel;;

let lexbuf = Lexing.from_channel (open_in "/home/neron/Documents/These/En\ cours/test_dir/test_result/track_line_fun.sqrt.out");;
(*let world = get_environnement result [];;*)
let result = Parser.input Lexer.token lexbuf;;

#trace memory_and_exec_size_aux;;
max_memory_size result 4;;

type_infer result [];;
