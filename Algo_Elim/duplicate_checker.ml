open List
(* open Pretty_printer *)
(* open Extended_pretty_printer *)
open Language
open Tools
open Dag
open Common_template


(* These functions are designed to have a method to keep only the useful templates *)
(* Thus, we will have to construct some trees representing the templates expressions and check is those trees have inclusion (modulo commutativity) inclusion *)
  
(* The complete template element type is designed to store either a constant element or a template with its different arguments and their names *)
type complete_temp_elt_with_const = 
	| ConstCT of dag_elt (* codes for a constant element *)
	| TemplCT of temp_elt * (program list) * var (* template X arguments X arguments name *)
;;

type complete_template = complete_temp_elt_with_const list;;        

(* transform a complete template list in a template list *)
let rec complete_template_to_temp c_temp = 
	match c_temp with    
		| [] -> []
		| (ConstCT a)::q -> (dag_elt_to_temp a)::(complete_template_to_temp q)
		| (TemplCT(temp_elt,_,_))::q -> temp_elt::(complete_template_to_temp q) 
;;              

(* The next 3 functions are designed to create a predicate that will ensure that all the elements of a template list are used (there is a pointer to every element)*)
(* If not we know we can delete this template because it should have been generated otherwise (with an original permuted ldag with less columns) *)

(* search the pointers in the template element *)
let rec scan_for_pointers_in_temp_elt temp_elt cols =
	match temp_elt with
		| ExpT -> cols
		| PairT(d1,d2) | DivT (d1,d2) -> scan_for_pointers_in_temp_elt d1 (scan_for_pointers_in_temp_elt d2 cols)
		| VecT (l) -> fold_right (fun li b -> conc_no_repeat li b) l cols
                  
;;

(* Search for the pointers in the complete template *)
let rec scan_for_pointers_in_complete_template c_temp =
	scan_for_pointers_in_complete_template_aux c_temp []
and scan_for_pointers_in_complete_template_aux c_temp cols =  
	match c_temp with
		| [] -> cols
		| (ConstCT _) :: q -> scan_for_pointers_in_complete_template_aux q cols
		| (TemplCT(temp,_,_))::q -> scan_for_pointers_in_complete_template_aux q (scan_for_pointers_in_temp_elt temp cols) 
;;   		                       

(* The predicate itself *)                             
(* returns true if all the columns of ldag are used  *)
let keep_complete_template_predicate (c_temp,ldag) =   
	let n = (length (hd ldag)) -1 in   
	let cols = scan_for_pointers_in_complete_template c_temp in 
	let sum = fold_left (+) 0 cols in
	    2*sum = n*(n+1) (* checks if all the integer between 1 and n are in the cols list *)
;; 
       
(* template_tree type represents the tree corresponding to a template  *)
(* a bit like an sqr tree except that it does not reprensent any expression *)
type template_tree =    
	| ExpTT                          
	| ConstTT of dag_elt
	| DivTT of template_tree * template_tree
	| PairTT of template_tree * template_tree
	| VecTT of ( template_tree list ) list  
;;	
					   
(* Given a complete template, this function constructs the corresponding template tree *)
let rec reconstruct_template_tree_from_ct c_template =
	reconstruct_template_tree_from_ct_aux (hd c_template) c_template   
and reconstruct_template_tree_from_ct_aux c_temp_elt c_template = 
	match c_temp_elt with
		| ConstCT a -> ConstTT a  (* if the template is a constant, return a constant tree *)
		| TemplCT(temp_elt,_,_) -> 	reconstruct_template_tree_from_temp_elt temp_elt c_template (* otherwise, construct a real tree starting with temp_elt*)
and reconstruct_template_tree_from_temp_elt temp_elt c_template =
	match temp_elt with
		| ExpT -> ExpTT
		| DivT(t1,t2) -> 	let temp_tree1 = reconstruct_template_tree_from_temp_elt t1 c_template
						   	and temp_tree2 = reconstruct_template_tree_from_temp_elt t2 c_template in
								DivTT(temp_tree1,temp_tree2)
		| PairT(t1,t2) ->	let temp_tree1 = reconstruct_template_tree_from_temp_elt t1 c_template
						  	and temp_tree2 = reconstruct_template_tree_from_temp_elt t2 c_template in
		                    	PairTT(temp_tree1,temp_tree2)
		| VecT(l) -> 		(* here is the "tricky" point : we need to construct the template trees corresponding to the template elements which are pointed in l *)	
							VecTT( map (map (fun i -> reconstruct_template_tree_from_ct_aux (nth c_template i) c_template)) l)
	
;;
                     
(* Checks if the tree tt1 is "included" in the tree tt2 *)
(* The inclusion considers only the plus operation (the function checks if all the operands in tt1 are in tt2) *)
let rec is_template_tree_included tt1 tt2 = 
	match tt1,tt2 with
		| ExpTT, ExpTT -> true
		| ConstTT a, ConstTT b when a = b -> true
		| DivTT(tt11,tt12),DivTT(tt21,tt22) -> (is_template_tree_included tt11 tt21) && (is_template_tree_included tt12 tt22)
		| PairTT(tt11,tt12),PairTT(tt21,tt22) -> (is_template_tree_included tt11 tt21) && (is_template_tree_included tt12 tt22)
        | VecTT([]),VecTT(sum2) -> true
		| VecTT(l1i::qsum1),VecTT(sum2) -> 
			begin
				try let l2i = find (is_template_tree_included_aux l1i) sum2 in
					is_template_tree_included (VecTT qsum1) (VecTT (remove_once l2i sum2))
				with Not_found -> false
			end
		| _ -> false

and is_template_tree_included_aux l1i l2i =
	match l1i with
		| [] -> true
		| tt1::q -> try let tt2 = find (is_template_tree_included tt1) l2i in
							is_template_tree_included_aux q  (remove_once tt2 l2i)
					with Not_found -> false
					
				;;
                
(* The predicate used for the (template tree,(complete template,ldag)) triple *)
let template_contains_predicate_commutativity (tt1,(c_temp1,_)) (tt2,(c_temp2,_)) = ((length c_temp1) = (length c_temp2)) && (is_template_tree_included tt2 tt1)
;;

let filter_predicate_commutativity = template_contains_predicate_commutativity;;
    
(* Keeps in the list only the "smallest" elements *)
let rec rebuild_template_set_commutativity l_complete_templates =
	let l_ct_with_template_tree = rev_map (fun (c_templ,ldag) -> (reconstruct_template_tree_from_ct c_templ,(c_templ,ldag))) l_complete_templates in  
	rev_map snd (rebuild_template_set_commutativity_aux l_ct_with_template_tree [])
and rebuild_template_set_commutativity_aux l_ct_with_template_tree accumulator= 
	match l_ct_with_template_tree with 
		| [] -> accumulator
		| c_temp::q ->	(* let new_set = rebuild_template_set_commutativity_aux q in *)
						if exists (filter_predicate_commutativity c_temp) accumulator then 
							rebuild_template_set_commutativity_aux q accumulator
						else
							rebuild_template_set_commutativity_aux q (c_temp::(remove_bigger_temps_in_set_commutativity c_temp accumulator))
and remove_bigger_temps_in_set_commutativity c_temp set =   
	filter (fun template -> not (filter_predicate_commutativity template c_temp)) set
;;

(* add_templates_without_duplicates does the same thing than rebuild_template_set_commutativity except that we assume that l_complete_templates
 	keeps already only the smallest elements, and adds new_templates if needed, removing bigger elements*)
let rec add_templates_without_duplicates new_templates l_complete_templates = 
	match new_templates with
	| [] -> l_complete_templates
	| (c_templ,ldag)::q -> 	let c_temp = (reconstruct_template_tree_from_ct c_templ,(c_templ,ldag)) in
							if exists (filter_predicate_commutativity c_temp) l_complete_templates then 
							 		add_templates_without_duplicates q l_complete_templates
							else
									add_templates_without_duplicates q (c_temp::(remove_bigger_temps_in_set_commutativity c_temp l_complete_templates))
   	

(* returns the "size" of the templates. Indeed, the number of atoms needed to remove the square roots with our method *)
let rec measure_complete_temp c_temp = 
	let temp = complete_template_to_temp c_temp in
	  measure_complete_temp_aux temp
and measure_complete_temp_aux temp =
	match temp with
    | PairT(t1,t2)::q -> measure_complete_temp_aux (t1::q) + measure_complete_temp_aux (t2::q)
    | _ -> power 4 (length (all_dependencies_temp temp))
;;
    

	


	
	
	
	
														