open Language
open Arithmetic

(* Transforms a Numerical expression into its DNF*)

let rec to_dnf e =
  match e with
    | BOp(op,e1,e2) ->
	begin
	  match (op,to_dnf e1,to_dnf e2) with
	    | Plus,v,BOp(Div,t,u) -> to_dnf (div (plus (mult v u) t) u)
	    | Plus,BOp(Div,t,u),v -> to_dnf (div (plus t (mult v u)) u)
	    | Mult,v,BOp(Div,t,u) -> to_dnf (div (mult v t) u)
	    | Mult,BOp(Div,t,u),v -> to_dnf (div (mult v t) u)
	    | Div,v,BOp(Div,t,u) -> to_dnf (div (mult v u) t)
	    | Div,BOp(Div,t,u),v -> to_dnf (div t (mult u v))
	    | Plus,ee1,ee2 -> plus ee1 ee2
	    | Mult,ee1,ee2 -> mult ee1 ee2
	    | Div,ee1,ee2 -> div ee1 ee2
	end
    | UOp(op,e1) ->
	begin
	  match (op,to_dnf e1) with
	    | Umin,BOp(Div,t,u) -> to_dnf (div (umin t) u)
	    | Sqrt,ee1 -> sqrt(ee1)
	    | Umin,ee1 -> umin ee1
	end
    | _ -> e
	      

(* Rule 1 in order to remove division from a comparison and transform the atoms as relation to zero *)

let rule_div1 e =
  match e with
    | BOp(op,BOp(Div,en1,ed1), BOp(Div,en2,ed2)) when op = Eq || op = Neq -> BOp(op,minus (mult en1 ed2) (mult en2 ed1),zero)
    | BOp(op,BOp(Div,en1,ed1), e2) when op = Eq || op = Neq -> BOp(op,minus en1 (mult e2 ed1),zero)
    | BOp(op,e1,BOp(Div,en2,ed2)) when op = Eq || op = Neq -> BOp(op,minus (mult ed2 e1) en2,zero)
    | BOp(op,BOp(Div,en1,ed1), BOp(Div,en2,ed2)) when op = Gt || op = Lt || op = Geq || op = Leq -> 
	ors
	  (ands
	     (BOp(op,minus (mult en1 ed2) (mult en2 ed1),zero))
	     (BOp(Gt,mult ed1 ed2,zero)))
	  (ands
	     (BOp(op,minus (mult en2 ed1) (mult en1 ed2),zero))
	     (BOp(Lt,mult ed1 ed2,zero)))
    | BOp(op,BOp(Div,en1,ed1), e2) when op = Gt || op = Lt || op = Geq || op = Leq ->	
	ors
	  (ands
	     (BOp(op,minus en1 (mult e2 ed1),zero))
	     (BOp(Gt,ed1,zero)))
	  (ands
	     (BOp(op,minus (mult e2 ed1) en1,zero))
	     (BOp(Lt,ed1,zero)))
    | BOp(op,e1,BOp(Div,en2,ed2))  when op = Gt || op = Lt || op = Geq || op = Leq ->
	ors
	  (ands
	     (BOp(op,minus (mult e1 ed2) en2,zero))
	     (BOp(Gt,ed2,zero)))
	  (ands
	     (BOp(op,minus en2 (mult e1 ed2),zero))
	     (BOp(Lt,ed2,zero)))
    | BOp(op,e1,e2) -> BOp(op,minus e1 e2,zero);;



(* Rule 2 in order to remove division from a comparison and transform the atoms as relation to zero *)

let rule_div2 e =
  match e with
    | BOp(op,BOp(Div,en1,ed1), BOp(Div,en2,ed2)) when op = Eq || op = Neq -> BOp(op,minus (mult en1 ed2) (mult en2 ed1),zero)
    | BOp(op,BOp(Div,en1,ed1), e2) when op = Eq || op = Neq -> BOp(op,minus en1 (mult e2 ed1),zero)
    | BOp(op,e1,BOp(Div,en2,ed2)) when op = Eq || op = Neq -> BOp(op,minus (mult ed2 e1) en2,zero)
    | BOp(op,BOp(Div,en1,ed1), BOp(Div,en2,ed2)) when op = Gt || op = Lt || op = Geq || op = Leq ->
	BOp(op,minus (mult (mult en1 ed1) (mult ed2 ed2)) (mult (mult en2 ed2) (mult ed1 ed1)), zero)
    | BOp(op,BOp(Div,en1,ed1), e2) when op = Gt || op = Lt || op = Geq || op = Leq ->
	BOp(op,minus (mult en1 ed1) (mult e2 (mult ed1 ed1)),zero)
    | BOp(op,e1, BOp(Div,en2,ed2)) when op = Gt || op = Lt || op = Geq || op = Leq ->
	BOp(op,minus (mult e1 (mult ed2 ed2)) (mult en2 ed2), zero)
    | BOp(op,e1,e2) -> BOp(op,minus e1 e2,zero);;

(* Define the rule for division elimination we want to use *)

let rule_div = ref rule_div2;;

(* spread the elimination of divisions to all the atoms of a formula *)

let rec spread_elim_div e =
  match e with
    | Value _ | Const _ | Fst _ | Snd _ -> e
    | Pair(e1,e2) -> Pair(spread_elim_div e1, spread_elim_div e2)
    | Letin(x,e1,e2) -> Letin(x,spread_elim_div e1,spread_elim_div e2)
    | BOp(op,e1,e2) when is_bool_binop op -> BOp(op,spread_elim_div e1,spread_elim_div e2)
    | BOp(op,e1,e2) when is_comp_binop op ->
	!rule_div (BOp(op,to_dnf e1,to_dnf e2))
    | UOp(Neg,e1) -> UOp(Neg, spread_elim_div e1);;

