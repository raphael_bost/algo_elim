open List
open Pretty_printer
open Language
open Tools
open Program_classes
open Variables
open Arithmetic
open Elim_div_bool
open Basic_tree
open Dag
open Template
open Anti_unif

let xsq = ref "sq"

(* transform a tuple of variable into its value as a tuple *)

let rec tuple_var_to_prog v =
  match v with
    | Pairvar(v1,v2) -> Pair(tuple_var_to_prog v1, tuple_var_to_prog v2)
    | Var s -> Value s;;

(* give the new variable, the sub expressions and the template corresponding to an expression *)

let transform_letin_exp x e locvar n =
  let temp,see,nx,k = extract_dag_template_se_var (tree_to_dag (expr_to_tree e)) x n (!xsq) locvar in
    (nx,see,temp,k);;


(* Extract the final expression of a program in P_let *)

let rec extract_expressions_P_let p =
  match p with
    | e when is_an_expression_with_letin_bool e -> (fun x -> x),e,[]
    | Letin(x,e1,e2) -> 
	let (letins,e,lvar) = extract_expressions_P_let e2 in
	  (fun exp -> Letin(x,e1,letins exp)),e,((get_list_var_id x)@lvar)


(* gives the variable tuple, the new body and the template to inline for a variable definition in P_let *)

let rec transform_letin_P_let x p1 p2 n =
  let (letins,e,lvar) = extract_expressions_P_let p1 in
  let (nx,see,temp,k) = transform_letin_exp x e lvar n in
  let nnx = new_tup_var x nx p2 in
    (nnx,letins see,(temp (tuple_var_to_prog nnx)),k) 
  

(* Get the list of expressions under square roots in an expression *)

let rec get_positive_expressions dag =
  map (fun sq -> dag_to_tree (sq::(tl dag))) (tl dag);;


(* given a variable definition, return the new list of positive expression, ie remove expressions that contain redefined variables and add the new ones corresponding to the template *)

let rec update_positive_expression_list nx temp lpos =
  let lvarnx = get_list_var_id nx in
    update_positive_expression_list_aux lvarnx temp lpos
and update_positive_expression_list_aux lvarnx temp lpos =
    match lpos with
	[] -> (get_positive_expressions temp)
      | a::q when no_loc_var_tree a lvarnx -> cons_no_repeat a (update_positive_expression_list_aux lvarnx temp q)
      | a::q -> (update_positive_expression_list_aux lvarnx temp q);;


(* extract the list of returned expressions and the list of local variables *)

let rec extract_expressions_list e =
  let (iflet,le,lvar) = extract_expressions_list_aux e in
    ((fun l -> fst (iflet l)),le,lvar)
and extract_expressions_list_aux e =
  match e with
    | e1 when is_an_expression_with_letin_bool e1 -> (fun (e::q) -> e,q),[e1],[]
    | Letin(x,e1,e2) -> 
	let (letins,e,lvar) = extract_expressions_list_aux e2 in
	  (fun le -> 
	     let (ee2,le2) = letins le in
	       Letin(x,e1,ee2),le2),e,((get_list_var_id x)@lvar)
    | If(f,e1,e2) -> 
	let (letins1,le1,lvar1) = extract_expressions_list_aux e1 in
	let (letins2,le2,lvar2) = extract_expressions_list_aux e2 in
	  (fun le -> 
	     let (ee1,lee1) = letins1 le in
	     let (ee2,lee2) = letins2 lee1 in
	       If(f,ee1,ee2),lee2),(le1@le2),(conc_no_repeat lvar1 lvar2)


(* gives the new variables, the body and the template to inline for a variable definition with tests *)

let transform_letin_tests x p1 p2 posvar n =
  let (letins,lexp,locvar) = extract_expressions_list p1 in
  let ldag = (map (fun e -> tree_to_dag (expr_to_tree e)) lexp) in
  let ((temp,lbodies,nx),k) = construct_template_se_var ldag x (!xsq) n posvar locvar in
  let nnx = new_tup_var x nx p2 in
    (nnx,(letins lbodies), (temp (tuple_var_to_prog nnx)),k);;

    

(* elim_let *)

let rec elim_let x p1 p2 posvar n =
  match p1 with
    | p when is_in_P_let p -> 
	let (nx,bod,temp,k) = transform_letin_P_let x p p2 n in
	let lposvar = update_positive_expression_list nx temp posvar in
	let pp2 = inline_tuple_expr x (dag_to_expr temp) p2 in
	  (nx,bod,pp2,lposvar,k)
    | p -> 
	let (nx,bod,temp,k) = transform_letin_tests x p p2 posvar n in
	let lposvar = update_positive_expression_list nx temp posvar in
	let pp2 = inline_tuple_expr x (dag_to_expr temp) p2 in
	  (nx,bod,pp2,lposvar,k)
