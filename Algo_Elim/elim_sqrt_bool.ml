open Language
open Arithmetic
open Elim_div_bool
open List
open Tools

(* factorize_square_root e sq = p,r => e = p\sqrt{sq} + r*)

let rec factorize_square_root e sq =
  match e with
    | Const _ | Value _ | Fst _ | Snd _ -> (zero,e)
    | UOp(Sqrt,e1) when e1 = sq -> (one,zero)
    | UOp(Sqrt,e1) -> (zero,e)
    | UOp(Umin,e1) -> 
	let (p,r) = factorize_square_root e1 sq in (umin p, umin r)
    | BOp(Plus,e1,e2) -> 
	let ((p1,r1),(p2,r2)) = (factorize_square_root e1 sq,factorize_square_root e2 sq) in
	  (plus p1 p2, plus r1 r2)
    | BOp(Mult,e1,e2) -> 
	let ((p1,r1),(p2,r2)) = (factorize_square_root e1 sq,factorize_square_root e2 sq) in
	  (plus (mult p1 r2) (mult p2 r1), plus (mult (mult p1 p2) sq) (mult r1 r2))


(* Get the list of all square roots of the expression with their maximal depth*)

let rec get_list_sqrt_depth e =
  match e with
    | BOp(op,e1,e2) ->
	let (l1,l2) = (get_list_sqrt_depth e1, get_list_sqrt_depth e2) in
	  add_max l1 l2
    | UOp(Umin,e1) -> get_list_sqrt_depth e1
    | UOp(Sqrt,e1) -> 
	let l1 = get_list_sqrt_depth e1 in
	  (e1,0)::(map (fun (a,b) -> (a,b+1)) l1)
    | Value _ | Const _ | Fst _ | Snd _ -> [];;
	  

(* Give one of the top level square roots (square root of depth 0) *)

let rec get_depth_zero_sqrt e =
  get_to_zero_elt (get_list_sqrt_depth e)



(* Name of the atom we will use to remove square roots*)

let (atp,atr,atfm,atneq) = (Value "at_p", Value "at_r", Value "at_rel", Value "at_neq")


(* Elimination of square roots from an element : p sqrt{sq} + r op 0*)

let rule_sqrt_name op p sq r =
  match op with
    | Eq -> 
	ands
	  (BOp(Leq,mult p r,zero))
	  (BOp(Eq,minus (mult (mult p p) sq) (mult r r), zero))
    | Neq -> 
	ors
	  (BOp(Gt,mult p r,zero))
	  (BOp(Neq,minus (mult (mult p p) sq) (mult r r), zero))
    | Gt ->
	Letin(Pairvar(Pairvar(Var "at_p",Var "at_r"), Pairvar(Var "at_rel",Var "at_neq")),
	      Pair(
		Pair(
		  BOp(Gt,p,zero),
		  BOp(Gt,r,zero)  
		),
		Pair(
		  BOp(Gt,minus (mult (mult p p) sq) (mult r r), zero),
		  BOp(Neq,minus (mult (mult p p) sq) (mult r r), zero)
		)
	      ),
	      ors (ands atp atr) (ors (ands atp (ands (neg atr) atfm))  (ands (ands (neg atp) atr) (ands (neg atfm) atneq)))
	     )
    | Lt ->
	Letin(Pairvar(Pairvar(Var "at_p",Var "at_r"), Pairvar(Var "at_rel",Var "at_neq")),
	      Pair(
		Pair(
		  BOp(Lt,p,zero),
		  BOp(Lt,r,zero)  
		),
		Pair(
		  BOp(Lt,minus (mult (mult p p) sq) (mult r r), zero),
		  BOp(Neq,minus (mult (mult p p) sq) (mult r r), zero)
		)
	      ),
	      ors (ands atp atr) (ors (ands (neg atp) (ands atr atfm)) (ands (ands atp (neg atr)) (ands (neg atfm) atneq)))
	     )
    | Geq ->
	Letin(Pairvar(Pairvar(Var "at_p",Var "at_r"), Pairvar(Var "at_rel",Var "at_neq")),
	      Pair(
		Pair(
		  BOp(Geq,p,zero),
		  BOp(Geq,r,zero)  
		),
		Pair(
		  BOp(Geq,minus (mult (mult p p) sq) (mult r r), zero),
		  BOp(Eq,minus (mult (mult p p) sq) (mult r r), zero)
		)
	      ),
	      ors (ands atp atr) (ors (ands atp (ands (neg atr) atfm))  (ands (ands (neg atp) atr) (ors (neg atfm) atneq)))
	     )
    | Leq ->
	Letin(Pairvar(Pairvar(Var "at_p",Var "at_r"), Pairvar(Var "at_rel",Var "at_neq")),
	      Pair(
		Pair(
		  BOp(Leq,p,zero),
		  BOp(Leq,r,zero)  
		),
		Pair(
		  BOp(Leq,minus (mult (mult p p) sq) (mult r r), zero),
		  BOp(Neq,minus (mult (mult p p) sq) (mult r r), zero)
		)
	      ),
	      ors (ands atp atr) (ors (ands (neg atp) (ands atr atfm))  (ands (ands atp (neg atr)) (ors (neg atfm) atneq)))
	     );;


let rule_sqrt_no_name op p sq r =
  match op with
    | Eq -> 
	ands
	  (BOp(Leq,mult p r,zero))
	  (BOp(Eq,minus (mult (mult p p) sq) (mult r r), zero))
    | Neq -> 
	ors
	  (BOp(Gt,mult p r,zero))
	  (BOp(Neq,minus (mult (mult p p) sq) (mult r r), zero))
    | Gt ->
	ors 
	  (ands 
	     (BOp(Gt,p,zero))
	     (BOp(Gt,r,zero)))
	  (ors 
	     (ands 
		(BOp(Gt,p,zero)) 
		(ands 
		   (BOp(Leq,r,zero)) 
		   (BOp(Gt,minus (mult (mult p p) sq) (mult r r), zero))))  
	     (ands 
		(ands 
		   (BOp(Leq,p,zero)) 
		   (BOp(Gt,r,zero))) 
		(BOp(Lt,minus (mult (mult p p) sq) (mult r r), zero))))
	     
    | Lt ->
	ors 
	  (ands 
	     (BOp(Lt,p,zero)) 
	     (BOp(Lt,r,zero))) 
	  (ors 
	     (ands 
		(BOp(Lt,p,zero))
		(ands 
		   (BOp(Geq,r,zero)) 
		   (BOp(Gt,minus (mult (mult p p) sq) (mult r r), zero))))  
	     (ands 
		(ands 
		   (BOp(Geq,p,zero)) 
		   (BOp(Lt,r,zero))) 
		(BOp(Lt,minus (mult (mult p p) sq) (mult r r), zero))))
    | Geq ->
	ors 
	  (ands 
	     (BOp(Geq,p,zero))
	     (BOp(Geq,r,zero)))
	  (ors 
	     (ands 
		(BOp(Geq,p,zero))
		(ands 
		   (BOp(Leq,r,zero)) 
		   (BOp(Geq,minus (mult (mult p p) sq) (mult r r), zero))))  
	     (ands 
		(ands 
		   (BOp(Leq,p,zero)) 
		   (BOp(Geq,r,zero))) 
		(BOp(Leq,minus (mult (mult p p) sq) (mult r r), zero))))
    | Leq ->
	ors 
	  (ands 
	     (BOp(Leq,p,zero)) 
	     (BOp(Leq,r,zero))) 
	  (ors 
	     (ands 
		(BOp(Geq,p,zero))
		(ands 
		   (BOp(Leq,r,zero)) 
		   (BOp(Leq,minus (mult (mult p p) sq) (mult r r), zero))))  
	     (ands 
		(ands 
		   (BOp(Leq,p,zero)) 
		   (BOp(Geq,r,zero))) 
		(BOp(Geq,minus (mult (mult p p) sq) (mult r r), zero))))



let rule_sqrt = ref rule_sqrt_name;;

let rec elim_bool e =
    match (spread_elim_div e) with
      | BOp(op,e1,e2) when is_bool_binop op -> BOp(op,elim_bool e1,elim_bool e2)
      | BOp(op,e1,e2) when is_comp_binop op -> 
	  begin
	    match get_depth_zero_sqrt e1 with
	      | None -> BOp(op,e1,e2)
	      | Some sq -> 
		  let (p,r) = factorize_square_root e1 sq in elim_bool (!rule_sqrt op p sq r)
	  end
      | Letin(x,e1,e2) -> Letin(x,elim_bool e1,elim_bool e2)
      | Pair(e1,e2) -> Pair(elim_bool e1, elim_bool e2)
      | UOp(Neg,e1) -> neg (elim_bool e1)
      | Value _ | Const _ | Fst _ | Snd _ -> e

