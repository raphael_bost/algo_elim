open List
open Language
open Pretty_printer
open Variables
open Program_classes
open Type_inference
open Elim_sqrt_bool
open Elim_let
open Extended_unif
open Basic_tree
open Dag
open Template
open Language

                                 

let extended_transform_letin_tests x p1 p2 posvar n =
  let (letins,lexp,locvar) = extract_expressions_list p1 in
  let ldag = (map (fun e -> tree_to_dag (expr_to_tree e)) lexp) in        
  let  template_list = construct_all_templates_se_var ldag x (!xsq) n posvar locvar in
  map (
		fun ((temp,lbodies,nx),k) -> let nnx = new_tup_var x nx p2 in (nnx,(letins lbodies), (temp (tuple_var_to_prog nnx)),k)
	) template_list
  ;;


let rec extended_elim_let x p1 p2 posvar n =
	match p1 with
    | p when is_in_P_let p -> 
	let (nx,bod,temp,k) = transform_letin_P_let x p p2 n in
	let lposvar = update_positive_expression_list nx temp posvar in
	let pp2 = inline_tuple_expr x (dag_to_expr temp) p2 in
	  [(nx,bod,pp2,lposvar,k)]
    | p -> 
	let template_list = extended_transform_letin_tests x p p2 posvar n in
	map (fun (nx,bod,temp,k) -> let lposvar = update_positive_expression_list nx temp posvar in
								let pp2 = inline_tuple_expr x (dag_to_expr temp) p2 in
	  							(nx,bod,pp2,lposvar,k) 
	    ) template_list

 

let rec extended_elim_main e tenv posvar n =
  match e with
    | e when is_an_expression e && type_infer e tenv = BoolT -> [elim_bool (red_pair e)]
    | e when is_an_expression e && type_infer e tenv = NumT -> [red_pair e]
    | Pair(e1,e2) -> let list1 = extended_elim_main e1 tenv posvar n
					 and list2 = extended_elim_main e2 tenv posvar n in 
				   	fold_left (fun acc t1 -> fold_left (fun b t2 -> (Pair(t1,t2))::b) acc list2) [] list1 
    | e when is_an_expression e -> [e]
    | If(f,e1,e2) -> let listf = extended_elim_main f tenv posvar n
 					 and list1 = extended_elim_main e1 tenv posvar n
					 and list2 = extended_elim_main e2 tenv posvar n in
					 fold_left(fun accf tf -> fold_left (fun acc1 t1 -> fold_left (fun acc2 t2 -> (If(tf,t1,t2))::acc2) acc1 list2) accf list1) [] listf 
				   	
    | Letin(x,p1,p2) ->
	let lpp1 = extended_elim_main p1 tenv posvar n in 
	let list_let = concat (map (fun pp1 -> extended_elim_let x pp1 p2 posvar n) lpp1) in 
	(* let (nx,bod,pp2,nposvar,k) = elim_let x pp1 p2 posvar n in *)
	fold_left 	(fun acc (nx,bod,pp2,nposvar,k) -> 	let list_ppp2 = extended_elim_main pp2 (add_type_env nx (type_infer bod tenv) tenv) nposvar k in
														(map (fun ppp2 -> Letin(nx,bod,ppp2)) list_ppp2) @acc
			  	)
	[] list_let;;
 
let extended_main_transform p = 
  let pif = p_norm_reduction "ifname" p in
  let pp = extended_elim_main pif [] [] 1 in map red_pair pp;;
                 
let rec program_flusher p_list path = 
	print_string "\nFlush generated programs\n";
	program_flusher_aux p_list path 1 (length p_list)
  	and program_flusher_aux p_list path i n = 
   	match p_list with
	| [] -> ()
	| p::q -> let out = open_out ((path^"."^(string_of_int i))^".out") in
		output_expr out p;
		flush out;
		close_out out;
		print_string ("\r");Extended_pretty_printer.progress_bar_printer i n;
		print_string (" "^(string_of_int i)^"/"^(string_of_int n));
		flush stdout;
		
		program_flusher_aux q path (i+1) n;;
