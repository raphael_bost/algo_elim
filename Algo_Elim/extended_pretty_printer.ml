open Pretty_printer
open Dag                
open List           

	
let rec dag_elt_pretty_printer dag_elt =
	match dag_elt with
	    | EmptyD -> print_string "#"
		| ExprD e -> print_prog e
		| PairD(d1,d2) -> print_string "("; dag_elt_pretty_printer d1 ; print_string " , "; dag_elt_pretty_printer d2 ; print_string ")" 
		| DivD(d1,d2) -> print_string "("; dag_elt_pretty_printer d1 ; print_string ") / ("; dag_elt_pretty_printer d2 ; print_string ")" 
		| PtD i -> print_string ("|"^(string_of_int i)^"|")
		| VecD l ->  iter (fun (ai,li) -> ( print_string (prog_to_string ai)); (dag_elt_pretty_printer_aux li);print_string " + ") (tl l); 
						let (a,li) = hd l in print_string (prog_to_string a); (dag_elt_pretty_printer_aux li)		   
and dag_elt_pretty_printer_aux li =
	match li with 
		| [] -> print_string ""
		| a::q -> print_string " * sqrt(" ; (dag_elt_pretty_printer a); print_string ")"; (dag_elt_pretty_printer_aux q)
;;

let rec dag_elt_to_string dag_elt =
	match dag_elt with
	    | EmptyD -> "#"
		| ExprD e -> prog_to_string e
		| PairD(d1,d2) -> "("^(dag_elt_to_string d1)^","^(dag_elt_to_string d2)^")" 
		| DivD(d1,d2) -> "("^(dag_elt_to_string d1)^")/("^(dag_elt_to_string d2)^")" 
		| PtD i -> "|"^(string_of_int i)^"|"
		| VecD l ->  let (a,li) = hd l in  		   
				fold_left (fun conc (ai,li) -> conc^( (prog_to_string ai))^(dag_elt_to_string_aux li)^"+") ((prog_to_string a)^(dag_elt_to_string_aux li)) (tl l); 
and dag_elt_to_string_aux li =
	match li with 
		| [] -> ""
		| a::q -> "*sqrt("^(dag_elt_to_string a)^")"^(dag_elt_to_string_aux q)
;;         
let rec dag_pretty_printer dag =
	match dag with
		| [] -> print_string ""
		| d::q -> dag_elt_pretty_printer d; print_string " | "; dag_pretty_printer q
;;
let rec dag_to_string dag =
	match dag with
		| [] -> ""
		| d::q -> (dag_elt_to_string d)^"|"^(dag_to_string q)
;;                         

let rec dag_list_pretty_printer ldag =
		print_string "[\n"; dag_list_pretty_printer_aux ldag; print_string "]"; print_string "\n"
	and dag_list_pretty_printer_aux ldag = 
		match ldag with
		| [] -> print_string "\n"
		| d::q -> dag_pretty_printer d; print_string "\n"; dag_list_pretty_printer_aux q 
;;
let rec dag_list_to_string ldag =
		"["^dag_list_to_string_aux ldag
	and dag_list_to_string_aux ldag = 
		match ldag with
		| [] -> "]"
		| d::q -> (dag_to_string d)^";"^(dag_list_to_string_aux q) 
;;			
		
let rec base_pretty_printer base =
	print_string "["; base_pretty_printer_aux base ; print_string "]"
and base_pretty_printer_aux base = 
	match base with
	| [] -> print_string " "
	| None::q -> print_string "# | " ; base_pretty_printer_aux q
	| Some d::q -> print_string "dag : "; dag_pretty_printer d ; print_string " | "; base_pretty_printer_aux q 

let rec base_list_pretty_printer lbase =
		print_string "[\n"; base_list_pretty_printer_aux lbase; print_string "]"; print_string "\n"
	and base_list_pretty_printer_aux lbase = 
		match lbase with
		| [] -> print_string "\n"
		| b::q -> base_pretty_printer b; print_string "\n"; base_list_pretty_printer_aux q 


let rec progress_bar_printer n max =
	print_string "[";progress_bar_printer_aux n max 1 50;
	print_string "]";
and progress_bar_printer_aux n max i max_i =
 	if i <= max_i then
		begin
			if max_i*n >= i*max then print_string "#"
			else print_string "_";
			progress_bar_printer_aux n max (i+1) max_i
		end
;;