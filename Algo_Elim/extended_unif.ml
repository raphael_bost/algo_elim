(************************************************************************
*
*  extended_unif.ml
*  Algo_Elim
*
*  Created by Raphael Bost on 
6 Apr 2012.
*  Copyright (c) 
2012 Ecole Polytechnique. All rights reserved.
*
************************************************************************)

                                                     
                                    
open List
open Pretty_printer
open Extended_pretty_printer
open Language
open Tools
open Arithmetic
open Dag
open Common_template
open Anti_unif 
open Basic_tree       
open Ldag_fill
open Break_sharing
open Duplicate_checker
open Permutation
open Hash_functions    


  
exception Invalid_args
               
                 
(* for n_min n_max as args, constructs the list [n_min, n_min+1, ..., n_max] *)                        
let rec create_list_min_max n_min n_max =
	match n_min with 
	| n when n_min > n_max -> []
	| _ -> n_min::(create_list_min_max (n_min +1) n_max);; 

                  
(* extend the dag to have a defined length : complete by empty dags *)
let rec complete_dag dg n =
   match n,dg with
	    | 0,[] -> []
	    | n,a::q -> a::(complete_dag q (n-1))
	    | n,[] -> EmptyD ::(complete_dag [] (n-1))
	
let complete_dags ldg n =
    map (fun d -> complete_dag d n) ldg;;

 

(* give all the possible dag and permutations according to the llperm list (a list of permutations) *)
(* all the bases are extended to the length lgth *) 
(* return the triplet (dag list, bases list, corresponding permutation) but only selects the right 
ones (with correct dependencies) *)
                                                              

let rec all_permutations_perm ldag llperm lgthdag =
	let lbasopt = complete_bases_opt (map tl ldag) (lgthdag - 1) in  (* contruct the new bases options *)
	let edag = complete_dags ldag lgthdag in
		(*let permuted_bases_list = map (map2 modify_with_perm lbasopt) llperm in  (* apply permutations *)  *)
		fold_right (filter_permutations lbasopt edag) llperm []
and filter_permutations lbasopt ldag lperm acc = 
	let permuted_bases = map2 modify_with_perm lbasopt lperm in  
		if (for_all left_dependencies permuted_bases) then (*dependencies are good -> *) 
			
			(* write a modify routine that takes care of the fact that the permutation can be longer than the dag *)
		  	let nldag = map2 modify_dag_with_perm ldag lperm in   (*modify the dags*)   
				(nldag, permuted_bases)::acc
		else acc;;
		

(* give all the possible base permutations according to the llperm list (a list of permutations) *)
(* all the bases are extended to the length lgth *) 
(* the bases are the bases corresponding to the ldag list *)            
let all_base_permutations_perm ldag llperm lgthdag =
	let lbasopt = complete_bases_opt (map tl ldag) (lgthdag - 1) in  (* contruct the new bases options *)
		let permuted_bases_list = map (map2 modify_with_perm lbasopt) llperm in  (* apply permutations *)
			filter (for_all left_dependencies)  permuted_bases_list;; (* return only the bases with correct dependencies *)


(* compute all valid permutations for the ldag list extended to the length n*)
let all_permutations_n ldag n = 
	let llperm = gives_permutations_n_m n (length ldag) in
		all_permutations_perm ldag llperm (n+1);;
	
(* construct all the valid permutations *)
let compute_all_possible_permutations ldag = 
	let dag_length = map (fun dag -> ((length dag) -1) ) ldag in
		let n_min = fold_right max dag_length 0 
		and n_max = fold_right (+) dag_length 0 in
				let n_list = create_list_min_max n_min n_max in 
					concat (map (all_permutations_n ldag) n_list);;
					           
					
(* list the dags that contains square roots *)
let rec non_empty_dags_indices ldag =
	non_empty_dags_indices_aux ldag 0
and non_empty_dags_indices_aux ldag i = 
	match ldag with
	| [] -> []
	| dag::q when (length dag) > 1 -> i::(non_empty_dags_indices_aux q (i+1))
	| dag::q -> (non_empty_dags_indices_aux q (i+1))
;;
	             

let rec non_empty_dags ldag =
	non_empty_dags_aux ldag 0
and non_empty_dags_aux ldag i = 
	match ldag with
	| [] -> []
	| dag::q when (length dag) > 1 -> dag::(non_empty_dags_aux q (i+1))
	| dag::q -> (non_empty_dags_aux q (i+1))
;;

	(* apply the permutations of llperm to the dags listed in dag_indices *)
	(* dag_indices must be sorted *)
let rec apply_perms_to_dags lperm dag_indices ldag =
	apply_perms_to_dags_aux lperm dag_indices ldag 0
and apply_perms_to_dags_aux lperm dag_indices ldag pos = 
	match dag_indices with
	| [] -> ldag
	| i::q when i = pos -> (modify_dag_with_perm (hd ldag) (hd lperm))::(apply_perms_to_dags_aux (tl lperm) q (tl ldag) (pos+1))
	| i::q ->  (hd ldag)::(apply_perms_to_dags_aux lperm dag_indices (tl ldag) (pos+1))
		
(* same as previous but for bases *)
let rec apply_perms_to_bases lperm base_indices lbas =
	apply_perms_to_bases_aux lperm base_indices lbas 0
and apply_perms_to_bases_aux lperm base_indices lbas pos = 
	match base_indices with
	| [] -> lbas
	| i::q when i = pos -> (modify_with_perm (hd lbas) (hd lperm))::(apply_perms_to_bases_aux (tl lperm) q (tl lbas) (pos+1))
	| i::q ->  (hd lbas)::(apply_perms_to_bases_aux lperm base_indices (tl lbas) (pos+1))
	       
	(* apply the permutations of llperm to the all the dags *)
let rec apply_perms_to_all_dags lperm ldag =
	map2 modify_dag_with_perm  ldag lperm
;;
(* same as previous but for bases *)
let rec apply_perms_to_all_bases lperm lbas =
   	map2 modify_with_perm lbas lperm
;;

(* change the pointer due to the removal of  column i the the dag or base*)
let rec shift_col_in_dag_elt i elt = 
	match elt with
		| ExprD _ | EmptyD -> elt
		| DivD (e1,e2) -> DivD(shift_col_in_dag_elt i e1, shift_col_in_dag_elt i e2)
		| PairD (e1,e2) -> PairD(shift_col_in_dag_elt i e1, shift_col_in_dag_elt i e2)
		| VecD	(l) -> VecD(map (fun (ai,li) -> (ai, map (fun sqi -> shift_col_in_dag_elt i sqi) li)) l)
		| (PtD j) when j >= i -> PtD (j-1) (* take care of dependencies *)
		| PtD j	-> elt

let rec shift_col_in_dag dag n =
	map (shift_col_in_dag_elt n) dag;;
	
let rec shift_col_in_base base n =
	match base with 
		| [] -> []
		| None::q -> None::(shift_col_in_base q n)
		| (Some a)::q -> (Some (shift_col_in_dag_elt n a))::(shift_col_in_base q n)
	                                             
	
(* delete the column with index n in the base *)
exception Invalid_args_delete_base_col

let rec delete_base_col base n =
	delete_base_col_aux base n 1
and delete_base_col_aux base n i = 
	match i,base with
	 | _, [] -> [];
	 | i,a::li when i=n -> shift_col_in_base li n
	 | i,a::li when i<n -> a::(delete_base_col_aux li n (i+1))
	 | _ -> print_int i; print_string ","; print_int n; print_string " : "; raise Invalid_args_delete_base_col 

let delete_bases_col lbase n =
	map (fun base -> delete_base_col base n) lbase

(* delete all the columns in cols in the bases of lbase *)
let delete_bases_cols lbase cols =
	fold_left delete_bases_col lbase (rev (sort compare cols)) (* remove the farthest first *)  
  
exception Invalid_args_delete_dag_col
                
(* same thing as before but for dags *)
let rec delete_dag_col dag n = 
		(shift_col_in_dag_elt n (hd dag))::delete_dag_col_aux (tl dag) n 1  (* we assume left dependencies but we have to take care of the first element *)
	and delete_dag_col_aux dag n i = 
		match i,dag with 
		 | _,[] -> []
		 | i,a::li when i=n -> shift_col_in_dag li n
		 | i,a::li when i<n -> a::(delete_dag_col_aux li n (i+1))
		 | _ -> print_int i; print_string ","; print_int n; print_string " : "; raise Invalid_args_delete_dag_col
		
let delete_dags_col ldag n =
	map (fun dag -> delete_dag_col dag n) ldag
		 
let delete_dags_cols ldag cols =
	fold_left delete_dags_col ldag (rev (sort compare cols)) (* remove the farthest first *)  
	         
let rec columns_to_delete lbase =
	match lbase with 
	| [] -> []
	| _ ->	columns_to_delete_aux lbase 1 (length (hd lbase)) (* the length of the bases *)
and columns_to_delete_aux lbase i n = 
	if i > n then []
	else 
		if (for_all (fun base -> (hd base == None)) lbase) then 
					i::(columns_to_delete_aux (map tl lbase) (i+1) n)
			   else columns_to_delete_aux (map tl lbase) (i+1) n;;
			
	
(* now we have all the routines to delete the None cols in the bases or in the dags, just do it ! *)	
   
let tidy (ldag,lbase) = 
	 	let cols = columns_to_delete lbase in (* search for columns to delete *)
										let nldag = delete_dags_cols ldag cols (* delete them in dags *)
										and nlbase = delete_bases_cols lbase cols (*delete them in bases*) in
											(nldag,nlbase);;
let rec tidy_none_cols  lldagbasepair= 
	match lldagbasepair with 
		| [] -> []
		| (ldag,lbase)::q -> tidy(ldag,lbase)::(tidy_none_cols q);;
											
 
											 					
(* search the constant columns in the base *)				
let rec get_constant_elts_of_dag ldag ldag_length locvar =
	get_constant_elts_of_dag_aux ldag [] 1 ldag_length (hd ldag) locvar
and get_constant_elts_of_dag_aux ldag cst_elt n ldag_length ref_dag locvar =
	if n = ldag_length+1 then cst_elt
	else 
		let dag_col = (map hd ldag) in                                      
		let a = hd dag_col in
			if (contain_comm (direct_dependencies a) cst_elt) && (for_all (equal_dag_elt a) (tl dag_col)) && (no_loc_var_dag (a::ref_dag) locvar)  then
				get_constant_elts_of_dag_aux (map tl ldag) (n::cst_elt) (n+1) ldag_length ref_dag locvar
			else get_constant_elts_of_dag_aux (map tl ldag) cst_elt (n+1) ldag_length ref_dag locvar 
		
	                                            
(* reconstruct a template according to the ldag *)
let rec construct_template_se_var_with_dag_base ldag x xsq nsq lpos locvar = 
	let cst_elt = get_constant_elts_of_dag (map tl ldag) (length (hd ldag) -1) locvar in 
		let (t,lse,nx) = antiunif (map hd ldag) (x) in
			match construct_template_aux (map tl ldag) xsq nsq cst_elt 1 with
				| ConstT dbase,_ -> ((fun se -> (t se)::dbase),lse,nx),(nsq)
		      	| Templa (tbase,lseb,nxb),k -> ((fun (Pair(se1,se2)) -> (t se1)::(tbase se2)),(map2 (fun x y -> (Pair(x,y))) lse lseb), (Pairvar(nx,nxb))),k
				
		

(* (* convert the base as the list of its columns		 *)
let rec lbase_to_lcol lbase =
	if (length (hd lbase)) = 0 then []
	else
		(map hd lbase)::(lbase_to_lcol (map tl lbase)) *)

		 
(* (* returns the indexes of the columns that are greater than col in the base*)
      
let rec get_greater_cols_in_base col lbase ldag = 
	get_greater_cols_in_base_aux col lbase ldag 1 (length (hd lbase))
and get_greater_cols_in_base_aux col lbase ldag i n=
	if i > n then
		 []
	else
	   	let base_col = map hd lbase in
			if (geq_column base_col col ldag) then
				i::(get_greater_cols_in_base_aux col (map tl lbase) ldag (i+1) n)
			else                                                            
				(get_greater_cols_in_base_aux col (map tl lbase) ldag (i+1) n)
	
(* generate the list of greater cols for each column *)
let rec construct_geq_list lbase ldag =
	map (fun col -> get_greater_cols_in_base col lbase ldag) (lbase_to_lcol lbase)
	 *)
		         
(* transform a list of complete_temp into a fundag *)
(* see duplicate_checker.ml for the definition of complete templates *)
(* do the same thing that construct_template_se_var but starts from a complete template already computed *)
let rec complete_template_to_fundag temp ldag nsq =
	let TemplCT(t,lse,nx) = hd temp in 
	let tfun = temp_elt_to_fundag_elt t in
    match complete_template_to_fundag_aux (tl temp) (map tl ldag) nsq with
			| ConstT dbase,_ -> ((fun se -> (tfun se)::dbase),lse,nx),(nsq+1)
			| Templa (tbase,lseb,nxb),k ->
		  ((fun (Pair(se1,se2)) -> (tfun se1)::(tbase se2)),(map2 (fun x y -> (Pair(x,y))) lse lseb), (Pairvar(nx,nxb))),k
		
   
and complete_template_to_fundag_aux temp ldag nsq = 
	match temp with
		| [] -> (ConstT [],nsq)
		| (TemplCT (t,lse,nxsq))::_ ->
			 	let tfun = temp_elt_to_fundag_elt t in
				begin
					match complete_template_to_fundag_aux (tl temp) (map tl ldag) (nsq+1) with
		 					| ConstT dbase,_ -> Templa ((fun se -> (tfun se)::dbase),lse,nxsq),(nsq+1)
							| Templa (tbase,lseb,nx),k ->
						  Templa ((fun (Pair(se1,se2)) -> (tfun se1)::(tbase se2)),(map2 (fun x y -> (Pair(x,y))) lse lseb), (Pairvar(nxsq,nx))),k
				end
		 | (ConstCT a)::_ ->  		
				begin
					match complete_template_to_fundag_aux (tl temp) (map tl ldag) (nsq) with
					    | ConstT dbase,_ -> (ConstT (a::dbase)),nsq
					    | Templa (tbase,lse,nx),k ->
						Templa ((fun se -> a::(tbase se)),lse,nx),k
				end
					
		
(* same as antiunif but return the template element instead of the fundag *)
let extended_antiunif ldag_elt x =
  	let temp =  (greatest_temp_elt_list (map dag_elt_to_temp ldag_elt)) in 
  	let lse = map (fun x -> get_associated_tuple_elt_with_temp x temp) ldag_elt in
  	let nx = construct_tuple_var_from_temp_elt temp x in
    	(temp,lse,nx)
        
(* same as before : instead of the fundag, return a list of complete template *)

let rec construct_complete_template_aux ldag xsq nsq elt_const ndag = 
  match (hd ldag) with
    | [] -> ([],nsq)
	| a::q when mem ndag elt_const -> 
			   let (l_ctemp,k) = construct_complete_template_aux (map tl ldag) xsq nsq elt_const (ndag+1) in
					((ConstCT a)::l_ctemp),k
	| a::q ->  let (t,lse,nxsq) = extended_antiunif (map hd ldag) (Var (xsq^"_"^(string_of_int nsq))) in 
					let (l_ctemp,k) = construct_complete_template_aux (map tl ldag) xsq (nsq+1) elt_const (ndag+1) in
						( (TemplCT(t,lse,nxsq)) ::l_ctemp),k
	 
let rec construct_complete_template_se_var ldag x xsq nsq lpos locvar = 
	let cst_elt = get_constant_elts_of_dag (map tl ldag) (length (hd ldag) -1) locvar in 
		let (t,lse,nx) = extended_antiunif (map hd ldag) (x) in
			let (l_ctemp,k) = construct_complete_template_aux (map tl ldag) xsq nsq cst_elt 1 in
				( (TemplCT(t,lse,nx)) ::l_ctemp),k
			   

(* let rec construct_complete_template_from_temp_aux ltemp ldag xsq nsq elt_const ndag = 
	match ltemp with 
	 	| [] -> []
		| Constant a::q -> (ConstCT a)::construct_complete_template_from_temp_aux q (map tl ldag) xsq
      
let rec construct_template ldag x xsq nsq lpos elt_const =
  let t(* ,lse,nx) *) = extended_antiunif (map hd ldag) (x) in
    let (l_ctemp,k) = construct_template_aux (map tl ldag) xsq nsq elt_const 1 in
       ((TemplT(t(* ,lse,nx *)))::l_ctemp),k *)
                        


                     
(* replace the references to the pointer i by the pointer j *)
let rec exchange_pointer_in_dag_elt i j dag_elt=
	match dag_elt with
		| ExprD _ -> dag_elt
		| DivD(d1,d2) -> DivD(exchange_pointer_in_dag_elt i j d1,exchange_pointer_in_dag_elt i j d2)
		| PairD(d1,d2) -> PairD(exchange_pointer_in_dag_elt i j d1,exchange_pointer_in_dag_elt i j d2)
		| VecD(ls) -> VecD(map (fun (ai,li) -> (ai, map (exchange_pointer_in_dag_elt i j) li) )ls)
		| PtD(k) -> PtD(if k = i then j else k) 
				
	 

(* for a give template element, generate all the possible templates obtained by replacing pointers according to the geq_list list *)
(* the newly replaced pointers are made negative in the aux function so they could not be changed again*)
let rec generate_all_possible_dag_elt_by_exchanging_pointers geq_list dag_elt  = 
	let minus_temp_dag_list = generate_all_possible_dag_elt_by_exchanging_pointers_aux geq_list [dag_elt] 1 in
		map make_pointer_positive_in_dag_elt minus_temp_dag_list
and generate_all_possible_dag_elt_by_exchanging_pointers_aux geq_list ldag_elt i =
	match geq_list with
		| [] ->  ldag_elt
		| l::q -> let minus_l = map (~-) l in
					let nldag_elt = fold_left (fun acc j -> rev_append (map (exchange_pointer_in_dag_elt i j) ldag_elt) acc) [] minus_l in
						generate_all_possible_dag_elt_by_exchanging_pointers_aux q nldag_elt (i+1)


let rec generate_all_possible_dags_by_exchanging geq_list dag =
	match dag with
	| [] -> [[]]
	| _ ->
    let n_dagelts = generate_all_possible_dag_elt_by_exchanging_pointers geq_list (hd dag) in
	let nl_cdag_tl =  generate_all_possible_dags_by_exchanging geq_list (tl dag) in
	   fold_left (fun acc dag_elt -> rev_append (map (fun lt -> dag_elt::lt) nl_cdag_tl) acc)[] n_dagelts
	
let rec generate_all_ldags_by_exchanging geq_list ldag = 
  	let all_dags = map (generate_all_possible_dags_by_exchanging geq_list) ldag in
 		invert_list_of_list all_dags
	
let rec permute_pointers_in_ldags_list geq_list lldag =
	concat (map (generate_all_ldags_by_exchanging geq_list) lldag);;    
                                        

(* determine the number of duplication we might have to do *)
let rec number_of_pairs_in_dag_elt dag_elt = 
	match dag_elt with
		| PairD(d1,d2) -> (number_of_pairs_in_dag_elt d1) + (number_of_pairs_in_dag_elt d2)+1
		| _ -> 0;;

let determine_dag_length_for_unification ldag =                                  
	let d = number_of_pairs_in_dag_elt (hd (hd ldag)) in
	(fold_left (max) 0 (map length ldag)) + (min !Settings_vars.max_duplications d);; 

let extended_unification_for_length_break_sharing start_ldag x xsq nsq lpos locvar def_elt n =
	let lpermldag = all_permutations_n start_ldag n in (* construct all possible permutations *)
	let lpermldag_reduced = tidy_none_cols lpermldag in (* remove none columns *)
		let lnewldags = fold_right conc_no_repeat (map (fun (ldag,lbase) -> construct_all_dags_from_base lbase ldag lpos def_elt (length start_ldag)) lpermldag_reduced) []  in (* replace none placeholders *)
 		let ll_ptr_permuted_ldags = map (generate_ldags_by_breaking_sharing) lnewldags in
			let final_ldags = fold_right conc_no_repeat ll_ptr_permuted_ldags [] in
	 		let l_complete_templates = map (fun ldag -> (fst(construct_complete_template_se_var ldag x xsq nsq lpos locvar),ldag)) final_ldags in
			let l_complete_templates_useful = filter keep_complete_template_predicate l_complete_templates in 
			let l_complete_templates_unique = rebuild_template_set_commutativity l_complete_templates_useful in 
				map (fun (ct,ldag) -> complete_template_to_fundag ct ldag nsq) l_complete_templates_unique
			
				
let extended_unification_for_length_break_sharing_repeat start_ldag x xsq nsq lpos locvar def_elt n =
	let lpermldag = all_permutations_n start_ldag n in (* construct all possible permutations *)
	let lpermldag_reduced = tidy_none_cols lpermldag in (* remove none columns *)
		let lnewldags = concat (map (fun (ldag,lbase) -> construct_all_dags_from_base lbase ldag lpos def_elt (length start_ldag)) lpermldag_reduced)  in (* replace none placeholders *)
 		let ll_ptr_permuted_ldags = map (generate_ldags_by_breaking_sharing) lnewldags in
			let final_ldags = concat ll_ptr_permuted_ldags in
	 			   map (fun ldag -> construct_template_se_var_with_dag_base ldag x xsq nsq lpos locvar) final_ldags
	    


let best_template_of_the_world start_ldag x xsq nsq lpos locvar def_elt =
	let n = determine_dag_length_for_unification start_ldag in
	let lpermldag = all_permutations_n start_ldag n in (* construct all possible permutations *)
	let lpermldag_reduced = tidy_none_cols lpermldag in (* remove none columns *)
		let lnewldags = fold_right conc_no_repeat (map (fun (ldag,lbase) -> construct_all_dags_from_base lbase ldag lpos def_elt (length start_ldag)) lpermldag_reduced) []  in (* replace none placeholders *)
 		let ll_ptr_permuted_ldags = map (generate_ldags_by_breaking_sharing) lnewldags in
			let final_ldags = fold_right conc_no_repeat ll_ptr_permuted_ldags [] in
	 		let l_complete_templates = map (fun ldag -> (fst(construct_complete_template_se_var ldag x xsq nsq lpos locvar),ldag)) final_ldags in
			let l_complete_templates_useful = filter keep_complete_template_predicate l_complete_templates in 
			let l_complete_templates_unique = rebuild_template_set_commutativity l_complete_templates_useful in 
			(* we have done just like before, now we have to select the best template *)
			let (best_ct, best_ldag) = fst( fold_left (fun (a,mes) (c_temp,ldag) -> let new_measure = measure_complete_temp c_temp in 
																			if (mes > new_measure) then ((c_temp,ldag),mes)
																			else (a,mes)) (hd l_complete_templates_unique,measure_complete_temp (fst (hd l_complete_templates_unique)) ) (tl l_complete_templates_unique) 
								   		) in
								
			complete_template_to_fundag best_ct best_ldag nsq																
		;;



let add_template accumulator (tidied_ldag,tidied_base) x xsq nsq lpos locvar def_elt = 
	let acc_length = length !accumulator in
			let new_ldags = construct_all_dags_from_base tidied_base tidied_ldag lpos def_elt (length tidied_ldag)in
			print_endline ("break sharing "^(string_of_int (length new_ldags))^" templates ... ");
			(* iter dag_list_pretty_printer new_ldags; *)
			
			(* let l_ptr_permuted_dags_sets = (map (generate_ldags_by_breaking_sharing_set) new_ldags) in *)
			let l_ptr_permuted_dags = fold_right (conc_no_repeat) (map (fun ldag ->
				 	(* print_string "\n"; *)
					let ptr_permuted = generate_ldags_by_breaking_sharing ldag in
					(* print_endline ("permuted dags "^(string_of_int (length ptr_permuted))^"... "); *)
					ptr_permuted
					) new_ldags) [] in
 			(* print_endline ("Union sets list "^(string_of_int (length l_ptr_permuted_dags_sets))^"... ");		
   			let ptr_permuted_dags_set = (Hashset.multiple_union) l_ptr_permuted_dags_sets in 
			print_endline ("Convert set to list ");		
			let l_ptr_permuted_dags = Hashset.to_list ptr_permuted_dags_set in  *)
			
			(* print_endline ("get only uniques "^(string_of_int (length l_ptr_permuted_dags_non_unique))^"... "); *)
			(* let l_ptr_permuted_dags = delete_duplicates l_ptr_permuted_dags_non_unique in  *)
			(* iter dag_list_pretty_printer l_ptr_permuted_dags; *)
			print_endline ("construct templates "^(string_of_int (length l_ptr_permuted_dags))^"... ");
			let l_complete_templates = map (fun ldag -> (fst(construct_complete_template_se_var ldag x xsq nsq lpos locvar),ldag)) l_ptr_permuted_dags in   
		   	print_endline ("filter useless templates ...");
		 	let l_complete_templates_useful = filter keep_complete_template_predicate l_complete_templates in 
		  		(* accumulator := conc_no_repeat (l_complete_templates_useful) !accumulator; *)
				print_endline ("reduce the template accumulator ...");
			   	accumulator := add_templates_without_duplicates l_complete_templates_useful !accumulator;
				(* print_string ((string_of_int (length l_ptr_permuted_dags))^" templates found with all permutations    /   "); *)
				print_endline ((string_of_int (length !accumulator))^" templates found after reducing \n");
				
				if(acc_length <> (length !accumulator)) then
				 	begin
						print_endline ("number of templates changed : "^(string_of_int (length !accumulator))^"\n");
					end;
		    	flush stdout;
;;
	
let iterative_extended_unification start_ldag x xsq nsq lpos locvar def_elt n = 
	print_string (("Selected n : "^(string_of_int n))^"\n");
	let non_empty = non_empty_dags_indices start_ldag in
	let lgthpermutation = (length non_empty) in   
	print_string (("non empty length : "^(string_of_int lgthpermutation))^"\n"); flush stdout ;
	
	let lgthdag = (length start_ldag) in
	let perm_gen = new generator n lgthpermutation in
	let lbasopt = complete_bases_opt (map tl start_ldag) n in  (* contruct the new bases options *)
	let extended_dag = complete_dags start_ldag (n+1) in
	let ldag_accumulator = ref [] in
		begin
			try
	
				while true do
						let lperm = perm_gen#get_perms() in 
						
						let permuted_bases = apply_perms_to_bases lperm non_empty lbasopt in                       
							if (for_all left_dependencies permuted_bases) then (*dependencies are good -> *) 
								let nldag = apply_perms_to_dags lperm non_empty extended_dag in   (*modify the dags*)
								let (tidied_ldag,tidied_base) = tidy (nldag, permuted_bases) in  
						
					add_template ldag_accumulator (tidied_ldag,tidied_base) x xsq nsq lpos locvar def_elt;
			 			
		               perm_gen#next_permutation();
				done;
			with Last_permutation -> ()
		end;
		let l_complete_templates_unique = map snd !ldag_accumulator in  
			map (fun (ct,ldag) -> complete_template_to_fundag ct ldag nsq) l_complete_templates_unique      
;;
	

let add_template2 accumulator (tidied_ldag,tidied_base) x xsq nsq lpos locvar def_elt = 
	let acc_length = Hashset.size accumulator in
			let new_ldags = construct_all_dags_from_base tidied_base tidied_ldag lpos def_elt (length tidied_ldag) in
			(* print_endline ("break sharing ... "); *)
			(* let l_ptr_permuted_dags_sets = (map (generate_ldags_by_breaking_sharing_set) new_ldags) in *)
			iter (fun ldag -> Hashset.add_multiple accumulator (generate_ldags_by_breaking_sharing ldag)) new_ldags;
			(* print_string ("union newly generated by permutations dags : "^(string_of_int (length l_ptr_permuted_dags_sets))^" lists of "^(string_of_int (fold_left (fun sum set -> sum+(Hashset.size set)) 0 l_ptr_permuted_dags_sets))^" templates ..."); *)
			(* flush stdout; *)
			(* let ptr_permuted_dags_set = Hashset.union_to_list l_ptr_permuted_dags_sets in   *)
			(* print_endline ("union with accumulator ... "); *)
            (* Hashset.union_to_list (accumulator::l_ptr_permuted_dags_sets); *)
            (* print_endline (" done "); *)

    		(* if(acc_length <> (Hashset.size accumulator)) then
 				 	begin
 						print_endline ("number of ldags changed : "^(string_of_int (Hashset.size accumulator)));
 					end; *)
 		    	(* flush stdout; *)
;;

let iterative_extended_unification2 start_ldag x xsq nsq lpos locvar def_elt n =
	(* print_string ("Unify dags :"); *)
	(* dag_list_pretty_printer start_ldag; *)
	if !Settings_vars.verbose then print_string (("Selected n : "^(string_of_int n))^"\n");
	let non_empty = non_empty_dags_indices start_ldag in
	let lgthpermutation = (length non_empty) in   

	if !Settings_vars.verbose then begin
		print_string (("non empty length : "^(string_of_int lgthpermutation))^"\n"); flush stdout ;
		print_endline("Computing templates : ");              
	end;
	
	let lgthdag = (length start_ldag) in
	let perm_gen = new generator n lgthpermutation in
	let n_perm = power (fact n) lgthpermutation in
	let i = ref 1 in 
	let lbasopt = complete_bases_opt (map tl start_ldag) n in  (* contruct the new bases options *)
	let extended_dag = complete_dags start_ldag (n+1) in
	let ldag_accumulator = Hashset.create 211 in
		begin
			try
	
				while true do
						let lperm = perm_gen#get_perms() in 
						(* print_newline ();                                 *)
						print_string ("\rPermutation :     ");
						progress_bar_printer (!i) (n_perm);
						print_string (" "^(string_of_int !i)^"/"^(string_of_int n_perm));
						flush stdout;
						(* print_endline ("search new template ... "); *)
						
						let permuted_bases = apply_perms_to_bases lperm non_empty lbasopt in                       
							if (for_all left_dependencies permuted_bases) then 
							begin(*dependencies are good -> *)
								let nldag = apply_perms_to_dags lperm non_empty extended_dag in   (*modify the dags*)
								let (tidied_ldag,tidied_base) = tidy (nldag, permuted_bases) in  
								add_template2 ldag_accumulator (tidied_ldag,tidied_base) x xsq nsq lpos locvar def_elt;
				            end;
						
			 			
		               perm_gen#next_permutation();
					   i := !i+1;
				done;
			with Last_permutation -> ()
		end;

		if !Settings_vars.very_verbose then begin 
			print_string "\nAccumulator informations : \n";
			Hashset.print_properties ldag_accumulator;
		end;     

		(* print_endline ("construct templates "^(string_of_int (ldag_accumulator.size))^"... "); *)
		if !Settings_vars.verbose then begin
		   	print_string ("\n");
			print_string ("Construct "^(string_of_int (Hashset.size ldag_accumulator))^" templates ...\n");
		end;
		let l_ptr_permuted_dags = Hashset.to_list ldag_accumulator in  
		let l_complete_templates = rev_map (fun ldag -> (fst(construct_complete_template_se_var ldag x xsq nsq lpos locvar),ldag)) l_ptr_permuted_dags in   
		
		if !Settings_vars.verbose then begin
			print_string ("Filter useless templates ...\n");
		end;
		
		let l_complete_templates_useful = filter keep_complete_template_predicate l_complete_templates in 

		if !Settings_vars.verbose then begin
			print_string ("Search unique templates modulo commutativity ...\n");
		end;
		let l_complete_templates_unique = rebuild_template_set_commutativity l_complete_templates_useful in 
		
		if !Settings_vars.verbose then begin
			print_string ((string_of_int (length l_complete_templates_unique))^" templates found after reducing \n\n");
			flush stdout;	
		end;
		
		rev_map (fun (ct,ldag) -> complete_template_to_fundag ct ldag nsq) l_complete_templates_unique

;;


(* reinsert in the non_empty list the dags whose indices in start_ldag are not in non_empty_indices*)
let rec reinsert_empty_dags start_ldag non_empty non_empty_indices =
	reinsert_empty_dags_aux start_ldag non_empty non_empty_indices 0 (length (hd non_empty))
and reinsert_empty_dags_aux start_ldag non_empty non_empty_indices pos l =
	match non_empty_indices with
	| [] -> map (fun dag -> complete_dag dag l) start_ldag 
	| i::q when i = pos -> (hd non_empty)::(reinsert_empty_dags_aux (tl start_ldag) (tl non_empty) q (pos+1)) l
	| i::q -> (complete_dag (hd start_ldag) l)::(reinsert_empty_dags_aux (tl start_ldag) non_empty non_empty_indices (pos+1)) l
;;

(* the real ldag generator *)
(* generates all the dags by filling the indeterminates and breaking sharing (changing the pointers) and add them to the accumulator (a hashset)*)
let add_template3 accumulator (tidied_ldag,tidied_base) x xsq nsq lpos locvar def_elt original_ldag_length= 
	let acc_length = Hashset.size accumulator in
			let new_ldags = construct_all_dags_from_base tidied_base tidied_ldag lpos def_elt original_ldag_length in
			iter (fun ldag -> Hashset.add_multiple accumulator (generate_ldags_by_breaking_sharing ldag)) new_ldags;
;;

let iterative_extended_unification3 start_ldag x xsq nsq lpos locvar def_elt n =
	if !Settings_vars.verbose then print_string (("Selected n : "^(string_of_int n))^"\n");
	
	let non_empty = non_empty_dags start_ldag in (* get the dags with square roots *)
	if length non_empty = 0 then (* There is no square root, just find a simple common template *) 
	[construct_template_se_var start_ldag x xsq nsq lpos locvar]
	else  
	begin
		let non_empty_indices = non_empty_dags_indices start_ldag in (* compute the indices of these dags in the start_ldag list *)
		let lgthpermutation = (length non_empty) in (* get the number of them, as we permute only non empty dags, it will be the number of permutations to consider *)  

		if !Settings_vars.verbose then begin
			print_string (("non empty length : "^(string_of_int lgthpermutation))^"\n"); flush stdout ;
			print_endline("Computing templates : ");              
		end;
	
		let perm_gen = new generator n lgthpermutation in (* create the permutations generator for the non empty dag list *)
		let n_perm = power (fact n) lgthpermutation in (* the number of inner permutations to consider *)
		let i = ref 1 in (* i is the index of the inner permutation *) 
		
		let lbasopt = complete_bases_opt (map tl non_empty) n in  (* contruct the new bases options ie add indeterminates so all the bases options have the same length *)
		let extended_dag = complete_dags non_empty (n+1) in (* and do the same things for the dags *)
		
		let ldag_accumulator = Hashset.create 211 in (* create the Hashset which will contain all the generated ldags *)
			begin
				try
	
					while true do
							let lperm = perm_gen#get_perms() in (* get the permutation from the generator *)
							print_string ("\rPermutation :     ");
							progress_bar_printer (!i) (n_perm);
							print_string (" "^(string_of_int !i)^"/"^(string_of_int n_perm));
							flush stdout;
						
							let permuted_bases = apply_perms_to_all_bases lperm lbasopt in                       
								if (for_all left_dependencies permuted_bases) then 
								begin(*dependencies are good -> *)
									let nldag = apply_perms_to_all_dags lperm extended_dag in   (* modify the dags - permute them *)
									let (tidied_ldag,tidied_base) = tidy (nldag, permuted_bases) in (* remove indeterminates columns in the bases and dags *)
									
									(* generate all the dags by filling indeterminates and exchanging pointers*)  
									add_template3 ldag_accumulator (tidied_ldag,tidied_base) x xsq nsq lpos locvar def_elt (length start_ldag); 
					            end;
						
			 			
			               perm_gen#next_permutation(); (* switch to the next permutation ... *)
						   i := !i+1; (* ... and increment the counter *)
					done;
				with Last_permutation -> () (* all permutations enumerated, we have to stop our search *)
			end;

			if !Settings_vars.very_verbose then begin 
				print_string "\nAccumulator informations : \n";
				Hashset.print_properties ldag_accumulator;
			end;     

			if !Settings_vars.verbose then begin
			   	print_string ("\n");
				print_string ("Construct "^(string_of_int (Hashset.size ldag_accumulator))^" templates ...\n");
			end;
			
			let l_ptr_permuted_dags = Hashset.to_list ldag_accumulator in (* transform the hashset in a list *)                      
			(* reinsert the empty dags we removed before *)
			let reinserted_dags_list = map (fun ldag -> reinsert_empty_dags start_ldag ldag non_empty_indices) l_ptr_permuted_dags in 
	  		(* and create the templates (not yet fundags so we can compare them) *)
			let l_complete_templates = rev_map (fun ldag -> (fst(construct_complete_template_se_var ldag x xsq nsq lpos locvar),ldag)) reinserted_dags_list in   
		
			if !Settings_vars.verbose then begin
				print_string ("Search unique templates modulo commutativity ...\n");
			end;
		    
   			(* keep only the smallest templates (in the sense of inclusion) *)
			let l_complete_templates_unique = rebuild_template_set_commutativity l_complete_templates in 

			if !Settings_vars.verbose then begin
				print_string ((string_of_int (length l_complete_templates_unique))^ " small templates\n");
				print_string ("Filter useless templates ...\n");
			end;
			
		    (* remove templates for which there exist a column which is not pointed *)  	
			let l_complete_templates_useful = filter keep_complete_template_predicate l_complete_templates_unique in 
		
			if !Settings_vars.verbose then begin
				print_string ((string_of_int (length l_complete_templates_useful))^" templates found after reducing \n\n");
				flush stdout;	
			end;
		    
			(* transform the templates to fundags *)
			rev_map (fun (ct,ldag) -> complete_template_to_fundag ct ldag nsq) l_complete_templates_useful
	end;
;;
	

	let rec construct_all_templates_se_var ldag x xsq nsq lpos locvar = 
		let n = determine_dag_length_for_unification ldag in
		iterative_extended_unification3 ldag x xsq nsq lpos locvar (!default_sq) (n-1);;

	