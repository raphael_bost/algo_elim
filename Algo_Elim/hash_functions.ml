open Dag
open Language
open List
                         
let uop_hash uop = 
	match uop with
	| Sqrt -> -2
	| Umin -> -1
  	| Neg -> -1

let uop_hash_fun uop = 
	match uop with
	| Sqrt -> fun i -> 1 lsl i
	| Umin -> (~-)
  	| Neg -> lnot

let bop_hash_fun bop =
	match bop with 
	| Plus -> (+)
	| Mult -> ( * )
	| Div -> (fun a b -> a*(-b)) 
  	| And -> ( * )
 	| Or -> ( + ) 
  	| Neq -> (fun a b -> a*(-b)) 
	| Eq -> (-)
  	| Gt -> (fun a b -> a - (b+1))
 	| Geq -> ( - )
	| Lt  -> (fun a b -> b - (a+1))
	| Leq -> (fun a b -> b - a)
   
 
let rec program_hash p =
	match p with 
	| Value s -> String.length s
  	| Const(Num f) -> int_of_float f
	| Const(Bool f) -> if f then 1 else 0
  	| UOp(uop,p1) -> (uop_hash uop)*(program_hash p1)
  	| BOp(bop ,p1,p2) -> (bop_hash_fun bop) (program_hash p1) (program_hash p2)
  	
  	| Pair(p1,p2) -> (program_hash p1)*(program_hash p2)
  	| Fst(p1) -> program_hash p 
  	| Snd(p1) -> program_hash p
  	| _ -> invalid_arg "Accepts only numerical expressions"
;;

let rec program_hash2 p =
	match p with 
	| Value s -> (String.length s)*(int_of_char s.[0])
  	| Const(Num f) -> int_of_float f
	| Const(Bool f) -> if f then 0 else 1
  	| UOp(uop,p1) -> (uop_hash_fun uop) (program_hash2 p1)
  	| BOp(bop ,p1,p2) -> (bop_hash_fun bop) (program_hash2 p1) (program_hash2 p2)
  	
  	| Pair(p1,p2) -> -(program_hash2 p1)*(program_hash2 p2)
  	| Fst(p1) -> program_hash2 p 
  	| Snd(p1) -> 2*(program_hash2 p)
  	| _ -> invalid_arg "Accepts only numerical expressions"
;;
   
let rec dag_elt_hash dag_elt =
	match dag_elt with
	| ExprD p -> program_hash2 p  
  	| VecD([]) -> 1
 	| VecD((ai,li)::q) -> ((program_hash2 ai)*(fold_left (fun prod d -> (dag_elt_hash d)*prod) 1 li))  + 4*dag_elt_hash (VecD q)
  	| DivD(d1,d2) -> dag_elt_hash d1 + 3*(dag_elt_hash d2)
  	| PtD i -> i
  	| PairD(d1,d2) -> dag_elt_hash d1 + 2*(dag_elt_hash d2) 
  	| EmptyD -> 1
;;         

let rec dag_elt_hash2 dag_elt =
	match dag_elt with
	| ExprD p -> program_hash p  
  	| VecD([]) -> 42
 	| VecD((ai,li)::q) -> (-(program_hash ai)*(fold_left (fun prod d -> (dag_elt_hash2 d)*(prod-1)) 1 li))  + 4*dag_elt_hash2 (VecD q)
  	| DivD(d1,d2) -> -3*(dag_elt_hash2 d1)*(dag_elt_hash2 d2)
  	| PtD i -> 1 lsr (i mod 32)
  	| PairD(d1,d2) -> 2*(dag_elt_hash2 d1)*((dag_elt_hash2 d2)-666) 
  	| EmptyD -> 1
;;         

let rec dag_hash dag =
	match dag with
	| [] -> 1
	| a::q -> (dag_elt_hash a)+(dag_hash q)
;; 

let rec dag_hash2 dag =
	match dag with
	| [] -> 1
	| a::q -> (dag_elt_hash a)+5*(dag_hash2 q)
;; 

let rec ldag_hash ldag = 
	match ldag with
	| [] -> 0
	| [dag] ->  (dag_hash dag)
	| dag::q -> (dag_hash dag)*(ldag_hash q)
;; 

let rec ldag_hash2 ldag = 
	match ldag with
	| [] -> 0
	| [dag] ->  (dag_hash2 dag)
	| dag::q -> 3*((dag_hash2 dag)+2)*(ldag_hash2 q)
;; 

             
(* Very very slow but better than the previous ones so far *)
let ldag_hash3 ldag = 
	Hashtbl.hash (Extended_pretty_printer.dag_list_to_string ldag)
;;
                    
let rec dag_elt_hash4 dag_elt =
	match dag_elt with
	| ExprD p -> program_hash2 p  
  	| VecD([]) -> 1
 	| VecD((ai,li)::q) -> ((program_hash2 ai)*(fold_left (fun prod d -> (dag_elt_hash4 d)+19*prod) 0 li))  + 13*dag_elt_hash4 (VecD q)
  	| DivD(d1,d2) -> dag_elt_hash4 d1 + 23*(dag_elt_hash4 d2)
  	| PtD i -> i
  	| PairD(d1,d2) -> dag_elt_hash4 d1 + 97*(dag_elt_hash4 d2) 
  	| EmptyD -> 11
;;         


let rec dag_hash4 dag =
	match dag with
	| [] -> 1
	| a::q -> (dag_elt_hash4 a)+33*(dag_hash4 q)
;; 

let rec ldag_hash4 ldag = 
	match ldag with
	| [] -> 0
	| [dag] ->  (dag_hash4 dag)
	| dag::q -> (dag_hash4 dag)+31*(ldag_hash4 q)
;; 

(* module HashLDagType:HashedType = struct
	type t = Dag.dag list
	let equal = (=)
	let hash = ldag_hash
end;;

module LDagHashset = Hashset.Make (HashLDagType);; *)
