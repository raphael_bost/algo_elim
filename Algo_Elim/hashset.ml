(**************************************************************************)
(*                                                                        *)
(*  Copyright (C) Jean-Christophe Filliatre                               *)
(*                                                                        *)
(*  This software is free software; you can redistribute it and/or        *)
(*  modify it under the terms of the GNU Library General Public           *)
(*  License version 2.1, with the special exception on linking            *)
(*  described in file LICENSE.                                            *)
(*                                                                        *)
(*  This software is distributed in the hope that it will be useful,      *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *)
(*                                                                        *)
(**************************************************************************)
             
(* Sets as hash tables. Code adapted from ocaml's Hashtbl. *)

(* We do dynamic hashing, and resize the table and rehash the elements
   when buckets become too long. *)
                           
(* let performance_injective_hashcode_flag = ref false;; (* set to true if you want performance enhancement in counterpart to less exhaustivity *) *)

(* Modified version of the original Hashset code : we know use the ldag hash functions *)
(* This new Hashset version can only contain ldags *)
                 
let hash_function_index = ref 4;;
let hashf ldag = 
 	match !Settings_vars.hash_function_index with
	| 1 -> Hash_functions.ldag_hash  ldag
	| 2 -> Hash_functions.ldag_hash2 ldag
	| 3 -> Hash_functions.ldag_hash3 ldag
	| 4 -> Hash_functions.ldag_hash4 ldag
;;
		
		

let modulo a b =
	let m = a mod b in
	if m <0 then m+b
	else m
;;
    
let debug_option = true;;

type 'a t =
  { mutable size: int;            (* number of elements *)
	mutable add_count: int;		  (* number of add attempts *)
    mutable data: 'a list array } (* the buckets *)

let create initial_size =
  let s = min (max 1 initial_size) Sys.max_array_length in
  { size = 0; add_count = 0; data = Array.make s [] }

let clear h =
  for i = 0 to Array.length h.data - 1 do
    h.data.(i) <- []
  done;
  h.size <- 0

let copy h =
  { size = h.size;
	add_count = h.add_count;
    data = Array.copy h.data }
     
let depth h = 
	Array.fold_left (fun m l -> max m (List.length l)) 0 h.data
;;

let depth_index h = 
	let (max,index,_) = Array.fold_left (fun (m,max_i,i) l -> if (List.length l) > m then  
	 									  	(List.length l,i,i+1)
										else (m,max_i,i+1)
									) (0,-1,0) h.data
	in (max,index)
;;

let number_of_collisioned_buckets k h = 
	Array.fold_left (fun i l -> if (List.length l)>=k then i+1 else i) 0 h.data
;;
let number_of_exact_collisioned_buckets k h = 
	Array.fold_left (fun i l -> if (List.length l)=k then i+1 else i) 0 h.data
;;

let number_of_empty_buckets h = 
	Array.fold_left (fun i l -> if (List.length l)=0 then i+1 else i) 0 h.data
;;
 
let print_properties tbl =
	print_endline ("\nInfo for size "^(string_of_int tbl.size));
	let (m,i) = depth_index tbl in                                                                
	let k = 4 in 
	print_endline ("number of  buckets : "^(string_of_int (Array.length tbl.data))^"");
	print_endline ("hashset depth : "^(string_of_int (m))^" for hash index "^(string_of_int i));
	let non_empty_count = (number_of_collisioned_buckets 1 tbl) in
	let k_count = (number_of_collisioned_buckets k tbl) in
	let exact_c_count = number_of_exact_collisioned_buckets 1 tbl in 
	let c_count = number_of_collisioned_buckets 2 tbl in 
	print_string ("number of non empty buckets : "^(string_of_int non_empty_count)^" ");
	print_endline(" proportion : "^(string_of_float ((float_of_int non_empty_count)/.(float_of_int (Array.length tbl.data))) ));

	print_string ("number of collisioned buckets : "^(string_of_int c_count)^" ");
	print_string(" proportion : "^(string_of_float ((float_of_int c_count)/.(float_of_int (Array.length tbl.data))) ));
	print_endline(" / non empty : "^(string_of_float ((float_of_int c_count)/.(float_of_int non_empty_count)) ));

	print_string ("number of non-collisioned buckets : "^(string_of_int exact_c_count)^" ");
	print_string(" proportion : "^(string_of_float ((float_of_int exact_c_count)/.(float_of_int (Array.length tbl.data))) ));
	print_endline(" / non empty : "^(string_of_float ((float_of_int exact_c_count)/.(float_of_int non_empty_count)) ));

	print_string ("number of "^(string_of_int k)^"-collisioned buckets : "^(string_of_int k_count)^" ");
	print_string(" proportion : "^(string_of_float ((float_of_int k_count)/.(float_of_int (Array.length tbl.data))) ));

	print_endline(" / non empty : "^(string_of_float ((float_of_int k_count)/.(float_of_int non_empty_count)) ));
	print_string ("number of empty buckets : "^(string_of_int (number_of_empty_buckets tbl)));
	print_endline(" proportion : "^(string_of_float ((float_of_int (number_of_empty_buckets tbl))/.(float_of_int (Array.length tbl.data))) ))
;;
	     
let resize hashfun tbl =
  let odata = tbl.data in
  let osize = Array.length odata in
  (* let nsize = min (2 * osize + 1) Sys.max_array_length in *)
  let nsize = min (Prime_generator.prime_between (2*osize +1) (3*osize)) Sys.max_array_length in
  if nsize <> osize then begin
	(* print_endline ("\nresize hashset from "^(string_of_int (osize))^" to "^(string_of_int (nsize))^" ... "); *) 

	if !Settings_vars.very_verbose then 
		begin
			print_endline "\nResize hashset\n";
			print_properties tbl; 
		end;

    let ndata = Array.create nsize [] in
    let rec insert_bucket = function
        [] -> ()
      | key :: rest ->
          insert_bucket rest; (* preserve original order of elements *)
          let nidx = modulo (hashfun key) nsize in
          ndata.(nidx) <- key :: ndata.(nidx) in
    for i = 0 to osize - 1 do
      insert_bucket odata.(i)
    done;
    tbl.data <- ndata;

	if !Settings_vars.very_verbose then begin print_properties tbl; end
	(* print_newline(); *)
	(* print_endline ("Resizing ended"); *)
  end

let unique_predicate key bucket = 
	if !Settings_vars.performance_injective_hashcode_flag then
		(List.length bucket) = 0 (* the devil is with us ! don't add if the hash key already exists *)
    else not (List.mem key bucket)
;;

let resize_predicate h = 
	if !Settings_vars.performance_injective_hashcode_flag then
		3*h.size > 2*(Array.length h.data)
	else (h.size > Array.length h.data lsl 1)
		
let add h key =
  h.add_count <- h.add_count +1;
	if !Settings_vars.very_verbose then 
 		print_string ("\r" ^ (string_of_int h.add_count)^" insertion attempts");

  let i = modulo (hashf key) (Array.length h.data) in
  let bucket = h.data.(i) in
  if unique_predicate key bucket then begin
    h.data.(i) <- key :: bucket;
    h.size <- succ h.size;
    if (resize_predicate h) then resize hashf h;
	(* else if (h.size mod 5000) = 0 then 	begin print_properties h; (* print_newline(); *) end *)
	
  end;
  	if !Settings_vars.very_verbose then 
		print_string (" for "^(string_of_int h.size)^" elements inserted");
;;
let rec add_multiple h l_key = 
	match l_key with
	| [] -> ()
	| key::q -> add h key; add_multiple h q
;;

let add_duplicate h key =
  let i = modulo (hashf key) (Array.length h.data) in
  let bucket = h.data.(i) in
    h.data.(i) <- key :: bucket;
    h.size <- succ h.size;
    if h.size > Array.length h.data lsl 1 then resize hashf h

let remove h key =
  let rec remove_bucket = function
      [] ->
        []
    | k :: next ->
        if k = key
        then begin h.size <- pred h.size; next end
        else k :: remove_bucket next in
  let i = modulo (hashf key) (Array.length h.data) in
  h.data.(i) <- remove_bucket h.data.(i)

let mem h key =
  List.mem key h.data.(modulo (hashf key) (Array.length h.data))

let cardinal h =
  let d = h.data in
  let c = ref 0 in
  for i = 0 to Array.length d - 1 do
    c := !c + List.length d.(i)
  done;
  !c

let iter f h =
  let d = h.data in
  for i = 0 to Array.length d - 1 do
    List.iter f d.(i)
  done

let fold f h init =
  let rec do_bucket b accu =
    match b with
      [] ->
        accu
    | k :: rest ->
        do_bucket rest (f k accu) in
  let d = h.data in
  let accu = ref init in
  for i = 0 to Array.length d - 1 do
    accu := do_bucket d.(i) !accu
  done;
  !accu            

let to_list h = 
	fold (fun a q -> a::q) h []
	(* Array.fold_left (@) [] h.data *)
	
let union h1 h2 = 
	let h = copy h1 in
	iter (add h) h2;
	h
;; 

let union_to h h2 =
	iter (add h) h2
;;
	

let size h = h.size
;;
let rec max_depth h =
	let max = ref 0 in 
  	let d = h.data in
  	for i = 0 to Array.length d - 1 do
    	if(List.length d.(i) > !max) then max := List.length d.(i)
	done;
	!max

let multiple_union h_list = 
	match h_list with 
	| [] -> invalid_arg "Empty set list"
	| h::q -> let u = copy h in
				List.iter (fun hashset -> iter (add u) hashset) q;
				u
;;

let rec union_to_list h_list =  			
	match h_list with
	| [] -> invalid_arg "Empty set list"
	| [h] -> (* print_endline ("Done") *)();
	| h::a::q -> 
		(* print_endline ("Union with : "^(string_of_int a.size)^" max depth : "^ (string_of_int (max_depth a))); *)
    			 union_to h a;
				 union_to_list (h::q)   
;;

let brutal_union_to h h2 =
	iter (add_duplicate h) h2
;;

let rec brutal_union_to_list hashset h_list = 
	match h_list with
	|[] -> ();
	|a::q -> print_endline ("Brutal Union with : "^(string_of_int a.size));
			 brutal_union_to hashset a;
			 brutal_union_to_list hashset q
;; 

let rec add_list_to_set hashset l = 
	match l with
	| [] -> hashset
	| a::q -> (add hashset a); 
		add_list_to_set hashset q
;;

(* Functorial interface *)

module type HashedType =
  sig
    type t
    val equal: t -> t -> bool
    val hash: t -> int
  end

module type S =
  sig
    type elt
    type t
    val create: int -> t
    val clear: t -> unit
    val copy: t -> t
    val add: t -> elt -> unit
    val remove: t -> elt -> unit
    val mem : t -> elt -> bool
    val cardinal: t -> int
    val iter: (elt -> unit) -> t -> unit
    val fold: (elt -> 'a -> 'a) -> t -> 'a -> 'a
  end

module Make(H: HashedType): (S with type elt = H.t) =
  struct
    type elt = H.t
    type set = elt t
    type t = set
    let create = create
    let clear = clear
    let copy = copy

    let safehash key = (H.hash key) land max_int

    let rec mem_in_bucket key = function
      | [] -> false
      | x :: r -> H.equal key x || mem_in_bucket key r

    let add h key =
      let i = modulo (safehash key) (Array.length h.data) in
      let bucket = h.data.(i) in
      if not (mem_in_bucket key bucket) then begin
	h.data.(i) <- key :: bucket;
	h.size <- succ h.size;
	if h.size > Array.length h.data lsl 1 then resize safehash h
      end
    
    let add_duplicate h key =
      let i = modulo (safehash key) (Array.length h.data) in
      let bucket = h.data.(i) in
	h.data.(i) <- key :: bucket;
	h.size <- succ h.size;
	if h.size > Array.length h.data lsl 1 then resize safehash h
	
    let remove h key =
      let rec remove_bucket = function
          [] ->
            []
        | k :: next ->
            if H.equal k key
            then begin h.size <- pred h.size; next end
            else k :: remove_bucket next in
      let i = modulo (safehash key) (Array.length h.data) in
      h.data.(i) <- remove_bucket h.data.(i)

    let mem h key =
      mem_in_bucket key h.data.(modulo (safehash key) (Array.length h.data))

    let cardinal = cardinal

    let iter = iter
    let fold = fold 
    
	let union_to h h2 =
		iter (add h) h2
	
	let multiple_union h_list = 
		match h_list with 
		| [] -> invalid_arg "Empty set list"
		| h::q -> let u = copy h in
				List.iter (fun hashset -> iter (add u) hashset) q;
				u

	let rec union_to_list h_list =  			
		match h_list with
		| [] -> invalid_arg "Empty set list"
		| [h] -> print_endline ("Done");
		| h::a::q -> print_endline ("Union with : "^(string_of_int a.size)^" max depth : "^ (string_of_int (max_depth a)));
	    			 union_to h a;
					 union_to_list (h::q)   

	let brutal_union_to h h2 =
		iter (add_duplicate h) h2

	let rec brutal_union_to_list hashset h_list = 
		match h_list with
		|[] -> ();
		|a::q -> print_endline ("Brutal Union with : "^(string_of_int a.size));
				 brutal_union_to hashset a;
				 brutal_union_to_list hashset q
		
  end
