open Language;;
open List;;
open Tools;;

(* different kinds of objects a program can compute *)

type interpreter_result = 
  | NumI of float 
  | BoolI of bool 
  | PairI of interpreter_result * interpreter_result 


(* exception that can happen at execution *)
exception Identical_variable;;
exception Pairvar_error;;
exception Undefined_variable of uvar;;

(* compute the minimum positive number and the maximum negative one *)
let minpos e1 e2 =
  match e1>=0, e2>=0 with
    | true,true -> min e1 e2
    | false,true -> e2
    | true, false -> e1
    | false,false -> e2
and maxneg e1 e2 =
  match e1<=0, e2<=0 with
    | true,true -> max e1 e2
    | false,true -> e2
    | true, false -> e1
    | false,false -> e2;;

(* compute the minimum positive exponent of a tuple of floats *)

let rec minpos_tuple e =
    minpos_tuple_aux e (-1)
and minpos_tuple_aux e mn =
  match e with
    | NumI f -> let (m,e)=frexp f in minpos e mn
    | PairI (e1,e2) ->
	minpos_tuple_aux e2 (minpos_tuple_aux e1 mn);;

(* Compute the maximum negative exponent of a tuple of floats *)

let rec maxneg_tuple e =
  maxneg_tuple_aux e 1 
and maxneg_tuple_aux e mx =
  match e with
    | NumI f -> let (m,e)=frexp f in maxneg e mx
    | PairI (e1,e2) ->
	maxneg_tuple_aux e2 (maxneg_tuple_aux e1 mx);;

(* withdraw k to all exponents of a tuple of floats*)

let rec divide_tuple e k =
  match e with 
    | NumI f -> let (m,exp)=frexp f in NumI (ldexp m (exp-k))
    | PairI (e1,e2) -> PairI (divide_tuple e1 k, divide_tuple e2 k);;

(* definition of the clear instruction behaviour*)

let rec clean_instruction e = 
  let maxn,minp = (maxneg_tuple e, minpos_tuple e) in
    match (maxn <= 0, minp>= 0) with
      | true,true -> e
      | false,true -> divide_tuple e minp
      | true,false -> divide_tuple e maxn
      | false,false -> e

(* compute the interpretation of a program *)

let rec interpreter p env =
  match p with
    | Value v -> 
	begin
	  try assoc v env with
	    | Not_found -> raise (Undefined_variable v)
	end
    | Const (Num f) -> NumI f
    | Const (Bool b) -> BoolI b
    | BOp (Plus,e1,e2) ->
	let (NumI ei1,NumI ei2) = (interpreter e1 env,interpreter e2 env) in NumI (ei1 +. ei2)
    | BOp (Mult,e1,e2)->
	let (NumI ei1,NumI ei2) = (interpreter e1 env,interpreter e2 env) in NumI (ei1 *. ei2)
    | BOp (Div,e1,e2)->
	let (NumI ei1,NumI ei2) = (interpreter e1 env,interpreter e2 env) in NumI (ei1 /. ei2)
    | UOp (Umin,e1) ->
	let (NumI ei1) = interpreter e1 env in NumI (-. ei1)
    | UOp (Sqrt,e1) ->
	let (NumI ei1) = interpreter e1 env in NumI (sqrt ei1)
    | BOp (Gt,e1,e2) ->
	let (NumI ei1,NumI ei2) = (interpreter e1 env,interpreter e2 env) in BoolI (ei1 > ei2)
    | BOp (Lt,e1,e2) ->
      let (NumI ei1,NumI ei2) = (interpreter e1 env,interpreter e2 env) in BoolI (ei1 < ei2)
    | BOp (Eq,e1,e2) ->
	let (NumI ei1,NumI ei2) = (interpreter e1 env,interpreter e2 env) in BoolI (ei1 = ei2)
    | BOp (Geq,e1,e2) ->
	let (NumI ei1,NumI ei2) = (interpreter e1 env,interpreter e2 env) in BoolI (ei1 >= ei2)
    | BOp (Leq,e1,e2) ->
	let (NumI ei1,NumI ei2) = (interpreter e1 env,interpreter e2 env) in BoolI (ei1 <= ei2)
    | BOp (Neq,e1,e2) ->
	let (NumI ei1,NumI ei2) = (interpreter e1 env,interpreter e2 env) in BoolI (ei1 != ei2)
    | UOp (Neg,e1) ->
	let (BoolI ei1) = interpreter e1 env in BoolI (not ei1)
    | BOp (And,e1,e2) ->
	let (BoolI ei1,BoolI ei2) = (interpreter e1 env,interpreter e2 env) in BoolI (ei1 && ei2)
    | BOp (Or,e1,e2) ->
	let (BoolI ei1,BoolI ei2) = (interpreter e1 env,interpreter e2 env) in BoolI (ei1 || ei2)
    | Pair (e1,e2) -> 
	PairI (interpreter e1 env,interpreter e2 env)
    | Fst a ->
	let (PairI (ei1,_)) = interpreter a env in ei1
    | Snd a ->
	let (PairI (_,ei1)) = interpreter a env in ei1
    | Letin (v,e1,e2) ->
	let env2 = add_def_env v (interpreter e1 env) env in interpreter e2 env2
    | If (f,e1,e2) ->
	let (BoolI b) = interpreter f env in 
	  (if b then interpreter e1 env else interpreter e2 env)
    | Clean e -> clean_instruction (interpreter e env)
and add_def_env_aux v ie =
  match v with
    | Var s -> [(s, ie)]
    | Pairvar (v1,v2) ->
	match ie with
	  | PairI (ie1,ie2) -> 
	      let (env1,env2) = (add_def_env_aux v1 ie1, add_def_env_aux v2 ie2) in
		fold_right 
		  (fun (var,value) en -> 
		     if mem_assoc var en 
		     then raise Identical_variable
		     else ((var,value)::en)
		  )
		  env1
		  env2
	  | _ -> raise Pairvar_error
and add_def_env v ie env =
  let nenv = add_def_env_aux v ie in
    fold_right
      (fun (var,value) newenv -> (var,value)::(remove_assoc var newenv))
      nenv
      env;;


let rec get_environnement e l = 
  match e with
    | BOp (Eq,Value x,c) -> (x, get_const_t c)::l
    | BOp (And,e1,e2) -> conc_no_repeat (get_environnement e1 []) (get_environnement e2 l)
and get_const_t c =
  match c with
    | Const (Num e) -> NumI e
    | Const (Bool b) -> BoolI b
    | Pair(e1,e2) -> PairI(get_const_t e1, get_const_t e2)
