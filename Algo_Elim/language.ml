(* type of the variables *)

type uvar = string

type var = 
  | Var of uvar
  | Pairvar of var * var

(* Definitions of the constants *)

type constants =
  | Num of float
  | Bool of bool

type num_const = float
type bool_const = bool

(* The unary operators *)
type unop = 
  | Sqrt | Umin 
  | Neg

let is_num_unop op = op = Sqrt || op = Umin

(* The binary operators *)

type binop = 
  | Plus | Mult | Div 
  | And | Or 
  | Neq | Eq
  | Gt | Geq | Lt | Leq

let is_num_binop op = op = Mult || op = Plus || op = Div

let is_bool_binop op = op = And || op = Or

let is_comp_binop op = op = Eq || op = Neq || op = Geq || op = Gt || op = Leq || op = Lt

(* Unary numerical operator without and with square root 

type uopN = Umin
type uopNsq = Umin | Sqrt


type bopN = Plus | Mult
type bopNd = Plus | Mult | Div

type uopB = Neg
type bopB = And | Or

type bopC = 
  | Neq | Eq
  | Gt | Geq | Lt | Leq

type num_expr = 
  | Const of num_const
  | Value of uvar
  | Uop of uopN * num_expr
  | Bop of bopN * num_expr * num_expr

type num_expr_sq = 
  | Const of num_const
  | Value of uvar
  | Uop of uopNsq * num_expr_sq
  | Bop of bopNd * num_expr_sq * num_expr_sq

type bool_expr =
  | Const of bool_const
  | Value of uvar
  | Uop of uopB * bool_expr
  | Bop of bopB * bool_expr * bool_expr
  | Cop of bopC * num_expr * num_expr
  | LetB of uvar * bool_expr * bool_expr

type exprN = 
  | Num of num_expr 
  | Bool of bool_expr 
  | Pair of exprN * exprN 
  | Fst of exprN * exprN

type exprNsq = 
  | Num of num_expr_sq 
  | Bool of bool_expr 
  | Pair of exprNsq * exprNsq 
  | Fst of exprNsq * exprNsq


type progN = 
  | Exp of exprN
  | Letin of var * progN * progN
  | If of progN * progN * progN

type progNsq =
  | Exp of exprNsq
  | Letin of var * progN * progNsq
  | If of progN * progNsq * progNsq

type exprE = 
  | Value of uvar
  | Const of constants
  | UOp of unop * exprE
  | BOp of binop * exprE * exprE
  | Pair of exprE * exprE
  | Fst of exprE
  | Snd of exprE

type progP =
  | Exp of exprE
  | Letin of var * progP * progP
  | If of progP * progP * progP
*)

(* General type of programs*)

type program =
  | Value of uvar
  | Const of constants
  | UOp of unop * program
  | BOp of binop * program * program
  | Pair of program * program
  | Fst of program 
  | Snd of program
  | Letin of var * program * program
  | If of program * program * program
  | Temp of (program -> program) * (program list)
  | Clean of program                   


let rec var_to_program var =
	match var with 
		| Var x -> Value x
		| Pairvar (x,y) -> Pair(var_to_program x, var_to_program y)
