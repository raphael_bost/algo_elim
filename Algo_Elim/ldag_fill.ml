open List
open Pretty_printer
open Language
open Tools
open Arithmetic
open Dag
open Common_template
open Anti_unif 
open Basic_tree 
open Duplicate_checker
         
(* let performance_cst_col_flag = ref true;; (* set to true if you want performance enhancement in counterpart to les exhaustivity *) *)
(* let performance_mutiple_repetition_flag = ref true;; (* set to true if you want performance enhancement in counterpart to les exhaustivity *) *)

(* check if two col2 is greater or equal to col1 *)
(* this means that for every corresponding elements a and b of respectively col1 and col2, a=b or b is None *)
(* unused *)
exception Invalid_args_geq_column
                  
let rec geq_column col1 col2 ldag =
	geq_column_aux col1 col2 ldag (map tl ldag)
and geq_column_aux col1 col2 ldag ref_dag = 
	match col1, col2 with
	| [],[] -> true
	| None::q1,(Some d)::q2 -> false
	| a::q1,None::q2 -> geq_column q1 q2 (tl ref_dag)
	| (Some d1)::q1,(Some d2)::q2 -> let dag_queue = hd(ref_dag) in 
		(equal_tree (dag_to_tree (d1::dag_queue)) (dag_to_tree (d2::dag_queue))) && (geq_column_aux q1 q2 (tl ldag) (tl ref_dag))
	| _ -> raise Invalid_args_geq_column   								
	
                                   
(* checks if the two columns are equal  *) 
(*unused *)
let rec equal_column col1 col2 ldag =
	equal_column_aux col1 col2 ldag (map tl ldag)
and equal_column_aux col1 col2 ldag ref_dag= 
	match col1, col2 with
	| [],[] -> true
	| None::q1,None::q2 -> geq_column q1 q2 (tl ref_dag)
	| (Some d1)::q1,(Some d2)::q2 -> let dag_queue = hd(ref_dag) in 
		(equal_tree (dag_to_tree (d1::dag_queue)) (dag_to_tree (d2::dag_queue))) && (equal_column_aux q1 q2 (tl ldag) (tl ref_dag))
	| a1::q1,a2::q2 -> false
	| _ -> raise Invalid_args_geq_column   								
   	
(* if all column elements are either indetermined or an element with a positive complete all the column by a*)
(* if not, return an empty list*) 

let rec generate_identical_col col lpos ldag original_ldag_length =                 
	let (a,pos) = generate_identical_col_aux EmptyD col (-1) 0 in
		if (a <> EmptyD && (length col) = original_ldag_length  
			&& (mem (dag_to_tree (a::(tl (nth ldag pos)))) lpos)  (* check if the element finded is positive *)
			(* || not (mem None col)) (* if the column is already filled with a, do not add it, it will be done by the next function *)  *) (* use less as we will concatenate without repeating *)
			)
			then [create_constant_list (Some a) (length col)]
		else []
and generate_identical_col_aux elt col pos i = (* find if there is only one defined element in the column and if yes returns it with the position in the column*) 
	match col with
		| [] -> (elt,pos)
		| None::q -> generate_identical_col_aux elt q pos (i+1) 
		| (Some d)::q when elt = EmptyD -> generate_identical_col_aux d q i (i+1) (* we have found it !*)
		| (Some d)::q when equal_dag_elt d elt -> generate_identical_col_aux elt q pos (i+1)
		| _ -> (EmptyD,-1) (* an other non indetermined non equal to elt element *) 
                         

(* generate all possible columns obtained by filling none elements in col *)
(* encountered_elt_l is the list of the elements that have been encountered previously in the line *)
(* def_elt is the default element *)
let rec generate_all_possible_cols col encountered_elt_opt_l def_elt =   	
	match col with
		| [] -> [[]]
		| None::q -> let elts = (def_elt)::(hd encountered_elt_opt_l) (* the element that can be put in the box *) 
						and previous_cols = generate_all_possible_cols q (tl encountered_elt_opt_l) def_elt in
		 					fold_left (fun b elt -> rev_append (add_to_head (Some elt) previous_cols) b) [] elts  (* concatenate with the previous ones *)
		| a::q -> let previous_cols = generate_all_possible_cols q (tl encountered_elt_opt_l) def_elt in
					add_to_head a previous_cols

;;

(* scan the column to add the encountered elements of each line *)
(*  the couple (encountered_elt_l,encountered_tree_l) corresponds to the encountered elements respectivly as dags and as trees   *)

let rec update_encountered col (encountered_elt_l,encountered_tree_l) ldag= 
	match col,(encountered_elt_l,encountered_tree_l),ldag with 
		| [],(_,_),_ -> ([],[])
	    | None::q,(e_l::r,t_l::t),_::s -> (* no element to add *) 
										let (n_el,n_tl) = (update_encountered q (r,t) s)  in
											(e_l::n_el,t_l::n_tl)
		| (Some d)::q, (e_l::r,t_l::t),dag::s -> let (n_el,n_tl) = (update_encountered q (r,t) s)  in
										let tree = (dag_to_tree (d::(tl dag))) in
										   if mem tree t_l then (* the element d as already been encountered in the line *)
														 (e_l::n_el,t_l::n_tl) (* no element to add *)
										   else 	 ((d::e_l)::n_el,(tree::t_l)::n_tl)(* add d *)

;;   

(* the result dependends on the value of performance_cst_col_flag *)
(* if it is true, we first generate the identical column, return it if it is not empty
														and if yes, return all the columns obtained by filling None placeholders by encountered elements *)
(* if not, concatenate the columns (identical and encountered filled columns) and return the obtained list*)
let generate_all_cols_from_column col ldag encountered_elt_l def_elt lpos original_ldag_length =
	if !Settings_vars.performance_cst_col_flag then
		match generate_identical_col col lpos ldag original_ldag_length with
	   	| [] -> generate_all_possible_cols col encountered_elt_l def_elt
		| [col] -> [col]
	else
		conc_no_repeat (generate_identical_col col lpos ldag original_ldag_length) (generate_all_possible_cols col encountered_elt_l def_elt)
;;

(* given a ldag and its corresponding lbase, the positive elements and the default element, fill all the None placeholders *)
(* this is the "exhaustive" method, kind of bruteforce *)
let rec construct_all_dags_from_base_exhaustive lbase ldag lpos def_elt original_ldag_length=                                                                       
	let dag_tails = construct_all_dags_from_base_aux lbase ldag lpos ((create_empty_tab (length lbase)),(create_empty_tab (length lbase))) def_elt original_ldag_length in
	let dag_heads = map hd ldag in 
		add_col_to_bases dag_heads dag_tails
and construct_all_dags_from_base_aux lbase ldag lpos (encountered_elt_l,encountered_tree_l) def_elt original_ldag_length=
	if length (hd lbase) == 0 then [create_empty_tab (length lbase)]
	else 
		let col = map hd lbase in (* col is the considered column *) 
			(* generate the new columns *)
			(* let ncols = conc_no_repeat (generate_identical_col col lpos ldag) (generate_all_possible_cols col encountered_elt_l def_elt) *)
			let ncols =  generate_all_cols_from_column col ldag encountered_elt_l def_elt lpos original_ldag_length
			(* get the encoutered elements for the next iteration *)
			and (n_encountered_elt_l,n_encountered_tree_l) = update_encountered col (encountered_elt_l,encountered_tree_l) ldag in 
				(* print_string " ncols length : ";print_int (length ncols); print_string "\n"; *)
				(* construct all the possible bases after us *)
				let lnbases = construct_all_dags_from_base_aux (map tl lbase) ldag lpos (n_encountered_elt_l,n_encountered_tree_l) def_elt original_ldag_length in
					let ndags_cols = map (fun col -> map opt_to_dag_elt col) ncols in 
					let contructed_bases = map (fun c -> add_col_to_bases c lnbases) ndags_cols in 
						concat contructed_bases (* flatten the obtained list *)
;;
  
(*  *)
(* These functions are designed to take care of the number of times an element appears originally in a dag *)
(* Thus, we just don't copy it too many times instead of None placeholders, reducing the number of dags to study *)
(*  *)                                                                                                          


(* count_pointers_in_dag_elt dag_elt counts modifies the counts list by counting the references to each dag element in dag_elt *)
(* counts is a (dag_elt * int ref) list *)

let rec count_pointers_in_dag_elt dag_elt counts = 
	match dag_elt with
	  | ExprD(_) -> () 
	  | VecD(l) -> iter (fun (ai,li) -> iter (fun d -> count_pointers_in_dag_elt d counts) li ) l
	  | DivD(d1,d2) -> count_pointers_in_dag_elt d1 counts; count_pointers_in_dag_elt d2 counts;
	  | PtD(i) ->  let c = snd (nth counts i) in c := !c + 1
	  | PairD(d1,d2) -> count_pointers_in_dag_elt d1 counts; count_pointers_in_dag_elt d2 counts; 
	  | EmptyD -> ()
                                            
(* count the references to every element of the dag within the dag itself *)
(* returns a (dag_elt * int) list *)
let rec count_pointers_in_dag dag = 
	let count_refs = count_pointers_in_dag_aux dag (map (fun d -> (d,ref 0)) dag) in
	map (fun (d,i_ref) -> (d,!i_ref)) count_refs
and count_pointers_in_dag_aux dag counts = 
	match dag with
		|[] -> counts
		|d::q -> count_pointers_in_dag_elt d counts;
				 count_pointers_in_dag_aux q counts
;;
    
exception Insufficient_count_exception;;
  
(* Returns a new (dag_elt * int) list generated by removing an occurence of dag_elt in the dag_counts list *)
let rec remove_occurence dag_elt dag_counts =
	if dag_elt = ExprD(Const(Num 0.)) then dag_counts (* zero is a special element that can appear the number of times we want *)
	else
		match dag_counts with 
			| [] -> []
			| (d,count)::q when (d = dag_elt && count>0) -> (d,count-1)::q
			| (d,count)::q when (d = dag_elt) -> raise Insufficient_count_exception
			| (d,count)::q -> (d,count)::(remove_occurence dag_elt q)
		
                                                
(* Generate all the dag counts list (associated to a ldag) by removing the occurrence of the corresponding element in col to a dag *)
let rec update_counts_using_col l_dag_counts col =  
	match l_dag_counts,col with 
		| [],[] -> []
	    | dag_counts::q,dag_elt::r ->  let n_dag_counts = remove_occurence dag_elt dag_counts 
									   (* the let raises an Insufficient_count_exception if dag_elt can't appear in the dag *)
									   in n_dag_counts::(update_counts_using_col q r)
										  
   		| _ -> invalid_arg "l_dag_count and col must have the same size"
	    							
;;
                    
(* for a given l_dag_counts (associated with a ldag), just keep the columns we can add to this ldag, and also generate the new corresponding l_dag_counts *)
let rec select_cols_and_count cols l_dag_counts = 
	match cols with
		| [] -> ([],[])
		| col::q -> let (new_cols,new_counts) = select_cols_and_count q l_dag_counts in
						try let new_count = update_counts_using_col l_dag_counts col in
							(col::new_cols,new_count::new_counts)
						with Insufficient_count_exception -> (new_cols,new_counts) (* an element was inserted two many times with this col, reject it *)
;;

(* replace none placeholders either by zero or an element already encountered in the line *)
(* this is the version designed for improved performances : 
for a given dag, we don't insert an element more times than the number of times he is pointed *)

let rec construct_all_dags_from_base_perf lbase ldag lpos def_elt original_ldag_length= 
	let dag_tails = construct_all_dags_from_base_perf_aux lbase ldag lpos ((create_empty_tab (length lbase)),(create_empty_tab (length lbase))) def_elt original_ldag_length (map count_pointers_in_dag ldag)   in
	let dag_heads = map hd ldag in 
		add_col_to_bases dag_heads dag_tails
and construct_all_dags_from_base_perf_aux lbase ldag lpos (encountered_elt_l,encountered_tree_l)  def_elt original_ldag_length l_dag_counts =
	if length (hd lbase) == 0 then [create_empty_tab (length lbase)]
	else 
		let col = map hd lbase in (* col is the considered column *) 
			(* generate the new possible columns by filling col *)
			let ncols =  generate_all_cols_from_column col ldag encountered_elt_l def_elt lpos original_ldag_length
			(* get the encoutered elements for the next iteration *)
			and (n_encountered_elt_l,n_encountered_tree_l) = update_encountered col (encountered_elt_l,encountered_tree_l) ldag in
				
				(* check if the generated cols are valid ie. an element is not inserted too many times *)
				let ndags_cols = map (fun col -> map opt_to_dag_elt col) ncols in 
				let (selected_cols,n_dag_counts) = select_cols_and_count ndags_cols l_dag_counts in
				
				(* construct all the possible bases after us *)
				let llnbases = map (construct_all_dags_from_base_perf_aux (map tl lbase) ldag lpos (n_encountered_elt_l,n_encountered_tree_l) def_elt original_ldag_length) n_dag_counts  in
					(* add the generated columns to the generated bases*)
					let contructed_bases =  map (fun lnbases ->  concat (map (fun c -> add_col_to_bases c lnbases) selected_cols) ) llnbases
											    in 
						concat contructed_bases (* flatten the obtained list *)
;; 

(* switch between the performance and the exhaustive version *)
let construct_all_dags_from_base  =
	if !Settings_vars.performance_mutiple_repetition_flag then 
		construct_all_dags_from_base_perf
	else
		construct_all_dags_from_base_exhaustive
;;


(* these functions are called by iterative_extended_unification3 when reinserting empty dags *)
(* we have to modify a bit the empty dags so we could insert constants in the templates *) 

                             
let rec contains_empty col =
	match col with
	| [] -> false
	| EmptyD::q -> true
	| _::q -> contains_empty q
;;
let rec empty_dag_elt_to_zero col def_elt = match col with
	| [] -> []
	| EmptyD::q -> def_elt::(empty_dag_elt_to_zero q def_elt)
	| d::q -> d::(empty_dag_elt_to_zero q def_elt)
;;

let rec fill_empty_dags_with_constants lpos def_elt ldag =            
	let dag_tails = fill_empty_dags_with_constants_aux ldag (map tl ldag) lpos def_elt in
	let dag_heads = map hd ldag in 
		add_col_to_bases dag_heads dag_tails
and fill_empty_dags_with_constants_aux (start_ldag:Dag.dag_elt list list) ldag lpos def_elt =
	if length (hd ldag) == 0 then [create_empty_tab (length ldag)]
	else 
		begin
			let col =  (map hd ldag) in
			let other_cols = fill_empty_dags_with_constants_aux start_ldag (map tl ldag) lpos def_elt in
			if contains_empty col then
				(
			   	let cst_col = (generate_identical_col (map dag_elt_to_opt col) lpos start_ldag (length start_ldag)) in
				let no_constant = add_col_to_bases (empty_dag_elt_to_zero col def_elt) other_cols in
				(* let no_constant = add_col_to_bases col other_cols in *)
				                      
				(* print_endline("\nconstant col ? "^(string_of_int (length cst_col))); *)
				if length cst_col = 0 then 
					no_constant
				else
					if !Settings_vars.performance_cst_col_flag then
						(add_col_to_bases (map opt_to_dag_elt (hd cst_col)) other_cols)
					else 
					no_constant@(add_col_to_bases (map opt_to_dag_elt (hd cst_col)) other_cols)
            	)
			else
				add_col_to_bases col other_cols
		end 
;;	









