(* file : lexer.mll *)

{
open Parser (* Assumes the parser file is "parser.mly" *)
}


let digit = ['0'-'9']
let ident = ['a'-'z' 'A'-'Z']
let ident_num = ['a'-'z' 'A'-'Z' '0'-'9' '_']
rule token = parse
  | [' ' '\t']	{ token lexbuf }
  | '\n'	{ token lexbuf }
  | digit+
  | "." digit+
  | digit+ "." digit* as num
		{ CONST (float_of_string num) }
  | '+'		{ PLUS }
  | '-'		{ MINUS }
  | '*'		{ MULT }
  | '/'		{ DIV }
  | "sqrt"      { SQRT }
  | '('		{ LPAREN }
  | ')'		{ RPAREN }
  | '{'		{ LBR }
  | '}'		{ RBR }
  | '='		{ EQ }
  | '<'		{ LT }
  | '>'         { GT }
  | ">="        { GEQ }
  | "<="        { LEQ }
  | "!="        { NEQ }
  | "true"      { TRUE }
  | "false"     { FALSE }
  | '~'         { NEG }
  | "&&"        { AND }
  | "||"        { OR }
  | "()"        { UNIT }
  | "fail"      { FAIL }
  | "fst"       { FST }
  | "snd"       { SND }
  | ','         { VIR }
  | "let"       { LET }
  | "in"        { IN }
  | "Clean"     { CLEAN }
  | "if"        { IF }
  | "then"      { THEN }
  | "else"      { ELSE }
  | "fi"        { FI }
  | ";;"        { EOP }
  | ident ident_num* as word
                { VAR (word) }
  | _		{ token lexbuf }
  | eof		{ raise End_of_file }
