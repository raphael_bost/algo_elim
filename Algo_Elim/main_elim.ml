open Language
open Pretty_printer
open Variables
open Program_classes
open Type_inference
open Elim_sqrt_bool
open Elim_let

let rec elim_main e tenv posvar n =
  match e with
    | e when is_an_expression e && type_infer e tenv = BoolT -> elim_bool (red_pair e)
    | e when is_an_expression e && type_infer e tenv = NumT -> red_pair e
    | Pair(e1,e2) -> Pair(elim_main e1 tenv posvar n, elim_main e2 tenv posvar n)
    | e when is_an_expression e -> e
    | If(f,e1,e2) -> If(elim_main f tenv posvar n, elim_main e1 tenv posvar n, elim_main e2 tenv posvar n)
    | Letin(x,p1,p2) ->
	let pp1 = elim_main p1 tenv posvar n in
	let (nx,bod,pp2,nposvar,k) = elim_let x pp1 p2 posvar n in
	let ppp2 = elim_main pp2 (add_type_env nx (type_infer bod tenv) tenv) nposvar k in
	  Letin(nx,bod,ppp2);;
	  

let ifname = ref "ifname";; 

let main_transform p = 
  let pif = p_norm_reduction !ifname p in
  let pp = elim_main pif [] [] 1 in red_pair pp;;
