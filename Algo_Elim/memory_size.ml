open Language;;
open Pretty_printer;;
open List;;

type type_size = 
    Mem_Num of int
  | Mem_Bool 
  | Mem_Pair of type_size * type_size

exception Error_type_size of (type_size * (string * type_size) list)

let rec type_size_value t = 
  match t with
    | Mem_Num n -> n
    | Mem_Pair (t1,t2) -> type_size_value t1 + (type_size_value t2)
    | _ -> 1

let plus_mem t1 t2 =
  (type_size_value t1 + (type_size_value t2))

let max_mem t1 t2 = max (type_size_value t1) (type_size_value t2)

let rec max_tmem t1 t2 =
  match t1,t2 with
    | Mem_Bool, Mem_Bool -> Mem_Bool
    | Mem_Pair(t11,t12), Mem_Pair(t21,t22) ->
	Mem_Pair(max_tmem t11 t21,max_tmem t12 t22)
    | Mem_Num(n1),Mem_Num(n2) -> Mem_Num(n1 + n2)
    | _ -> raise (Error_type_size (Mem_Pair(t1,t2),[]))

let rec add_var_env_mem v vm lenv = 
  match v with
    | Var s -> (s,vm)::(remove_assoc s lenv)
    | Pairvar(v1,v2) -> 
	let Mem_Pair(vm1,vm2) = vm in add_var_env_mem v2 vm2 (add_var_env_mem v1 vm1 lenv)

let rec sum_list l = fold_left (fun s (a,b) -> (type_size_value b) + s) 0 l;;


let rec max_memory_size p sz =
  memory_and_exec_size_aux p sz [] 0 0
and memory_and_exec_size_aux p s l un_sup total_sup =
  match p with
    | Value y -> 
	begin 
	  try 
	    let vs = assoc y l in 
	      (vs, max (type_size_value vs) un_sup, max ((type_size_value vs) + (sum_list l)) total_sup)
	  with 
	      Not_found -> (Mem_Num s, max s un_sup, max (s + (sum_list l)) total_sup) 
	end
    | (Const (Num a)) -> (Mem_Num (snd(frexp(a)) + 1), max (snd(frexp(a)) + 1) un_sup,max ((sum_list l) + snd(frexp(a)) + 1) total_sup) 
    | (Const (Bool _)) -> (Mem_Bool, max 1 un_sup, max (sum_list l + 1) total_sup)
    | UOp(Umin,e1) -> (memory_and_exec_size_aux e1 s l un_sup total_sup)
    | UOp(Sqrt,e1) -> let (Mem_Num ms,um,tm) = (memory_and_exec_size_aux e1 s l un_sup total_sup) in (Mem_Num (2*ms), um, tm)
    | UOp(Neg,e1) -> (memory_and_exec_size_aux e1 s l un_sup total_sup)
    | BOp(Plus,e1,e2) -> 
	let (ms1,um1,tm1) = (memory_and_exec_size_aux e1 s l un_sup total_sup) in
	let (ms2,um2,tm2) = memory_and_exec_size_aux e2 s l um1 tm1 in 
	  (Mem_Num ((max_mem ms1 ms2) + 1), max (plus_mem ms1 ms2) um2, max ((plus_mem ms1 ms2) + sum_list l) tm2)
    | BOp(Mult,e1,e2) -> 	
	let (ms1,um1,tm1) = (memory_and_exec_size_aux e1 s l un_sup total_sup) in
	let (ms2,um2,tm2) = memory_and_exec_size_aux e2 s l um1 tm1 in 
	  (Mem_Num(plus_mem ms1 ms2), max (plus_mem ms1 ms2) um2, max ((plus_mem ms1 ms2) + sum_list l) tm2)
    | BOp(Div,e1,e2) -> 	
	let (ms1,um1,tm1) = (memory_and_exec_size_aux e1 s l un_sup total_sup) in
	let (ms2,um2,tm2) = memory_and_exec_size_aux e2 s l um1 tm1 in 
	  (Mem_Num(plus_mem ms1 ms2), max (plus_mem ms1 ms2) um2, max ((plus_mem ms1 ms2) + sum_list l) tm2)
    | BOp(op,e1,e2) when is_comp_binop op -> 
	let (ms1,um1,tm1) = (memory_and_exec_size_aux e1 s l un_sup total_sup) in
	let (ms2,um2,tm2) = memory_and_exec_size_aux e2 s l um1 tm1 in 
	  (Mem_Bool, max (plus_mem ms1 ms2) um2, max ((plus_mem ms1 ms2) + sum_list l) tm2)
    | BOp(op,e1,e2) when is_bool_binop op -> 
	let (ms1,um1,tm1) = (memory_and_exec_size_aux e1 s l un_sup total_sup) in
	let (ms2,um2,tm2) = memory_and_exec_size_aux e2 s l um1 tm1 in 
	  (Mem_Bool, max (plus_mem ms1 ms2) um2, max ((plus_mem ms1 ms2) + sum_list l) tm2)
    | Pair (e1,e2) -> 
 	let (ms1,um1,tm1) = (memory_and_exec_size_aux e1 s l un_sup total_sup) in
	let (ms2,um2,tm2) = memory_and_exec_size_aux e2 s l um1 tm1 in 
	  (Mem_Pair(ms1,ms2), max (plus_mem ms1 ms2) um2, max ((plus_mem ms1 ms2) + sum_list l) tm2)
    | Fst(e1) -> 
	let (Mem_Pair (ms1,ms2),um,tm) = (memory_and_exec_size_aux e1 s l un_sup total_sup) in 
	  (ms1,um,tm)
    | Snd(e1) -> 
	let (Mem_Pair (ms1,ms2),um,tm) = (memory_and_exec_size_aux e1 s l un_sup total_sup) in 
	  (ms2,um,tm)
    | Clean(e1) -> memory_and_exec_size_aux e1 s l un_sup total_sup
    | If(f,e1,e2) -> 
 	let (msf,umf,tmf) = (memory_and_exec_size_aux f s l un_sup total_sup) in
 	let (ms1,um1,tm1) = (memory_and_exec_size_aux e1 s l umf tmf) in
	let (ms2,um2,tm2) = memory_and_exec_size_aux e2 s l um1 tm1 in 
	  (max_tmem ms1 ms2, um2,tm2)
    | Letin(v,e1,e2) -> 
	let (ms1,um1,tm1) = memory_and_exec_size_aux e1 s l un_sup total_sup in
	  memory_and_exec_size_aux e2 s (add_var_env_mem v ms1 l) um1 tm1	



let rec print_mem_size p k =
  let (_,um,tm) = max_memory_size p k in
    print_string "\nmax element size is : "; print_string (string_of_int um); 
    print_string "\ntotal needed memory is :"; print_string ((string_of_int tm)^"\n");;
