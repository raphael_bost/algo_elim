type token =
  | VAR of (string)
  | CONST of (float)
  | LPAREN
  | RPAREN
  | LBR
  | RBR
  | VIR
  | PLUS
  | MINUS
  | MULT
  | DIV
  | SQRT
  | TRUE
  | FALSE
  | NEG
  | AND
  | OR
  | GT
  | LT
  | EQ
  | GEQ
  | LEQ
  | NEQ
  | UNIT
  | FAIL
  | CLEAN
  | FST
  | SND
  | LET
  | IN
  | FUND
  | IF
  | THEN
  | ELSE
  | FI
  | TY
  | ARROW
  | NUM
  | BOOL
  | EOP

val input :
  (Lexing.lexbuf  -> token) -> Lexing.lexbuf -> Language.program
