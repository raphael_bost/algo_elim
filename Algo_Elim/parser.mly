/* file : parser.mly */

%{
open Language
open Printf
%}

%token <string> VAR
%token <float> CONST
%token LPAREN RPAREN LBR RBR VIR
%token PLUS MINUS MULT DIV SQRT
%token TRUE FALSE
%token NEG AND OR
%token GT LT EQ GEQ LEQ NEQ
%token UNIT FAIL CLEAN
%token FST SND
%token LET IN CLEAN
%token FUND
%token IF THEN ELSE FI
%token TY ARROW NUM BOOL
%token EOP
%right VIR
%left OR
%left AND
%nonassoc NEG
%nonassoc GT LT EQ GEQ LEQ NEQ
%left PLUS MINUS
%left MULT DIV
%left SQRT NEG UMINUS 



%start input
%type <Language.program> input


/*Grammar */
%%

input : 
   | pexpr EOP                               { $1 }


pexpr:
   | CONST		                     { Const (Num $1) }
   | VAR                                     { Value $1 }
   | pexpr PLUS pexpr                        { BOp (Plus,$1, $3) }
   | pexpr MINUS pexpr		             { BOp (Plus,$1, UOp(Umin,$3)) }
   | pexpr MULT pexpr                        { BOp (Mult ,$1, $3) }
   | pexpr DIV pexpr		             { BOp (Div ,$1, $3) }
   | SQRT pexpr                              { UOp (Sqrt, $2) }
   | MINUS pexpr %prec UMINUS	             { UOp (Umin, $2) }
   | LPAREN pexpr RPAREN                     { $2 }   
   | pexpr GT pexpr                          { BOp (Gt,$1, $3) }
   | pexpr LT pexpr                          { BOp (Lt,$1, $3) }
   | pexpr EQ pexpr                          { BOp (Eq,$1, $3) }
   | pexpr GEQ pexpr                         { BOp (Geq,$1, $3) }
   | pexpr LEQ pexpr                         { BOp (Leq,$1, $3) }
   | pexpr NEQ pexpr                         { BOp (Neq,$1, $3) }
   | TRUE                                    { Const (Bool true) }
   | FALSE                                   { Const (Bool false) }
   | NEG pexpr                               { UOp (Neg,$2) }
   | pexpr AND pexpr                         { BOp (And,$1, $3) }
   | pexpr OR pexpr                          { BOp (Or,$1, $3) }
   | LET varexpr EQ pexpr IN pexpr           { Letin ($2,$4,$6) }      
   | CLEAN pexpr                             { Clean ($2) }
   | IF pexpr THEN pexpr ELSE pexpr FI       { If ($2, $4, $6) }
   | pexpr VIR pexpr                         { Pair ($1,$3) }
   | FST pexpr                               { Fst $2 }
   | SND pexpr                               { Snd $2 }
;;

varexpr:
   | varexpr VIR varexpr                     { Pairvar($1,$3) }
   | LPAREN varexpr RPAREN                   { $2 }
   | VAR                                     { Var $1}
;;
