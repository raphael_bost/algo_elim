(* Steinhaus–Johnson–Trotter algorithm to iteratively generate  *)

exception End;;

type direction = Left | Right;;

let value_of (_,n) = n
and direction_of (d,_) = d;;

let sees vect i = 
	match direction_of vect.(i) with
		| Left -> i-1
		| Right -> i+1;;
		
let is_mobile vect i =
	try value_of vect.(sees vect i) < value_of vect.(i)
	with Invalid_argument(_) -> false;;
	
let inverse vect i = 
	match vect.(i) with
		| Left,p -> vect.(i) <- Right,p
		| Right,p -> vect.(i) <- Left,p;;
		
let initialize n = 
	let v = Array.init n (fun i -> (Left,i+1)) in
	v;;
	
let next vect n = 
	let rec search_max_mobile i max_i = 
		if i = n then
			if max_i = (-1) then raise End
			else max_i
		else
			if is_mobile vect i && (max_i = (-1) || (value_of vect.(i)) > (value_of vect.(max_i)))
			then search_max_mobile (i+1) i
			else search_max_mobile (i+1) max_i
	in
	let max_i = search_max_mobile 0 (-1) in 
	let max = vect.(max_i)
	and see_index = sees vect max_i in
	let see = vect.(see_index) in 
		vect.(max_i) <- see; vect.(see_index) <- max;
	
	for i = 0 to n-1 do 
		if (value_of vect.(i)) > (value_of max) then inverse vect i
	done;;