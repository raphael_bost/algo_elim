open List
open Dag
open Tools 
open Perm_generator         
                
            
(* Given an element a and a list l, return all the lists where a is inserted in some place in l *)
let rec insert_every_place a l =
  match l with 
      [] -> [[a]]
    | b::q -> 
	(a::l)::(map (fun li -> b::li) (insert_every_place a q));;
         
let rec gives_permutations l = 
  match l with 
    | [] -> [[]]
    | a::q -> 
	let lq = gives_permutations q in
	  concat (map (insert_every_place a) lq);;

(* gives the list [1...n] *)

let rec list_n n =
  if n<=0 then [] else (list_n (n-1))@[n];;

(* gives all permutations of list [1...n]*)

let gives_permutations_n n =
  gives_permutations (list_n n);;

let rec generate_permutation_variable_length length_list =
	let non_mixed_permutations = map gives_permutations_n length_list in
	mix_lists non_mixed_permutations;;
	

let rec initialize_tables n m = 
	match m with
	| 0 -> []
	| _ -> (Perm_generator.initialize n)::(initialize_tables n (m-1))	
	                               
let perm_vect_to_list vect = 
	let l = Array.to_list vect in
	map snd l;; 
	
	
exception Last_permutation;;

class generator n_init m_init = 
	object
		val n = n_init
		val m = m_init
		val mutable perm_table = initialize_tables n_init m_init
		
		method get_perms () = map perm_vect_to_list perm_table
		
		method next_permutation () = let rec next_permutation_aux p_table i= 
 								   		match p_table with
											| [] -> raise Last_permutation
											| vect::q -> try Perm_generator.next vect n; p_table 
														with End -> (* (print_string (("One turn : "^(string_of_int i))^"\n")); *)
														(Perm_generator.initialize n)::(next_permutation_aux q (i+1))
									 in perm_table <- next_permutation_aux perm_table 1
	end;;         
	