open Language
open Interpreter

let bop_to_string op =
  match op with
    | Eq -> "="
    | Neq -> "!="
    | Lt -> "<"
    | Gt -> ">"
    | Leq -> "<="
    | Geq -> ">="
    | Or -> "||"
    | And -> "&&"
    | Plus -> "+"
    | Mult -> "*"
    | Div -> "/"

let uop_to_string op =
  match op with
    | Sqrt -> "sqrt"
    | Umin -> "-"
    | Neg -> "~"

let const_to_string c =
  match c with
    | Num a -> string_of_float a
    | Bool b -> string_of_bool b;;

let print_string_f o s =
  begin 
    output_string o s;
    flush o;
  end;;

let rec print_blank n =
  match n with
    | 0 -> ()
    | k -> print_string " "; print_blank (k-1)
;;

let rec print_blank_o out n =
  match n with
    | 0 -> ()
    | k -> output_string out " "; print_blank_o out (k-1)
;;

let rec print_ind e n =
  match n with
    | 0 -> print_string e
    | n when n>0 -> print_string " "; print_ind e (n-1);;

let rec print_ind_o o e n =
  match n with
    | 0 -> output_string o e
    | n when n>0 -> output_string o " "; print_ind_o o e (n-1);;

let rec var_to_string x =
  match x with
    | Var s -> s
    | Pairvar (v1,v2) -> "("^(var_to_string v1)^", "^(var_to_string v2)^")";;
  

exception Unprintable_programm

let rec prog_to_string p =
	match p with 
   		| Const x -> const_to_string x
		| Value x -> x
		| Clean(e1) -> "Clean("^(prog_to_string e1)^")"
		| BOp(op,e1,e2) -> "("^(prog_to_string e1)^(bop_to_string op)^(prog_to_string e2)^")"
		| UOp(op,e1) -> (uop_to_string op)^"("^(prog_to_string e1)^")"
		| Pair(e1,e2) -> "("^(prog_to_string e1)^", "^(prog_to_string e2)^")"     
		| Fst e1 -> "fst ("^(prog_to_string e1)^")"
		| Snd e1 -> "snd ("^(prog_to_string e1)^")"
		| Letin (x,e1,e2) -> "let "^(var_to_string x)^" = "^(prog_to_string e1)^" in \n"^(prog_to_string e2)
		| If (f,e1,e2) -> "if " ^ (prog_to_string f) ^"\n then "^(prog_to_string e1)^"\n else "^(prog_to_string e2)^"\n fi"
	    | _ -> raise Unprintable_programm;;


let rec print_prog p =
  print_prog_aux p 1; print_string ";;"
and print_prog_aux p n = 
  match p with
    | Const x -> print_ind (const_to_string x) n
    | Value x -> print_ind x n
    | Clean (e1) -> 
	print_ind ("Clean (") n; print_prog_aux e1 0; print_ind ")" 0 
    | BOp (op,e1,e2) -> 
	print_ind "(" n; 
	print_prog_aux e1 0; 
	print_ind (") "^(bop_to_string op)^ " (") 0; 
	print_prog_aux e2 0; print_ind ")" 0
    | UOp (op,e1) -> print_ind ((uop_to_string op)^"(") n; print_prog_aux e1 0; print_ind ")" 0;
    | Pair (e1,e2) -> 
	print_ind "(" n; print_prog_aux e1 0; print_ind ", " 0; (print_prog_aux e2) 1; print_ind ")" 0; 
    | Fst e1 -> print_ind ("fst (") n; print_prog_aux e1 0; print_ind ")" 0;
    | Snd e1 -> print_ind ("snd (") n; print_prog_aux e1 0; print_ind ")" 0;
    | Letin (x,e1,e2) -> 
	print_ind ("let "^(var_to_string x)^" = \n") (n); print_prog_aux e1 (n+5); print_ind " in \n" 1; print_prog_aux e2 (n); 
    | If (f,e1,e2) -> 
	print_ind ("if\n") n; 
	print_prog_aux f (n+3); print_string "\n";
	print_ind "then \n" n; 
	print_prog_aux e1 (n+2); print_string "\n"; 
	print_ind "else \n" n; 
	print_prog_aux e2 (n+2); print_string "\n"; print_ind "fi \n" n;;

let prec_bop_left op = 
  match op with
    | Eq -> 2
    | Neq -> 2
    | Lt -> 2
    | Gt -> 2
    | Leq -> 2
    | Geq -> 2
    | Or -> 0
    | And -> 1
    | Plus -> 3
    | Mult -> 4
    | Div -> 4;;

let prec_bop_right op = 
  match op with
    | Eq -> 2
    | Neq -> 2
    | Lt -> 2
    | Gt -> 2
    | Leq -> 2
    | Geq -> 2
    | Or -> 0
    | And -> 1
    | Plus -> 3 
    | Mult -> 4
    | Div -> 5;;

let prec_uop op =
  match op with
    | Sqrt -> 5
    | Umin -> 5
    | Neg -> 2;;



let output_expr out exp =
   (* Local function definitions *)
   let open_paren prec op_prec =
     if prec > op_prec then output_string out "(" in
   let close_paren prec op_prec =
     if prec > op_prec then output_string out ")" in
   let rec print prec exp n =     (* prec is the current precedence *)
     match exp with
       Const c -> print_ind_o out (const_to_string c) n
     | Value x -> print_ind_o out x n
    | Clean (e1) -> 
	print_ind_o out ("Clean (") n; print 0 e1 0; print_ind_o out ")" 0 
     | BOp(op,f, g) ->
         print_blank_o out n;
         open_paren prec (prec_bop_left op);
         print (prec_bop_left op) f 0; output_string out (" "^(bop_to_string op)^" "); 
	 print (prec_bop_right op) g 0;
         close_paren prec (prec_bop_left op)
    | UOp (op,e1) -> 
         print_blank_o out n;
         open_paren prec (prec_uop op);
         output_string out (uop_to_string op); print (prec_uop op) e1 0;
         close_paren prec (prec_uop op)
    | Pair (e1,e2) -> 
	print_ind_o out "(" n; print 0 e1 0; output_string out ", "; print 0 e2 0; output_string out ")"; 
    | Fst e1 -> print_ind_o out ("fst (") n; print 0 e1 0; output_string out ")";
    | Snd e1 -> print_ind_o out ("snd (") n; print 0 e1 0; output_string out ")";
    | Letin (x,e1,e2) -> 
	print_ind_o out ("let "^(var_to_string x)^" =\n") n; 
        print 0 e1 (n+5); output_string out " in\n"; 
	print 0 e2 (n); 
    | If (f,e1,e2) -> 
	print_ind_o out ("if\n") n; 
	print 0 f (n+3); output_string out "\n";
	print_ind_o out "then \n" n; 
	print 0 e1 (n+2); output_string out "\n"; 
	print_ind_o out "else \n" n; 
	print 0 e2 (n+2); output_string out "\n"; 
	print_ind_o out "fi \n" n
   in print 0 exp 0
;;

let print_expr exp =
	output_expr stdout exp
;;

let rec print_elt e =
  match e with
    | NumI e -> print_float e
    | BoolI true -> print_string "true"
    | BoolI false -> print_string "false"
    | PairI(e1,e2) -> print_elt e1; print_string ", "; print_elt e2
