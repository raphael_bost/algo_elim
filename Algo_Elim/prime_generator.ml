let rec power_mod a p m =
	  match p with 
	    | 0 -> 1
		| 1 -> a mod m
	    | n when n>0 && n mod 2 = 0 -> let an2 = power_mod a (n/2) m in (an2 * an2) mod m
	    | n when n>0 -> (a * (power_mod a (n-1) m)) mod m
;;
	   
exception Not_Prime;;                                                
let is_probable_prime ?nBase:(nBase = 20) (n:int) = 
	(* True if N is a strong pseudoprime for nbases random bases b < N.
    Uses the Miller--Rabin primality test.
    *)                                  
(* IMPORTANT : INITIALIZE THE RANDOM GENERATOR BEFORE USING THIS FUNCTION *)
	(* Random.self_init (); *)

	let s = ref 0 in
	let t = ref (n-1) in
	
	while (!t mod 2) = 0 do
		t := (!t)/2;
		s := (!s) +1;
	done;
	try 
		for i=1 to nBase do
			let a = (Random.int (n-1))+1 in (* a is between 1 and n-1 (inclusive) *) 
		
			let b = ref (power_mod a !t n) in
			if !b <> 1 then    (* else continue *)
				begin
					let flag = ref false in
						for k=0 to !s do
							if (!b = (n-1)) then flag := true;
						             
							b := ((!b)*(!b)) mod n;					
						done;                   
					if (not !flag) then (raise Not_Prime)					
				end		
		done;          
		true;
	with Not_Prime -> false
;;


let prime_between mini maxi = 
	Random.self_init (); 
	
	let n = ref (mini + 1 + (Random.int (maxi-mini))) in
	
	while (not (is_probable_prime !n)) do
		n := mini + 1 + (Random.int (maxi-mini))
	done;
	!n
;; 
	