open Language
open Type_inference



(* Check if a program is an expression (class E) *)

let rec is_an_expression p =
  match p with
    | Value _ | Const _ | Temp _-> true
    | BOp(_,e1,e2) | Pair(e1,e2)-> is_an_expression e1 && is_an_expression e2
    | UOp(_,e1) | Fst e1 | Snd e1 | Clean(e1) -> is_an_expression e1
    | Letin _ | If _ -> false;;


(* check if an expression is in E extended with boolean affectations *)

let rec is_an_expression_with_letin_bool p =
  match p with
    | Value _ | Const _ | Temp _-> true
    | BOp(_,e1,e2) | Pair(e1,e2)-> is_an_expression_with_letin_bool e1 && is_an_expression_with_letin_bool e2
    | UOp(_,e1) | Fst e1 | Snd e1 | Clean(e1) -> is_an_expression_with_letin_bool e1
    | Letin(x,e1,e2) -> 
	is_an_expression_with_letin_bool e2 && 
	  is_an_expression_with_letin_bool e1 &&
	  begin
	    try is_tuple_bool (type_infer_operation e1 []) with No_expression -> false
	  end
    | If _ -> false;;



(* Check if a program is in P_let (no test at top level) *)

let rec is_in_P_let p =
  match p with
    | Value _ | Const _ | Temp _-> true
    | BOp(_,e1,e2) | Pair(e1,e2) -> is_in_P_let e1 && is_in_P_let e2
    | UOp(_,e1) | Fst e1 | Snd e1 | Clean(e1) | Letin(_,_,e1) -> is_in_P_let e1
    | If _ -> false;;

