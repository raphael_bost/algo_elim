open Language
open Interpreter
open Pretty_printer;;
open Elim_div_bool;;
open Elim_sqrt_bool;;
open Elim_let;;
open Memory_size;;
open Main_elim;;
open Extended_elim;;


(* entry is a file or the standard input *)

let filename = ref None;;

let insert name = 
  filename := Some name;;

(* option test specifies the file is in the test directory *)

let curr_directory = ref ""

let test_fil () =
  curr_directory := "/Users/raphaelbost/Code/Stage_M1/Algo_Elim/Test_files/"

let input_filename () = 
    match !filename with
      | None -> "stdin"
      | Some f -> ((!curr_directory)^f)

                   
(* Performances options default values *)


(* main interpret or transform program *)

type operation =
    Elim | Elim_Generation | Interp | Mem_size of int

let op = ref Elim_Generation 

let interprate () = 
  op := Interp

let mem_size k = 
  op := Mem_size (int_of_string k)


let print_prog_elim p = 
  print_expr (main_transform p);;

let multiple_prog_elim p = 
  program_flusher (extended_main_transform p) (input_filename());;

let old_elim () = 
	op := Elim

let world = ref []


let print_interp p = 
  let newworld = (interpreter p (!world)) in
    print_elt newworld;;

let choose_fun o =
    match o with
      | Elim -> print_prog_elim
	  | Elim_Generation ->  multiple_prog_elim
      | Interp -> print_interp
      | Mem_size k -> fun p -> print_mem_size p k

(* change var sq name *)

let new_sq_var_name sq =
  xsq := sq;;

(* an initial world file is specified *)

let init_world w =
  interprate ();
  let lexbuf = Lexing.from_channel (open_in (!curr_directory^w)) in
  let result = Parser.input Lexer.token lexbuf in
    world := (get_environnement result []);;


(* use multiple atoms to eliminate division *)

let rel_to_zero_one_at () =
  rule_div := rule_div1;;

(* no name for the atoms in elimbool*)

let rule_sqrt_without_name () =
  rule_sqrt := rule_sqrt_no_name;;
	
(* list of available options *)

let options = Arg.align [
  "-t", Arg.Unit test_fil , " Called file is in the test_dir directory";
  "-i", Arg.Unit interprate, " Program interprate and return the world" ;
  "-iw", Arg.String init_world, " Interprate with initial world" ;
  "-ma", Arg.Unit rel_to_zero_one_at, " Use several atoms to eliminate division" ;
  "-nn", Arg.Unit rule_sqrt_without_name, " Does not name the atoms when eliminating division" ;
  "-nsq", Arg.String new_sq_var_name, " Change name of new squareroot vars";
  "-ms", Arg.String mem_size, " evaluate the memory required for exact computation";
  "-old", Arg.Unit old_elim, " Use the old way to generate one template";
  "-perf_col", Arg.Bool Settings_vars.set_performance_column_flag, " set a perf. flag : always put constants in the template if possible";
  "-perf_rep", Arg.Bool Settings_vars.set_performance_multiple_repetition_flag, " set a perf. flag : limit the number of duplications in a dag";
  "-perf_h_inj", Arg.Bool Settings_vars.set_performance_injective_hashcode_flag, " set a perf. flag : assume the hash function is injective";
  "-h", Arg.Int Settings_vars.set_hash_function, " set the hash function";
  "-v", Arg.Unit Settings_vars.set_verbose, "Verbose mode";
  "-vv", Arg.Unit Settings_vars.set_very_verbose, "Very verbose mode";
  "-md", Arg.Int Settings_vars.set_max_duplications, " Set the maximum number of duplications (max_int by default)";
 ];;

let usage =
  Printf.sprintf "Usage: %s <options> <filename>" Sys.argv.(0)
                                   	

let prog_input () = 
    match !filename with
      | None -> stdin
      | Some f -> (open_in ((!curr_directory)^f))
