(* set to true if you want performance enhancement in counterpart to less exhaustivity *) 
let performance_cst_col_flag = ref true;;    

let set_performance_column_flag flag = 
	performance_cst_col_flag := flag
;;
(* set to true if you want performance enhancement in counterpart to less exhaustivity - we should prove that the generated templates are exhaustive *)
let performance_mutiple_repetition_flag = ref true;; 

let set_performance_multiple_repetition_flag flag = 
	performance_mutiple_repetition_flag := flag
;;

(* set to true if you want performance enhancement in counterpart to less exhaustivity - the result given by this method are unpredictable*) 
let performance_injective_hashcode_flag = ref false;;

let set_performance_injective_hashcode_flag flag = 
	performance_injective_hashcode_flag := flag
;;                        
                            
let hash_function_index = ref 4;;

let set_hash_function index = 
	hash_function_index := index
;;
                                 

let verbose = ref false;;
let set_verbose () = verbose := true;;

let very_verbose = ref false;;
let set_very_verbose () = verbose := true;very_verbose := true;;

let max_duplications = ref max_int;;
let set_max_duplications k = max_duplications := k;;