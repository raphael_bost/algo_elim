open Pretty_printer
open Language
open Dag
open Common_template
open Anti_unif
open Extended_pretty_printer
open List
open Basic_tree
open Extended_unif
open Duplicate_checker
open Break_sharing
open Permutation
open Tools;;



let a = Value "a"   
let b = Value "b"
let c = Value "c"
let d = Value "d"  
let e = Value "e"  
let x = Var "x"
let xsq = "sq"
let nsq = 1
let lpos = []
let locvar = [] 
let def_elt = ExprD(Const(Num 0.))
let n = 1;;

let p1 = a;;
let p2 = BOp(Plus,UOp(Sqrt,b),a);;
let start_ldag = [tree_to_dag(expr_to_tree p1);tree_to_dag(expr_to_tree p2)];;       
let l_template = map fst (iterative_extended_unification3 start_ldag x xsq nsq lpos locvar def_elt n);;
dag_list_pretty_printer(map test_template l_template);;


let p1 = BOp(Plus,UOp(Sqrt,a),UOp(Sqrt,b))
let p2 = UOp(Sqrt,BOp(Plus,UOp(Sqrt,b),a))
let start_ldag = [tree_to_dag(expr_to_tree p1);tree_to_dag(expr_to_tree p2)];;       
  
let p1 = BOp(Div,UOp(Sqrt,a),b);;
let p2 = BOp(Plus,UOp(Sqrt,a),UOp(Sqrt,c));;
let start_ldag = [tree_to_dag(expr_to_tree p1);tree_to_dag(expr_to_tree p2)];;       



let p3 = Pair(UOp(Sqrt,a),Pair(UOp(Sqrt,b),UOp(Sqrt,b)));;
let p4 = Pair(UOp(Sqrt,c),Pair(UOp(Sqrt,c),UOp(Sqrt,d)));;
let start_ldag2 = [tree_to_dag(expr_to_tree p3);tree_to_dag(expr_to_tree p4)];;       
          
let a_1 = Value "a_1"
let a_2 = Value "a_2"
let interm = BOp(Plus,a_1,BOp(Mult,Const(Num 3.),a_2));; 
let sq_exp = UOp(Sqrt,BOp(Plus,b,UOp(Sqrt,c)));;          
let num = BOp(Plus,interm,sq_exp);;
let denom = BOp(Plus,d,UOp(Sqrt,e));;
let p5 = BOp(Div,num,denom);;

let p6 = BOp(Div,BOp(Plus,a,sq_exp),UOp(Sqrt,c));;
let p7 = sq_exp;;
let start_ldag2 = [tree_to_dag(expr_to_tree p7);tree_to_dag(expr_to_tree p6)];;       



let l_template = map fst (iterative_extended_unification2 start_ldag x xsq nsq lpos locvar def_elt n);;
dag_list_pretty_printer(map test_template l_template);;
length l_template;;


(* for step-by-step computation *)
let lpermldag = all_permutations_n start_ldag n;;
let lpermldag_reduced = tidy_none_cols lpermldag;;
let lnewldags = fold_right conc_no_repeat (map (fun (ldag,lbase) -> construct_all_dags_from_base lbase ldag lpos def_elt) lpermldag_reduced) [];;
let ll_ptr_permuted_ldags = map (generate_ldags_by_breaking_sharing) lnewldags;;
let final_ldags = fold_right conc_no_repeat ll_ptr_permuted_ldags [];;
let l_complete_templates = map (fun ldag -> (fst(construct_complete_template_se_var ldag x xsq nsq lpos locvar),ldag)) final_ldags;;
let l_complete_templates_unique = rebuild_template_set_commutativity l_complete_templates;; 
let l_complete_templates_useful = filter keep_complete_template_predicate l_complete_templates_unique;; 


                                                                         


let p1 = BOp(Div,UOp(Sqrt,a),b)
let p2 = UOp(Sqrt,BOp(Div,c,d))
let start_ldag = [tree_to_dag(expr_to_tree p1);tree_to_dag(expr_to_tree p2)];;   





If (BOp (Gt, Value "a", Const (Num 0.)),
   Pair (UOp (Sqrt, Value "a"),
    Pair (UOp (Sqrt, Value "b"), UOp (Sqrt, Value "b"))),
   Pair (UOp (Sqrt, Value "c"),
    Pair (UOp (Sqrt, Value "c"), UOp (Sqrt, Value "d")))),
  BOp (Plus, BOp (Plus, Value "x", Value "y"), Value "z"))

