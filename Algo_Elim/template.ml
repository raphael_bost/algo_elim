open Language
open List
open Tools
open Type_inference
open Arithmetic
open Elim_div_bool


let template_list l localvars posvar = 
  match l with 
    | [] -> (fun k -> zero),[]
    | a::q -> ((fun k -> a), (map (fun e -> zero) l));;

(* p2 is the scope since we do not want to create confict with the varaiables we choose, temp is the template used for x*)

let rec var_from_template x e temp = 
  match x with 
    | Var s -> Var (s^"_def")
    | Pairvar(v1,v2) -> Pairvar(var_from_template v1 e temp,var_from_template v2 e temp)





