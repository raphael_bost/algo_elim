open List

(* add to association list, in case of identical keys, the greatest value is preserved *)

let rec add_max l1 l2 =
  match l1 with 
    | (a,b)::q when mem_assoc a l2 -> 
	let bp = assoc a l2 in 
	  add_max q ((a,max b bp)::(remove_assoc a l2))
    | (a,b)::q -> add_max q ((a,b)::l2)
    | [] -> l2;;

(* in an association list extract one key that matches a 0 *)

let rec get_to_zero_elt l =
  match l with
    | (e,0)::q -> Some e
    | a::q -> get_to_zero_elt q
    | [] -> None;;


(* Add an element to a list if not already in it *)

let cons_no_repeat e l =
  if mem e l then l else e::l;;

(* Add two lists without repetitions *)

let rec conc_no_repeat l1 l2 =
  match l1 with
    | [] -> l2
    | a::q -> 
	if mem a l2 
	then conc_no_repeat q l2
	else conc_no_repeat q (a::l2);;
                                        
let rec delete_duplicates l =
	delete_duplicates_aux l []
and delete_duplicates_aux l1 l2 = 	 
	match l1 with
	| [] -> l2
	| a::q when mem a l2 -> delete_duplicates_aux q l2
	| a::q -> delete_duplicates_aux q (a::l2) 
;;
	
(* Check if 2 list contains the same elements *)

let rec equal_comm l1 l2 =
  if length l1 = length l2 then contain_comm l1 l2 else false
and contain_comm l1 l2 =
  match l1 with
    | a::q when mem a l2 -> contain_comm q l2
    | [] -> true
    | _ -> false

(* Give the intersection of 2 lists *)

let rec intersect l1 l2 =
  match l1 with
    | a::q when mem a l2 -> a::(intersect q l2)
    | [] -> []
    | a::q -> intersect q l2

(* remove an element from list *)

let rec remove a l =
  match l with
    | b::q when b = a -> remove a q
    | [] -> []
    | b::q -> b::(remove a q)

let rec remove_once a l = 
	match l with
	| [] -> []
	| b::q when b = a -> q
    | b::q -> b::(remove a q)
    
(* give the disjoint union of 2 lists *)

let rec disj_union l1 l2 =
  match l1 with
    | a::q when mem a l2 -> disj_union q (remove a l2)
    | a::q -> a::(disj_union q l2)
    | [] -> l2;;


(* return the matching element and the list without it in a reverse assoc list (as vect list)*)

let rec get_rev_assoc b l =
  match l with
    | (e1,e2)::q when equal_comm e2 b -> (Some e1,q)
    | a::q -> 
	let (e,l2) = get_rev_assoc b q in (e,a::l2)
    | [] -> (None,[]);;


(* return the position of the element that verify p *)

let rec get_pos_predicate p l =
  get_pos_predicate_aux p l 1
and get_pos_predicate_aux p l n =
  match l with
    | a::q when p a -> Some n
    | a::q -> get_pos_predicate_aux p q (n+1)
    | [] -> None

(* return the key of the element matching n *)

let rec rev_assoc i l =
  match l with
    | (a,b)::q when b = i -> b
    | _::q -> rev_assoc i q
    | _ -> raise Not_found


(* gives the position of the element a in l *)

let rec pos a l =
  match l with
    | b::q when b=a -> 0
    | b::q -> (pos a q) + 1;;


(* function a^b *)

exception Power_neg;;

let rec power a b =
  match b with 
      0 -> 1
    | n when n>0 && n mod 2 = 0 -> let an2 = power a (n/2) in an2 * an2
    | n when n>0 -> a * (power a (n-1))
    | _ -> raise Power_neg;;


let invert_lexical_order (a,b) (c,d) =
  if a=c then b > d else a < c;;

(* create a constant list with n times the element a *)
let rec create_constant_list a n =
	if n = 0 then []
	else a::(create_constant_list a (n-1))
	                      
(* add the element a to all the lists of lists in ll *)
let add_to_head a ll =
	map (fun l -> a::l) ll   
	
	
let rec invert_list_of_list ll = 
	if (length (hd ll)) = 0 then []
	else	(map hd ll)::(invert_list_of_list (map tl ll)) 
	
	
let rec replace_in_list_at_pos l a lpos =
	replace_in_list_at_pos_aux l a lpos 0
	and replace_in_list_at_pos_aux l a lpos n =
		match l with
			| [] -> []
			| b::q when (mem n lpos) -> a::(replace_in_list_at_pos_aux q a lpos (n+1))
			| b::q -> b::(replace_in_list_at_pos_aux q a lpos (n+1))
	                                                                                       
let rec remove_duplicates l = 
	match l with 
		| [] -> []
		| a::q -> cons_no_repeat a (remove_duplicates q);;
	
let rec remove_duplicate_predicate l p = 
	match l with
		| [] -> []
		| a::q -> let nq = remove_duplicate_predicate q p in
					if (exists (p a) nq) then
						nq
					else a::nq
                                   
(* col is an 'a list, lbase is a 'a list list , the two lists must have the same length *)
(* if col = [c1;c2;...;cn] and lbase = [l1;l2;...;ln] computes [c1::l1;...;cn::ln] *)
let  add_col_to_base col lbase = 
	map2 (fun elt b -> elt::b) col lbase
;;

(* do add_col_to_base for every lbase in llbase *)
(* for efficiency reason (avoid stack overflows), use rev_map *)
let add_col_to_bases col llbase = 
	rev_map (add_col_to_base col) llbase
;;

let rec mix_lists ll = 
	match ll with
		|[] -> [[]]
		|[l] -> [l]
		| l::q -> let mixedl = mix_lists q in
					fold_left (fun acc mlist -> fold_left (fun b a -> (a::mlist)::b) acc l) [] mixedl 
    
(* returns n! *)
let fact n =
  let rec loop acc i n =
    if i > n
    then acc
    else loop (i*acc) (i+1) n
  in
    loop 1 1 n
;;

(* create the list that contains n times the empty list *)
let rec create_empty_tab n =
  match n with 
    | 0 -> []
    | _ -> []::(create_empty_tab (n-1))
;;
	                    	