open Language;;
open Tools;;
open Variables;;
open Interpreter;;
open List;;



(* types possible in our language *)

type types = BoolT | NumT | PairT of types * types

(* typing error that can happen *)

exception Typing_error of program * types * types
exception No_pair_error of program * types

(* infer the type of an expression  we assume that non defined variables are of type num *)

let rec type_infer p env =
  match p with
    | Value v -> 
	begin
	  try assoc v env with
	    | Not_found -> NumT
	end
    | Const (Num f) -> NumT
    | Const (Bool b) -> BoolT
    | BOp (op,e1,e2) when is_num_binop op ->
	begin
	  match type_infer e1 env, type_infer e2 env with
	    | NumT,NumT -> NumT
	    | t,NumT -> raise (Typing_error (e1,t,NumT))
	    | NumT,t -> raise (Typing_error (e2,t,NumT))
	    | t1,t2 -> raise (Typing_error ((Pair(e1,e2)),(PairT (t1,t2)),(PairT(NumT,NumT))))
	end
    | BOp (op,e1,e2) when is_comp_binop op ->
	begin
	  match type_infer e1 env, type_infer e2 env with
	    | NumT,NumT -> BoolT
	    | t,NumT -> raise (Typing_error (e1,t,NumT))
	    | NumT,t -> raise (Typing_error (e2,t,NumT))
	    | t1,t2 -> raise (Typing_error ((Pair(e1,e2)),(PairT (t1,t2)),(PairT(NumT,NumT))))
	end
    | BOp (op,e1,e2) when is_bool_binop op ->
	begin
	  match type_infer e1 env, type_infer e2 env with
	    | BoolT,BoolT -> BoolT
	    | t,BoolT -> raise (Typing_error (e1,t,BoolT))
	    | BoolT,t -> raise (Typing_error (e2,t,BoolT))
	    | t1,t2 -> raise (Typing_error ((Pair(e1,e2)),(PairT (t1,t2)),(PairT(BoolT,BoolT))))
	end
    | UOp (op,e1) when is_num_unop op ->
	begin
	  match type_infer e1 env with
	    | NumT -> NumT
	    | t -> raise (Typing_error(e1,t,NumT))
	end
    | UOp (Neg,e1) ->
	begin
	  match type_infer e1 env with
	    | BoolT -> BoolT
	    | t -> raise (Typing_error(e1,t,BoolT))
	end
    | Pair (e1,e2) -> 
	PairT (type_infer e1 env,type_infer e2 env)
    | Fst a ->
	begin
	  match type_infer a env with
	  | PairT(t1,_) -> t1
	  | t -> raise (No_pair_error(a,t))
	end
    | Snd a ->
	begin
	  match type_infer a env with
	  | PairT(_,t2) -> t2
	  | t -> raise (No_pair_error(a,t))
	end
    | Letin (v,e1,e2) ->
	let env2 = add_type_env v (type_infer e1 env) env in type_infer e2 env2
    | If (f,e1,e2) ->
	let (t1,t2) = (type_infer e1 env, type_infer e2 env) in
	  begin
	    match type_infer f env with
	      | BoolT -> if t1=t2 then t1 else raise (Typing_error(e2,t2,t1))
	      | t -> raise (Typing_error(f,t,BoolT))
	  end
    | Clean e -> (type_infer e env)
and add_type_env_aux v t =
  match v with
    | Var s -> [(s, t)]
    | Pairvar (v1,v2) ->
	match t with
	  | PairT (t1,t2) -> 
	      let (env1,env2) = (add_type_env_aux v1 t1, add_type_env_aux v2 t2) in
		fold_right 
		  (fun (var,value) en -> 
		     if mem_assoc var en 
		     then raise Identical_variable
		     else ((var,value)::en)
		  )
		  env1
		  env2
	  | _ -> raise Pairvar_error
and add_type_env v t env =
  let nenv = add_type_env_aux v t in
    fold_right
      (fun (var,value) newenv -> (var,value)::(remove_assoc var newenv))
      nenv
      env;;




type abstract_type_operation =
    ABool | ANum | AVar | APair of abstract_type_operation * abstract_type_operation;;


let rec is_tuple_bool t =
  match t with
    | APair(e1,e2) -> is_tuple_bool e1 && is_tuple_bool e2
    | AVar -> true
    | ABool -> true
    | _ -> false

exception Pair_type_error;;
exception No_expression;;

let rec type_infer_operation p env =
  match p with
    | Const (Num _) -> ANum
    | Const (Bool _) -> ABool
    | Value x ->
	begin
	  try assoc x env with Not_found -> AVar
	end
    | Fst e1 -> 
	begin
	  match type_infer_operation e1 env with
	  | AVar -> AVar
	  | APair(t1,_) -> t1
	  | _ -> raise Pair_type_error
	end
    | Snd(e1) ->
	begin
	  match type_infer_operation e1 env with
	  | AVar -> AVar
	  | APair(_,t2) -> t2
	  | _ -> raise Pair_type_error
	end
    | UOp(Neg,_) -> ABool
    | UOp(op,_) when is_num_unop op -> ANum
    | BOp(op,_,_) when is_bool_binop op || is_comp_binop op -> ABool
    | BOp(op,_,_) when is_num_binop op -> ANum
    | Pair(e1,e2) -> APair (type_infer_operation e1 env, type_infer_operation e2 env)
    | Letin(x,e1,e2) -> 
	if is_tuple_bool (type_infer_operation e1 env) 
	then 
	  let vars = get_list_var_id x in
	  let benv = conc_no_repeat (map (fun x -> (x,ABool)) vars) env in
	    type_infer_operation e2 benv
	else 
	  raise No_expression;;


