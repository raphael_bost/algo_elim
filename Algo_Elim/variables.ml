open Language
open List
open Tools

(* List of the variable id the are in the variable tuple*)

let rec get_list_var_id var =
  match var with
    | Pairvar (e1,e2) -> (get_list_var_id e1)@(get_list_var_id e2)
    | Var s -> [s];;

(* Get the list of variables that appear in an expression *)

let rec get_list_free_var_exp e =
  match e with
    | Const _ -> []
    | Value y -> [y]
    | Fst(e1) | Snd(e1) | UOp(_,e1) -> get_list_free_var_exp e1
    | BOp(_,e1,e2) | Pair(e1,e2) -> get_list_free_var_exp e1 @ get_list_free_var_exp e2
    | Clean(e1) -> get_list_free_var_exp e1
    | Letin(v,e1,e2) -> fold_right remove (get_list_var_id v) (get_list_free_var_exp e1 @ get_list_free_var_exp e2)
    | If(f,e1,e2) -> get_list_free_var_exp f @ get_list_free_var_exp e1 @ get_list_free_var_exp e2;;


(* Check if a variable appear as free in e *)

let rec not_free_in x e =
  match e with
    | Const _ -> true
    | Value y when y=x -> false
    | Value y -> true
    | UOp(op,e1) -> not_free_in x e1 
    | BOp(op,e1,e2) -> not_free_in x e1 && not_free_in x e2
    | Pair(e1,e2) -> not_free_in x e1 && not_free_in x e2
    | Fst(e1) | Snd(e1) -> not_free_in x e1
    | Letin(v,e1,e2) ->
	not_free_in x e1 && (if mem x (get_list_var_id v) then true else not_free_in x e2)
    | If(f,e1,e2) -> not_free_in x f && not_free_in x e1 && not_free_in x e2
    | Clean(e1) -> not_free_in x e1;;



(* Check if all variables from the list appear as free in e *)

let rec not_free_in_list lx e =
  fold_right 
    (fun x b -> not_free_in x e && b)
    lx
    true;;

(* get a new var id that respect the conditions *)

let rec get_var_cond v f =
  if f v v then v else get_var_cond_aux v (v^"_f") f 0
and get_var_cond_aux v nv f n =
  if  f v (nv^(string_of_int n)) then (nv^(string_of_int n)) else get_var_cond_aux v nv f (n+1);;


(* replace x with xp in tuple v *)

let rec replace_in_tuple x xp v =
  match v with
    | Var y when y = x -> Var xp
    | Var y -> Var y
    | Pairvar(v1,v2) -> Pairvar(replace_in_tuple x xp v1, replace_in_tuple x xp v2);;

(* iter replace in tuple with list of var *)

let rec replace_in_tuple_list v lv =
  match v with
    | Pairvar(v1,v2) -> Pairvar(replace_in_tuple_list v1 lv, replace_in_tuple_list v2 lv)
    | Var y ->
	try Var (assoc y lv) with | Not_found -> v;;


(* Inlining function that respect conflicts (a modifier*)

let rec inline x e p =
  match p with
    | Value y when y = x -> e
    | Value y -> p
    | Const _ -> p
    | UOp(op,e1) -> UOp(op, inline x e e1)
    | BOp(op,e1,e2) -> BOp(op,inline x e e1,inline x e e2)
    | If(f,e1,e2) -> If(inline x e f, inline x e e1, inline x e e2)
    | Pair (e1,e2) -> Pair(inline x e e1, inline x e e2)
    | Fst(e1) -> Fst(inline x e e1)
    | Snd(e1) -> Snd(inline x e e1)
    | Letin(v,e1,e2) ->
	if (mem x (get_list_var_id v))
	then Letin(v,inline x e e1,e2)
	else
	  if not_free_in x e2 
	  then Letin(v,inline x e e1,e2)
	  else 
	    let varv = get_list_var_id v in
	    let vare = get_list_free_var_exp(e) in
	    let vare2 = get_list_free_var_exp(e2) in
	    let (lv,lve,p2) = fold_right 
	      (fun vi (l,le,p) -> if not (mem vi le) then (l,vi::le,p) else 
	       let nvi = get_var_cond vi (fun x y -> not (mem y (conc_no_repeat le vare2))) in ((vi,nvi)::l,nvi::le,inline vi (Value nvi) p)) varv ([],vare,e2) in
	    let nv = replace_in_tuple_list v lv in
	      Letin(nv,inline x e e1, inline x e p2)

let rec inline_tuple_expr v e p =
  match v,e with
    | (Var s,Value sp) when s = sp -> p
    | (Var s),_ -> inline s e p
    | (Pairvar(v1,v2),Pair(e1,e2)) -> inline_tuple_expr v2 e2 (inline_tuple_expr v1 e1 p)


(* reduction to take operation inside test and variable declarations given a program in normal form*)

let rec	uop_p_norm_switch op p =
  match p with
    | Letin(x,e1,e2) -> Letin(x,e1,uop_p_norm_switch op e2)
    | If(f,e1,e2) -> If(f,uop_p_norm_switch op e1,uop_p_norm_switch op e2)
    | _ -> UOp(op,p)

let rec fst_p_norm_switch p =
  match p with
    | Letin(x,e1,e2) -> Letin(x,e1,fst_p_norm_switch e2)
    | If(f,e1,e2) -> If(f,fst_p_norm_switch e1,fst_p_norm_switch e2)
    | Pair(e1,e2) -> e1
    | _ -> Fst(p)

let rec snd_p_norm_switch p =
  match p with
    | Letin(x,e1,e2) -> Letin(x,e1,snd_p_norm_switch e2)
    | If(f,e1,e2) -> If(f,snd_p_norm_switch e1,snd_p_norm_switch e2)
    | Pair(e1,e2) -> e1
    | _ -> Snd(p)

let rec	bop_p_norm_switch ifname op p1 p2 =
  match p1,p2 with
    | Letin(x,e1,e2),_ ->
      let lx = get_list_var_id x in
      let varp2 = get_list_free_var_exp p2 in
      let vare2 = get_list_free_var_exp e2 in
      let (lv,lve,p11) = fold_right
	(fun vi (l,le,p) -> if not (mem vi le) then (l,vi::le,p) else
	    let nvi = get_var_cond vi (fun x y -> not (mem y (conc_no_repeat le vare2))) in ((vi,nvi)::l,nvi::le,inline vi (Value nvi) p)) lx ([],varp2,e2) in
      let nx = replace_in_tuple_list x lv in
          Letin(nx,e1,bop_p_norm_switch ifname op p11 p2)
    | If(f,e1,e2),_ ->
      let xx = get_var_cond ifname (fun x y -> not_free_in y p2) in Letin(Var xx,p1,bop_p_norm_switch ifname op (Value xx) p2)
    | _,Letin(x,e1,e2) ->
      let lx = get_list_var_id x in
      let varp1 = get_list_free_var_exp p1 in
      let vare2 = get_list_free_var_exp e2 in
      let (lv,lve,p21) = fold_right
	(fun vi (l,le,p) -> if not (mem vi le) then (l,vi::le,p) else
	    let nvi = get_var_cond vi (fun x y -> not (mem y (conc_no_repeat le vare2))) in ((vi,nvi)::l,nvi::le,inline vi (Value nvi) p)) lx ([],varp1,e2) in
      let nx = replace_in_tuple_list x lv in
          Letin(nx,e1,bop_p_norm_switch ifname op p1 p21)
    | _,If(f,e1,e2) ->
      let xx = get_var_cond ifname (fun x y -> not_free_in y p1) in Letin(Var xx,p2,bop_p_norm_switch ifname op p1 (Value xx))
    | _,_ -> BOp(op,p1,p2);;


let rec	pair_p_norm_switch ifname p1 p2 =
  match p1,p2 with
    | Letin(x,e1,e2),_ ->
      let lx = get_list_var_id x in
      let varp2 = get_list_free_var_exp p2 in
      let vare2 = get_list_free_var_exp e2 in
      let (lv,lve,p11) = fold_right
	(fun vi (l,le,p) -> if not (mem vi le) then (l,vi::le,p) else
	    let nvi = get_var_cond vi (fun x y -> not (mem y (conc_no_repeat le vare2))) in ((vi,nvi)::l,nvi::le,inline vi (Value nvi) p)) lx ([],varp2,e2) in
      let nx = replace_in_tuple_list x lv in
          Letin(nx,e1,pair_p_norm_switch ifname p11 p2)
    | If(f,e1,e2),_ ->
      let xx = get_var_cond ifname (fun x y -> not_free_in y p2) in Letin(Var xx,p1,pair_p_norm_switch ifname (Value xx) p2)
    | _,Letin(x,e1,e2) ->
      let lx = get_list_var_id x in
      let varp1 = get_list_free_var_exp p1 in
      let vare2 = get_list_free_var_exp e2 in
      let (lv,lve,p21) = fold_right
	(fun vi (l,le,p) -> if not (mem vi le) then (l,vi::le,p) else
	    let nvi = get_var_cond vi (fun x y -> not (mem y (conc_no_repeat le vare2))) in ((vi,nvi)::l,nvi::le,inline vi (Value nvi) p)) lx ([],varp1,e2) in
      let nx = replace_in_tuple_list x lv in
          Letin(nx,e1,pair_p_norm_switch ifname p1 p21)
    | _,If(f,e1,e2) ->
      let xx = get_var_cond ifname (fun x y -> not_free_in y p1) in Letin(Var xx,p2,pair_p_norm_switch ifname p1 (Value xx))
    | _,_ -> Pair(p1,p2);;


let rec p_norm_reduction ifname p =
  match p with
    | UOp(op,e1) -> let p1 = p_norm_reduction ifname e1 in uop_p_norm_switch op p1
    | Fst(e1) -> let p1 = p_norm_reduction ifname e1 in fst_p_norm_switch p1
    | Snd(e1) -> let p1 = p_norm_reduction(ifname)(e1) in snd_p_norm_switch p1
    | BOp(op,e1,e2) -> let (p1,p2) = (p_norm_reduction ifname e1, p_norm_reduction ifname e2) in bop_p_norm_switch ifname op p1 p2
    | Pair(e1,e2) -> let (p1,p2) = p_norm_reduction ifname e1, p_norm_reduction(ifname)(e2) in pair_p_norm_switch ifname p1 p2
    | Letin(x,e1,e2) -> Letin(x,p_norm_reduction ifname e1,p_norm_reduction ifname e2)
    | If(f,e1,e2) -> If(p_norm_reduction ifname f,p_norm_reduction ifname e1,p_norm_reduction ifname e2)
    | _ -> p


(* the simpl pair reduction *)

let rec red_pair e =
  match e with
    | Const _ | Value _ | Clean _  -> e
    | UOp(op,e1) -> UOp(op,red_pair e1)
    | BOp(op,e1,e2) -> BOp(op, red_pair e1, red_pair e2)
    | Pair(e1,e2) -> Pair(red_pair e1, red_pair e2)
    | Fst(e1) ->
	begin
	  let ee1 = red_pair e1 in
	    match ee1 with
	      | Pair(e2,e3) -> e2
	      | _ -> Fst ee1
	end
    | Snd(e1) ->
	begin
	  let ee1 = red_pair e1 in
	    match ee1 with
	      | Pair(e2,e3) -> e3
	      | _ -> Snd ee1
	end
    | Letin(x,e1,e2) -> Letin(x,red_pair e1, red_pair e2)
    | If(f,e1,e2) -> If(red_pair f, red_pair e1,red_pair e2);;


(* check if it is ok to use nx to replace x in p2 *)

let rec get_conflict_variables x nx p2 =
  let lx = get_list_var_id x in
  let lnx = get_list_var_id nx in
    fold_right
      (fun y l ->
	 if ((mem y lx) || (not_free_in y p2)) then l else (y::l)
      )
      lnx
      [];;


(* replace a tuple by new values for variables according to the list of variables that create conflicts *)

let rec new_tup v l =
  match v with
    | Var s -> if mem s l then (Var ("new_"^s)) else v
    | Pairvar(v1,v2) -> Pairvar (new_tup v1 l, new_tup v2 l);;

(* construct a new tup var with no conflict *)

let rec new_tup_var x nx p2 =
  match get_conflict_variables x nx p2 with
    | [] -> nx
    | l -> new_tup_var x (new_tup nx l) p2
